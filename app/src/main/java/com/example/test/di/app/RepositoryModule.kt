package com.example.test.di.app

import android.content.Context
import com.example.test.common.NavigationHelper
import com.example.test.data.database.repository.DatabaseRepository
import com.example.test.data.database.repository.DatabaseRepositoryImpl
import com.example.test.data.repository.preferencesRepository.PreferencesRepository
import com.example.test.data.repository.preferencesRepository.PreferencesRepositoryImp
import com.example.test.data.repository.string.StringProvider
import com.example.test.data.repository.string.StringProviderImp
import com.example.test.data.server.ServerRepository
import com.example.test.data.server.ServerRepositoryImpl
import com.example.test.interactor.*

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideStringProvider(): StringProvider = StringProviderImp(context)

    @Provides
    @Singleton
    fun providePreferencesRepository(): PreferencesRepository = PreferencesRepositoryImp(context)

    @Provides
    @Singleton
    fun userInteractor(pref: PreferencesRepository,server:ServerRepository): UserInteractor = UserInteractor(pref,server)

    @Provides
    @Singleton
    fun serverRepository(): ServerRepository = ServerRepositoryImpl()

    @Provides
    @Singleton
    fun databaseRepository():DatabaseRepository = DatabaseRepositoryImpl(context)

    @Provides
    @Singleton
    fun waterPumpsInteractor(): WaterPumpsInteractor = WaterPumpsInteractor()

    @Provides
    @Singleton
    fun  navigationHelper(pref: PreferencesRepository): NavigationHelper = NavigationHelper(context,pref)

    @Provides
    @Singleton
    fun provideFeedbackInteractor(server: ServerRepository): FeedbackInteractor = FeedbackInteractor(server)


    @Provides
    @Singleton
    fun provideBindCardInteractor(server: ServerRepository):BindCardInteractor =  BindCardInteractor(server)

    @Provides
    @Singleton
    fun privideBuyWaterInteractor(server: ServerRepository):BuyWaterInteractor = BuyWaterInteractor(server)
}