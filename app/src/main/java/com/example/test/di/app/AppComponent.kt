package com.example.test.di.app


import com.example.test.data.repository.preferencesRepository.PreferencesRepository
import com.example.test.cicerone.LocalCiceroneHolder
import com.example.test.common.NavigationHelper
import com.example.test.data.repository.string.StringProvider
import com.example.test.interactor.UserInteractor
import com.example.test.interactor.WaterPumpsInteractor
import com.example.test.mvp.authorisation_code.AuthorisationCodePresenter
import com.example.test.mvp.authorisation_phone.AuthorisationPhonePresenter

import com.example.test.mvp.profile.ProfilePresenter

import com.example.test.mvp.search_pump.pumps_list.PumpsListPresenter
import com.example.test.mvp.search_pump.pumps_map.PumpsMapPresenter
import com.example.test.mvp.splash_screen.SplashScreenPresenter

import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        LocalNavigationModule::class,
        RepositoryModule::class]
)
interface AppComponent {

    fun provideNavigatorHolder(): NavigatorHolder

    fun provideGlobalRouter(): Router

    fun provideStringProvider(): StringProvider

    fun provideCiceroneHolder(): LocalCiceroneHolder

    fun provideUserInteractor(): UserInteractor

    fun navigationHelper(): NavigationHelper

    fun waterPumpInteractor():WaterPumpsInteractor
    fun userInteractor():UserInteractor

    fun providePreferencesRepository(): PreferencesRepository
    fun inject(splashScreenPresenter: AuthorisationPhonePresenter)
    fun inject(splashScreenPresenter: SplashScreenPresenter)
    fun inject(authorisationCodePresenter: AuthorisationCodePresenter)
    fun inject(pumpsMapPresenter: PumpsMapPresenter)

    fun inject(waterPumpsInteractor: WaterPumpsInteractor)
    fun inject(profilePresenter: ProfilePresenter)
    fun inject(pumpsListPresenter: PumpsListPresenter)

     fun inject(userInteractor: UserInteractor)
}