package com.example.test.di

import android.content.Context
import com.example.test.di.app.AppComponent
import com.example.test.di.app.AppModule
import com.example.test.di.app.DaggerAppComponent
import com.example.test.di.app.RepositoryModule

object DaggerUtils {

    lateinit var appComponent: AppComponent

    @JvmStatic
    fun buildComponents(context: Context) {
        appComponent = buildAppComponent(context)
    }

    private fun buildAppComponent(context: Context): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule())
            .repositoryModule(RepositoryModule(context))
            .build()
    }
}