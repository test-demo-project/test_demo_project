package com.example.test.common.errors


import android.app.Activity
import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment

import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.common.errors.excetpions.WrongFieldException
import com.example.test.common.extension.showMessageDialog
import com.example.test.R
import com.example.test.data.server.UnauthorisedException
import com.example.test.data.server.model.BaseResponse
import com.example.test.di.DaggerUtils
import com.google.gson.Gson
import moxy.MvpView
import moxy.viewstate.MvpViewState
import moxy.viewstate.strategy.alias.OneExecution

import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.CancellationException



class ErrorCause : Exception {

    var messageResId: Int? = null

    var httpCode: Int? = null
        private set

    var isConnectException = false

    var params: List<Any>? = null

    companion object {

        const val ERROR_WRONG_ROLE = "ERROR_WRONG_ROLE"
        const val ERROR_UNAUTHORIZED = "ERROR_UNAUTHORIZED"
        const val ERROR_SILENT = "ERROR_SILENT"
        const val ERROR_USER_VERIFICATION = "ERROR_USER_VERIFICATION"

        fun createErrorCause(e: Throwable): ErrorCause = when (e) {
            is ErrorCause -> e
            is HttpException -> HttpErrorResponseParser().parse(e)
            is SocketTimeoutException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            is UnknownHostException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            is ConnectException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            else -> ErrorCause(R.string.error_unknown)
        }

    }

    fun isUnauthorized() = message == ERROR_UNAUTHORIZED

    constructor(message: String) : super(message)

    constructor(message: String, httpException: HttpException) : super(message, httpException) {
        this.httpCode = httpException.code()
    }

    constructor(httpException: HttpException) : super(httpException) {
        this.httpCode = httpException.code()
    }

    constructor(messageResId: Int) {
        this.messageResId = messageResId
    }

    constructor(messageResId: Int, list: List<Any>) {
        this.messageResId = messageResId
        this.params = list
    }

    fun getErrorCause(context: Context): String {
        val errorMessage = message

        if (errorMessage != null) {
            return errorMessage
        }

        val params = this.params

        if (params != null) {

            return String.format(context.getString(messageResId!!), *params.toTypedArray())
            //val i = params.map { it }


        } else {
            return context.getString(messageResId!!)
        }
    }

}

@OneExecution
fun MvpView.showMessage(errorCause: ErrorCause, reloadable: ((String) -> Unit)? = null) {

    /* if (errorCause.message == ERROR_SILENT) {
         return
     }*/

    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }

    if (errorCause.message == ErrorCause.ERROR_UNAUTHORIZED) {
        DaggerUtils.appComponent.provideUserInteractor().logoutLocal()
        DaggerUtils.appComponent.navigationHelper().openNextScreen()

     /*   val loginInteactor = DaggerUtils.appComponent.provideLoginInteractor()
        loginInteactor.clearTokensInCaceOfError()*/

        val context = when (view) {

            is Activity -> {
                view
            }
            is Fragment -> {
                view.context
            }
            else -> {
                null
            }
        }

        if (context != null) {
            /*val signInIntent = Intent(context, LogoActivity::class.java)
            signInIntent.singleTop()
            signInIntent.clearTask()
            context.startActivity(signInIntent)
            return*/
        }

        /*  val prefe = DaggerUtils.appComponent.providePreferencesRepository()
          prefe.removeUserData()
          val context = when (view) {

              is Activity -> {
                  view
              }
              is Fragment -> {
                  view.context
              }
              else -> {
                  null
              }
          }

          if (context != null) {
              val signInIntent = Intent(context, SignInActivity::class.java)
              signInIntent.flags = signInIntent.flags or ActivityUtils.getFlagsClearStack()
              context.startActivity(signInIntent)
              return
          }*/
    }

    val context: Context? = view?.getContext() ?: this.getContext()
    context?.let {

        val message = errorCause.getErrorCause(it)

        if (reloadable != null) {
            reloadable.invoke(message)
        } else {
            view?.showMessageDialog(message, R.string.continue_str)
        }
    }
}

fun MvpView.getContext(): Context? {
    return when (this) {
        is Activity -> this
        is Fragment -> this.activity
        else -> null
    }
}

fun <T : View> MvpView.getLayout(id: Int): T? {
    return when (this) {
        is Activity -> findViewById(id)
        is Fragment -> view?.findViewById(id)
        else -> null
    }
}

@OneExecution
fun MvpView.showMessage(e: Throwable, reloadable: ((String) -> Unit)? = null) {

    e.printStackTrace()


    val errorMessage = when (e) {
        is ErrorCause -> e
        is HttpException -> {
            when (e.code()) {

                401 -> ErrorCause(ErrorCause.ERROR_UNAUTHORIZED)
                403 -> ErrorCause(ErrorCause.ERROR_UNAUTHORIZED)
                else -> {
                    val bodyError = e.bodyError()

                    if(bodyError?.message != null){
                        val message = bodyError.message
                        ErrorCause(message,e)
                    }else {
                        ErrorCause(R.string.http_error_with_code, listOf(e.code()))
                    }
                }
            }
        }
        is UnauthorisedException->{
            ErrorCause(ErrorCause.ERROR_UNAUTHORIZED)
        }
        is SocketTimeoutException -> ErrorCause(R.string.error_socket_timeout_exception)
        is UnknownHostException -> ErrorCause(R.string.error_unknown_host_exception)
        is ConnectException -> ErrorCause(R.string.error_unknown_host_exception)
        is WrongFieldException -> {
            if (e.additionalData == null) {
                ErrorCause(e.resourceId)
            } else {
                ErrorCause(e.resourceId, e.additionalData!!)
            }

        }
        is ServiceException -> ErrorCause(e.messageId)
       /* is ServiceCallException -> {
            e.toErrorCause()
        }*/
        is CancellationException -> {
            return
        }
        /*is MediaPlayerDecorator.PlaySoundException -> {
            ErrorCause(e.toString())
        }*/

        else -> {
            val s = e.localizedMessage ?: e.message
            if (s != null) {
                ErrorCause(s)
            } else {
                ErrorCause(R.string.error_unknown)
            }
        }
    }

    this.showMessage(errorMessage, reloadable)
}
/*
fun ServiceCallException.toErrorCause(): ErrorCause {
    val message = message
    return if (message != null) ErrorCause(message!!) else ErrorCause(R.string.unknown_service_error)
}*/

/*
fun HttpException.parse(): ServiceCallException? {
    try {
        val body = this.response().errorBody()?.string()
        val obj = Gson().fromJson(body,
                BodyError::class.java)

        return ServiceCallException(BaseResult<Any>(false, Any(), obj.errors))
    } catch (e: Exception) {
        return null
    }
}
*/


fun HttpException.bodyError(): BaseResponse? {
    try {
        return Gson().fromJson(
            this.response()?.errorBody()?.string(),
            BaseResponse::class.java
        )
    } catch (e: Exception) {
        return null
    }
}
