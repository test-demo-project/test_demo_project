package com.balinasoft.common.errors

class ErrorResponse(

        val message: String

)

class Error(

        val code: String,

        val message: String

)
