package com.example.test.common.view.list.adapter

import android.view.ViewGroup
import com.example.test.R
import com.example.test.common.extension.visibleOrGone
import kotlinx.android.synthetic.main.layout_in_adapter_progress_bar.view.*
import java.util.*
import kotlin.collections.ArrayList

open class LoadingSmartAdapter(): CoolSmartAdapter(){

    private var items: LinkedList<Any> = LinkedList<Any>()

    var showLoading:Boolean = false
        set(value){
            field = value
            super.updateItems(getContent())
        }

    private data class ProgressBar(val show:Boolean): ISmartModel {
        override fun areItemsTheSame(other: Any): Boolean {
            return other is ProgressBar
        }
    }

    private class ProgressBarViewHolder(parent:ViewGroup): SimpleViewHolder<ProgressBar>(parent, R.layout.layout_in_adapter_progress_bar){
        override fun onBind(item: ProgressBar) {
            itemView.progress_bar.visibleOrGone(item.show)
        }
    }

    init {
        registerItem(ProgressBar::class.java){
            ProgressBarViewHolder(it)
        }
    }

    override fun updateItems(newList: List<Any>){
        this.items.clear()
        this.items.addAll(newList)
        super.updateItems(getContent())
    }

    private fun getContent():List<Any>{
        val readyList = ArrayList<Any>(items.size+1)
        readyList.addAll(items)
        readyList.add(ProgressBar(showLoading))
        return readyList
    }

    fun showProgressBarInAdapter(show: Boolean) {
        showLoading = show
    }
}