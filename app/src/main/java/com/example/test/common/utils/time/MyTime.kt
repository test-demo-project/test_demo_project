package com.example.test.common.utils.time

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class MyTime(val hour: Int, val minute: Int, val seconds: Int) : Parcelable {

    companion object {

        fun build(date: Date): MyTime {
            val calendar = Calendar.getInstance().also {
                it.time = date
            }
            val h = calendar.get(Calendar.HOUR_OF_DAY)
            val m = calendar.get(Calendar.MINUTE)
            val s = calendar.get(Calendar.SECOND)
            return MyTime(h, m, s)
        }

        fun build(millis: Long): MyTime {
            val seconds = (millis / 1000) % 60
            val minutes = (millis / (1000 * 60) % 60)
            val hours = (millis / (1000 * 60 * 60) % 24)

            return MyTime(hours.toInt(), minutes.toInt(), seconds.toInt())

        }
    }


    fun applyTimeOnCalendar(calendar: Calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, seconds)
    }

    fun convertToDate(): Date {
        val calendar = Calendar.getInstance().also {
            it.clear()
            applyTimeOnCalendar(it)
        }
        return calendar.time
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MyTime

        if (hour != other.hour) return false
        if (minute != other.minute) return false

        return true
    }

    override fun hashCode(): Int {
        var result = hour
        result = 31 * result + minute
        return result
    }

    override fun toString(): String {
        return "MyTime(hour=$hour, minute=$minute, seconds=$seconds)"
    }
}
