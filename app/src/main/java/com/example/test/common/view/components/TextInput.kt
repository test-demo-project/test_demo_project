package com.example.test.common.view.components

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.example.test.R
import com.example.test.common.extension.parseAsHtml
import com.example.test.common.extension.visibleOrGone
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.view_edit_text.view.*

open class TextInput: LinearLayout{

    private val NO_VALUE = -1
    private val DEFAULT_INPUT_TYPE = InputType.TYPE_CLASS_TEXT

    @ColorRes
    private val DEFAULT_BACKGROUND_COLOR = R.color.input_background_color_light

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){

        if(context!=null && attrs!=null){
            val arr = context.obtainStyledAttributes(attrs,R.styleable.TextInput)

            hint = arr.getString(R.styleable.TextInput_text_input_hint)
            image = arr.getResourceId(R.styleable.TextInput_text_input_icon,NO_VALUE)
            text = arr.getString(R.styleable.TextInput_android_text)
            inputType = arr.getInt(R.styleable.TextInput_android_inputType,DEFAULT_INPUT_TYPE)
            label = arr.getString(R.styleable.TextInput_text_input_label)
           // backgroundEditTextColor = arr.getColor(R.styleable.TextInput_text_input_box_background_color,ContextCompat.getColor(context,DEFAULT_BACKGROUND_COLOR))

            isRequired = arr.getBoolean(R.styleable.TextInput_text_input_is_required,false)



            arr.recycle()
        }

    }

    private val textInputLayer: TextInputLayout

    val editText: TextInputEditText

    init{
        View.inflate(context, R.layout.view_edit_text,this)

        textInputLayer = text_layer
        editText = edit_text
        isRequired = false
    }

    var isRequired:Boolean = false
        set(value){
            field = value
            invalidateLabel()
        }

    var hint: String? = null
        set(value){
            field = value
            edit_text.hint = value
        }

    var text:String? = null
        set(value){
            field = value
            editText.setText(value)
        }

    var inputType:Int? = null
        set(value){
            field = value
            editText.inputType = value?:DEFAULT_INPUT_TYPE
        }

    @DrawableRes
    var image:Int? = null
        set(value){
            field = value

            val drawable = if(value!=null && value!=NO_VALUE){
                ContextCompat.getDrawable(context,value)
            }else{
                null
            }

            editText.setCompoundDrawablesWithIntrinsicBounds(drawable,null,null,null)
        }

    var label: String? = null
        set(value){
            field = value

            invalidateLabel()
        }

    private fun invalidateLabel(){
        if(isRequired){
            hint_tv.text = context.getString(R.string.requ_field,label?:"").parseAsHtml()
        }else{
            hint_tv.text = label?:""
        }

        hint_tv.visibleOrGone(!label.isNullOrBlank())
    }



    /*@ColorInt
    var backgroundEditTextColor: Int = ContextCompat.getColor(context,DEFAULT_BACKGROUND_COLOR)
        set(value){
            field = value
            text_layer.boxBackgroundColor = value
        }*/

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        textInputLayer.isEnabled = enabled
        editText.isEnabled = enabled

        if(enabled){
            textInputLayer.setBoxBackgroundColorResource(R.color.input_background_color_light)
        }else{
            textInputLayer.setBoxBackgroundColorResource(R.color.input_background_color_dark)
        }
    }
}