package com.example.test.common.utils.location

interface ILocataionProvider{

    fun onPermissionChanged()
    fun getLocationUpdates(locationCallback: LocationCallback)
    fun removeLocationUpdates(listener: LocationCallback)
}