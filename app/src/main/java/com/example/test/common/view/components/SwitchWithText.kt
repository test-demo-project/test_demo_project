package com.example.test.common.view.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.widget.SwitchCompat
import com.example.test.R
import com.example.test.common.extension.onClick
import com.example.test.databinding.ViewSwitchWithTextBinding

class SwitchWithText : LinearLayout{

    private val binding: ViewSwitchWithTextBinding

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        initialize(context,attrs)
    }

    var text:String? = null
        set(value){
            field = value
            binding.text.text = value
            binding.switchView.contentDescription = value
        }

    private fun initialize(context:Context?,attrs: AttributeSet?){
        if(attrs!=null && context!=null){
            val arr = context.obtainStyledAttributes(attrs,R.styleable.SwitchWithText)

            text = arr.getString(R.styleable.SwitchWithText_switch_text)


            arr.recycle()
        }
    }

    fun getSwitch():SwitchCompat{
        return binding.switchView
    }

    init{
        binding = ViewSwitchWithTextBinding.inflate(LayoutInflater.from(context),this,true)

        onClick {
            binding.switchView.toggle()
        }
    }

}