package com.example.test.common.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    private val timeFormatter = SimpleDateFormat("mm:ss", Locale("ru"))

    //private val serverDateFormat = SimpleDateFormat("mm:ss", Locale("ru"))

    fun formatTime(timestamp: Long): String {
        val date = Date(timestamp)
        return timeFormatter.format(date)
    }
}