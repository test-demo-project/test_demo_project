package com.example.test.common.utils

import android.widget.EditText
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.Mask
import com.redmadrobot.inputmask.model.CaretString

class PhoneMaskApplyer() {

    companion object {
        val PHONE_LENGTH = 11
        val MASK = "+7 ([000]) [000]-[00]-[00]"

        fun checkPhone(rawPhone: String): Boolean {
            return Mask.getOrCreate(MASK, emptyList())
                    .apply(CaretString(rawPhone, rawPhone.length), true).complete
        }

        fun formatPhone(phone: String): String {
            return Mask.getOrCreate(MASK, emptyList())
                    .apply(CaretString(phone, phone.length), true).formattedText.string
        }

        fun formatPhoneForServer(phone: String): String {
            return phone.replace("\\D".toRegex(), "")
        }
    }

    fun applyPhoneStyle(editText: EditText) {
        val mask = MASK

        if (mask != null) {
            val listener = MaskedTextChangedListener(
                mask,
                true,
                editText, null, null
            )
            editText.addTextChangedListener(listener)
            editText.onFocusChangeListener = listener
           // editText.hint = listener.placeholder()
        }
    }


}

fun convertPhone(rawPhone: String): String {
    return rawPhone.replace("\\D".toRegex(), "")
}

