package com.example.test.common.view.components

import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.example.test.R
import com.example.test.common.extension.onClick
import com.example.test.common.utils.TextListener
import com.example.test.databinding.ViewSmsCodeInputBinding

class SmsCodeInput: LinearLayout{

    private val CODE_LENGTH = 4

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    private val digitTextViews: List<TextView>

    private val binding: ViewSmsCodeInputBinding

    val editText: EditText

    init{
        val inflater = LayoutInflater.from(context)

        binding = ViewSmsCodeInputBinding.inflate(inflater,this,true)

        editText = binding.textView

        val digitTextViews = ArrayList<TextView>()

        this.digitTextViews = digitTextViews

        for(i in 0 until CODE_LENGTH){

            val digitTextView = inflater.inflate(R.layout.view_sms_code_digit,this,false) as TextView

            binding.digitsLayout.addView(digitTextView)

            digitTextViews.add(digitTextView)
        }

        binding.textView.addTextChangedListener(TextListener{text->
            digitTextViews.forEachIndexed {index,textView->
                textView.text = text.getOrNull(index)?.toString()?:""
            }
        })

        binding.textView.filters = arrayOf(InputFilter.LengthFilter(CODE_LENGTH))

        onClick {
            editText.requestFocus()
        }

    }



}