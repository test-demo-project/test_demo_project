package com.example.test.common.extension

import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment






fun Fragment.showDialog(
        title: String,
        message: String,
        positivie:Int,
        negative:Int,
        onPositiveButtonClick: () -> Unit,
        onNegativeButtonClick: () -> Unit
) {
    val builder = AlertDialog.Builder(context!!)
    builder.setTitle(title)
            .setMessage(message)
            .setCancelable(false)
            .setNegativeButton(getString(positivie)) { dialog, _ ->
                onNegativeButtonClick.invoke()
                dialog.cancel()
            }
            .setPositiveButton(negative) { dialog, _ ->
                onPositiveButtonClick.invoke()
                dialog.cancel()
            }
    val alert = builder.create()
    alert.show()
}

fun Fragment.getArgumentsOrCreate(): Bundle {
    var args = arguments
    if (args == null) {
        args = Bundle()
        arguments = args
    }
    return args
}

fun Fragment.putInt(name: String, value: Int) {
    getArgumentsOrCreate()?.also {
        it.putInt(name, value)
    }
}

fun Fragment.putString(name: String, value: String) {
    getArgumentsOrCreate()?.also {
        it.putString(name, value)
    }
}

fun Fragment.putBool(name: String, value: Boolean) {
    getArgumentsOrCreate()?.also {
        it.putBoolean(name, value)
    }
}

fun Fragment.putParcelabele(name: String, value: Parcelable) {
    getArgumentsOrCreate()?.also {
        it.putParcelable(name, value)
    }
}