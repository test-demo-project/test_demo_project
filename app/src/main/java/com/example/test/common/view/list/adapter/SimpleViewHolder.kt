package com.example.test.common.view.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test.common.extension.CallBackK
import com.example.test.data.repository.string.StringProvider
import com.example.test.di.DaggerUtils

abstract class SimpleViewHolder<T>(view: View)
    : RecyclerView.ViewHolder(view), StringProvider by DaggerUtils.appComponent.provideStringProvider() {

    constructor(parent: ViewGroup, idLayout: Int) : this(
        LayoutInflater.from(parent.context).inflate(idLayout, parent, false)
    )

    var onClick: CallBackK<T>? = null

    fun removeClickListener() {
        itemView.setOnClickListener(null)
        itemView.isClickable = false
    }

    fun getItemView(): View {
        return itemView
    }

    abstract fun onBind(item: T)
}