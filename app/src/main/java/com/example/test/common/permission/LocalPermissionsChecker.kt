package com.example.test.common.permission

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.example.test.common.extension.CallBackK

class LocalPermissionsChecker(val activity: Activity, val requestCode: Int) : IPermissionChecker {

    class RequestdPermissions(
            val permision: Array<String>,
            val callback: CallBackK<Boolean>
    )

    private var lastRequestedPermissions: ArrayList<RequestdPermissions> = ArrayList()

    override fun checkPermissions(permissions: Array<String>): Boolean {
        var granted: Boolean = true

        for (element in permissions){
            val result = ActivityCompat.checkSelfPermission(activity,element)
            if(result!=PackageManager.PERMISSION_GRANTED){
                granted = false
                break
            }
        }

        return granted
    }

    private fun findNeededPermissions(requestedPermissions: Set<String>): List<RequestdPermissions> {
        return lastRequestedPermissions.filter {
            if (it.permision.size == requestedPermissions.size) {
                var eq = true
                it.permision.forEach {
                    eq = eq && requestedPermissions.contains(it)
                }
                eq
            } else {
                false
            }
        }
    }

    fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {

        if (requestCode == this.requestCode) {
            val last = this.lastRequestedPermissions

            val callbacks = findNeededPermissions(permissions.toSet())

            var isSuccess = true

            grantResults.forEach {
                if(it!=PackageManager.PERMISSION_GRANTED){
                    isSuccess = false
                }
            }

            callbacks.forEach {
                it.callback(isSuccess)
            }

            lastRequestedPermissions.removeAll(callbacks)
        }
    }

    override fun requestPermissions(permision: Array<String>, callback: CallBackK<Boolean>) {
        ActivityCompat.requestPermissions(
                activity,
                permision,
                requestCode
        )
        lastRequestedPermissions.add(
            RequestdPermissions(
                permision,
                callback
            )
        )
    }
}
