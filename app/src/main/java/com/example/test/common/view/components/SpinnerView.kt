package com.example.test.common.view.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.carrera.common.view.spinner.bindSpinner
import com.example.test.R
import com.example.test.common.binding.spinner.Selectable
import com.example.test.common.extension.visibleOrGone
import kotlinx.android.synthetic.main.view_spinner.view.*

class SpinnerView: LinearLayout{

    private val NO_VALUE = -1

    @DrawableRes
    private val DEFAULT_BACKGROUND_DRAWABLE = R.drawable.bg_spinner_dark

    var defaultValue: String? = null
        set(value){
            field = value
            //      update()
        }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){

        if(attrs!=null){
            val arr = context!!.obtainStyledAttributes(attrs, R.styleable.SpinnerView)

            label = arr.getString(R.styleable.SpinnerView_spinner_view_label)
            image = arr.getResourceId(R.styleable.SpinnerView_spinner_view_icon,NO_VALUE)
            defaultValue = arr.getString(R.styleable.SpinnerView_spinner_view_default)
            //spinnerBackgroundDrawable = ContextCompat.getDrawable(context,arr.getResourceId(R.styleable.SpinnerView_SpinnerView_spinner_view_box_background_drawable,DEFAULT_BACKGROUND_DRAWABLE))

            arr.recycle()
        }

    }

    init{
        View.inflate(context, R.layout.view_spinner,this)


        material_spinner_view.id = View.generateViewId()

        label = null
        image = null
    }

    fun getSpinner(): AppCompatSpinner {
        return material_spinner_view
    }

    @DrawableRes
    var image:Int? = null
        set(value){
            field = value

            val drawable = if(value!=null && value!=NO_VALUE){
                ContextCompat.getDrawable(context,value)
            }else{
                null
            }
            drawable_left.setImageDrawable(drawable)

           // material_spinner_view.setCompoundDrawablesWithIntrinsicBounds(drawable,null,null,null)
        }

    var label:String? = null
        set(value){
            field = value
            label_tv.text = value
            label_tv.visibleOrGone(!value.isNullOrBlank())
        }


 /*   var spinnerBackgroundDrawable: Drawable? = ContextCompat.getDrawable(context,DEFAULT_BACKGROUND_DRAWABLE)
        set(value){
            field = value
            getSpinner().setBackground(value)
            //spinner_wrapper.background = value

        }*/

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        getSpinner().isEnabled = enabled
        spinner_wrapper.isEnabled = enabled
        //editText.isEnabled = enabled
    }

   /* fun bind(selectable: Selectable<*>){
        getSpinner().bindSpinner(selectable,defaultValue)
    }*/

    fun bind(lifecycleOwner: LifecycleOwner, selectable: Selectable<*>){
        getSpinner().bindSpinner(lifecycleOwner,selectable,defaultValue)
    }
}
