package com.example.test.common.permission

import com.example.test.common.extension.CallBackK

interface IPermissionChecker {

    fun requestPermissions(permissions: Array<String>, callback: CallBackK<Boolean>)
    fun checkPermissions(permissions: Array<String>):Boolean

}

