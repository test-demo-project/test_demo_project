package com.example.test.common.interfaces

import com.github.terrakok.cicerone.Router

interface RouterProvider {
    fun getRouter(): Router?
}
