package com.example.test.common.utils

import kotlinx.coroutines.delay

suspend fun <T> mintTimeExecute(minTime: Long, job: suspend () -> T): T {
    val beforeJob = System.currentTimeMillis()

    val result = job()

    val afterJob = System.currentTimeMillis()

    val timeToExecute = afterJob - beforeJob

    val awaitTime = minTime - timeToExecute

    if (awaitTime > 0) {
        delay(awaitTime)
    }

    return result
}