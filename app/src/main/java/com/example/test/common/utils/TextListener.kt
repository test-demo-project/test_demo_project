package com.example.test.common.utils

import android.text.Editable
import android.text.TextWatcher

class TextListener(val onChanged: (String) -> Unit) : TextWatcher {

    override fun afterTextChanged(s: Editable) {
        onChanged(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }
}