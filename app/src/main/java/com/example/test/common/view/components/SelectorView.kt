package com.example.test.common.view.components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.example.test.R
import com.example.test.common.extension.CallBackKUnit
import com.example.test.common.extension.visibleOrGone
import kotlinx.android.synthetic.main.view_selector_view.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick


open class SelectorView: LinearLayout{

    private val NO_VALUE = -1

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        if(context!=null && attrs!=null){
            val arr = context.obtainStyledAttributes(attrs,R.styleable.SelectorView)

            hint = arr.getString(R.styleable.SelectorView_selector_view_hint)
            image = arr.getResourceId(R.styleable.SelectorView_selector_view_icon,NO_VALUE)
           // text = arr.getString(R.styleable.SelectorView_sele)

            label = arr.getString(R.styleable.SelectorView_selector_view_label)

            arr.recycle()
        }
    }

    var openScreen: CallBackKUnit? = null

    var label: String? = null
        set(value){
            field = value
            label_tv.text = value
            label_tv.visibleOrGone(!value.isNullOrBlank())
        }

    var hint: String? = null
        set(value){
            field = value
            edit_text.hint = value

        }

    @DrawableRes
    var image:Int? = null
        set(value){
            field = value

            val drawable = if(value!=null && value!=NO_VALUE){
                ContextCompat.getDrawable(context,value)
            }else{
                null
            }
            drawable_left.setImageDrawable(drawable)

            drawable_left.visibleOrGone(drawable!=null)
          //  edit_text.setCompoundDrawablesWithIntrinsicBounds(drawable,null,null,null)
        }

    init{
        View.inflate(context, R.layout.view_selector_view,this)

        text_layer.onClick {
            openScreen?.invoke()
        }

        label = null
        image = null
        edit_text.keyListener = null
        edit_text.onClick {
            openScreen?.invoke()
        }
    }

    var text:String? = null
        set(value){
            field = value
            edit_text.setText(value)
        }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        edit_text.isEnabled = enabled
        text_layer.isEnabled = enabled
    }

    fun setTextLayerBg(drawable: Drawable){
        text_layer.background = drawable
    }
}