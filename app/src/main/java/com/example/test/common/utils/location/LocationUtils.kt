package com.example.test.common.utils.location

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle

class LocationUtils(val context: Activity) : ILocataionProvider {

    private var locationUpdatesCallbacks = ArrayList<LocationCallback>()

    private var isListening: Boolean = false

    private var currentLocation: Location? = null

    private var gpsAvailable: Boolean = false
        set(value) {
            if (value != field) {
                field = value
                updateAvailable()
            }
        }

    private var networkAvailable: Boolean = false
        set(value) {
            if (value != field) {
                field = value
                updateAvailable()
            }
        }

    val isLocationAvailable: Boolean
        get() {
            return gpsAvailable || networkAvailable
        }

    private fun updateAvailable() {

        locationUpdatesCallbacks.forEach {
            try {
                it.locationAvailable(isLocationAvailable)
            } catch (e: Throwable) {
            }
        }
    }


    override fun onPermissionChanged() {
        updateLocationServiceConnection()
    }

    override fun getLocationUpdates(locationCallback: LocationCallback) {
        if (!locationUpdatesCallbacks.contains(locationCallback)) {
            locationUpdatesCallbacks.add(locationCallback)
        }

        updateLocationServiceConnection()

        currentLocation?.let {
            locationCallback.setLocation(it)
        }

        locationCallback.locationAvailable(isLocationAvailable)
    }

    override fun removeLocationUpdates(listener: LocationCallback) {
        locationUpdatesCallbacks.remove(listener)
        updateLocationServiceConnection()
    }

    private fun updateLocationServiceConnection() {
        if (locationUpdatesCallbacks.isEmpty()) {
            removeLocationUpdates()
        } else {
            requestLocationUpdates()
        }
    }

    private fun requestLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
        }
        val minTime = 20f
        val minDistance = 10f

        try {
            val locationManager =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                minTime.toLong(),
                minDistance,
                locationListener
            )
            locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                minTime.toLong(),
                minDistance,
                locationListener
            )

            networkAvailable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            gpsAvailable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

            var lastKnown = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)

            if (lastKnown != null) {
                onNewLocation(lastKnown)
            } else {
                lastKnown = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (lastKnown != null) {
                    onNewLocation(lastKnown)
                }
            }

            isListening = true
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            isListening = false
        }
    }

    private fun removeLocationUpdates() {
        try {
            val locationManager =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.removeUpdates(locationListener)
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
        } finally {
            isListening = false
        }
    }

    private fun onNewLocation(newLocation: Location) {
        currentLocation = newLocation

        locationUpdatesCallbacks.forEach {
            try {
                it.setLocation(newLocation)
            } catch (e: Throwable) {

            }
        }
    }


    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            onNewLocation(location)
        }

        override fun onProviderDisabled(provider: String) {
            onProviderChanged(provider, false)
        }

        override fun onProviderEnabled(provider: String) {
            onProviderChanged(provider, true)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

        }

        private fun onProviderChanged(provider: String, status: Boolean) {
            when (provider) {
                LocationManager.NETWORK_PROVIDER -> networkAvailable = status
                LocationManager.GPS_PROVIDER -> gpsAvailable = status
            }
        }
    }


}