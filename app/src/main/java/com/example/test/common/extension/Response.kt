package com.example.test.common.extension

import com.example.test.R
import com.example.test.common.errors.showMessage
import com.example.test.common.utils.mintTimeExecute
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import kotlinx.coroutines.*
import moxy.presenterScope
import java.lang.Exception


fun launchUI(
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
): Job {
    return GlobalScope.launch(Dispatchers.Main, start, block)
}

inline fun safeRequest(
    crossinline request: suspend CoroutineScope.() -> Unit,
    crossinline errorHandler: (error: Throwable) -> Unit,
    crossinline finally: () -> Unit
):Job {
    return launchUI {
        try {
            request()
        } catch (e: Throwable) {
            errorHandler.invoke(e)
        } finally {
            finally()
        }
    }
}

enum class LoadingHandler {
    NONE, PLACEHOLDER, DIALOG
}

fun <T : BaseScreenView> BaseScreenPresenter<T>.runWithDialog(
    message: String? = null,
    reloadable: Boolean = false,
    request: suspend CoroutineScope.() -> Unit,
    onFail:  ((e:Exception) -> Boolean)? = null,
    finally: (() -> Unit)? = null
) {

    viewState.showProgressBarDialog(true, message)

    presenterScope.launch {
        try {
            request()
        } catch (e: Exception) {
            if(onFail==null || onFail(e)==false)
            viewState.showMessage(e) {
                if (reloadable) {
                    viewState.showMessageDialog(
                        it,
                        positiveButtonId = R.string.try_again_bt_text,
                        onPositiveClicked = {
                            runWithDialog(message, reloadable, request, onFail, finally)
                        },
                        negativeButton = R.string.cancel, onNegativeClicked = {

                        }, cancelable = true
                    )
                } else {
                    viewState.showMessageDialog(
                        it,
                        positiveButtonId = R.string.ok, cancelable = true
                    )
                }
            }
        } finally {
            viewState.showProgressBarDialog(false, null)
        }
    }

}

fun <T : BaseScreenView> BaseScreenPresenter<T>.runWithPlaceholder(
    reloadable: Boolean = false,
    request: suspend CoroutineScope.() -> Unit,
    finally: () -> Unit
) {

    viewState.showProgressBar(true)
    viewState.showFullContentMessage(false, "")
    viewState.showFullContentMessageWithButton(false, "")

    presenterScope.launch {
        try {
            mintTimeExecute(650) {
                request()
            }
        } catch (e: Throwable) {

            viewState.showMessage(e) {
                if (reloadable) {
                    viewState.showFullContentMessageWithButton(
                        true,
                        it,
                        R.string.try_again_bt_text
                    ) {
                        runWithPlaceholder(reloadable, request, finally)
                    }
                } else {
                    viewState.showFullContentMessage(true, it)
                }
            }

        } finally {
            viewState.showProgressBar(false)
            finally()
        }
    }

}
