package com.example.test.common.placeholders
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import com.example.test.R
import com.example.test.common.extension.visibleOrGone

import kotlinx.android.synthetic.main.placeholder_progress_bar.view.*

class StandartProgressBarScreen(viewGroup: ViewGroup) :
        AbstractScreenViewHolder(R.layout.placeholder_progress_bar, viewGroup),
    ProgressBarScreenViewHolder {

    init {
    }

    fun setBackground(drawable: Drawable?) {
        m_view.progress_bar_screen_bg.background = drawable
    }

    override fun setMessage(message: String?) {
        m_view.progress_bar_message_tv.text = message
        m_view.progress_bar_message_tv.visibleOrGone(!message.isNullOrBlank())
    }
}