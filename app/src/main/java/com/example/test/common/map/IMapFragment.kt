package com.swapmap.mvp.base.abstract_map

import com.example.test.common.map.google.PumpItem
import com.example.test.common.map.model.MapBounds
import com.example.test.common.map.model.MapCoordinate


interface IMapFragment{
    fun moveMap(mapCoordinate: MapCoordinate, smooth:Boolean, zoom:Float? = null)
    fun setPumps(markers: List<PumpItem>)
    fun zoomBy(value:Float)
    fun setZoom(zoom: Float)

    fun setMapBounds(mapBounds: MapBounds,smooth: Boolean)

    fun onLocationAccessGranted()

    /*  fun setMapClickListener(callback: CallBackKUnit)
      fun setMapOnLongClickListener(callback: CallBackKUnit)*/
}