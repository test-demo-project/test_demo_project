package com.example.test.common.utils

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.View
import android.widget.TextView
import com.example.test.common.extension.CallBackK


class SpannableTextUtils(val textView: TextView) {


    var onTextClicked: CallBackK<String>? = null

    var isUnderlineText: Boolean = false

    var linkIsBold: Boolean = false

    var imageGetter: Html.ImageGetter? = null

    fun setText(html: String) {
        val sequence = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SpannableString(Html.fromHtml(html, 0, imageGetter, null))
        } else {
            val htmlToUse = html.replace("<li>", "<br><br>&#8226").replace("</li>", "</br>")
            Html.fromHtml(htmlToUse, imageGetter, null)
        }
        val strBuilder = SpannableStringBuilder(sequence)
        val urls = strBuilder.getSpans(0, sequence.length, URLSpan::class.java)
        for (span in urls) {
            makeLinkClickable(
                textView.context,
                strBuilder,
                span,
                isUnderlineText,
                linkIsBold,
                onTextClicked
            )
        }

        textView.text = strBuilder
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    companion object {

        fun setTextViewHTML(
            text: TextView,
            html: String,
            isUnderlineText: Boolean = false,
            onTextClicked: CallBackK<String>? = null
        ) {

            val util = SpannableTextUtils(text).also {
                it.isUnderlineText = isUnderlineText
                it.onTextClicked = onTextClicked
            }

            util.setText(html)
        }

        private fun makeLinkClickable(
            context: Context,
            strBuilder: SpannableStringBuilder,
            span: URLSpan,
            isUnderlineText: Boolean,
            linkIsBold: Boolean,
            onTextClicked: CallBackK<String>?
        ) {
            val start = strBuilder.getSpanStart(span)
            val end = strBuilder.getSpanEnd(span)

            val flags = strBuilder.getSpanFlags(span)
            val clickable =
                MyClickableSpan(context, span, isUnderlineText, linkIsBold, onTextClicked)
            //strBuilder.removeSpan()
            strBuilder.removeSpan(span)
            strBuilder.setSpan(clickable, start, end, flags)
        }

        class MyClickableSpan(
            val context: Context,
            val span: URLSpan,
            val isUnderlineText: Boolean,
            val linkIsBold: Boolean,
            val onTextClicked: CallBackK<String>?
        ) : ClickableSpan() {

            override fun onClick(view: View) {
                onTextClicked?.invoke(span.url)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = isUnderlineText

                // ds.setTypeface(Typeface.create(Typeface.DEFAULT,Typeface.NORMAL))
            }
        }
    }

}

