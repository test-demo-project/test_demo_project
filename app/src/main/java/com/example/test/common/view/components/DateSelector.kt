package com.example.test.common.view.components

import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.example.test.R
import com.example.test.common.extension.CallBackK
import com.example.test.common.utils.convertToReadable
import com.example.test.common.utils.time.MyDate
import java.util.*

class DateSelector: SelectorView {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    var isSpinnerMode:Boolean = false

    init{
        setTextLayerBg(ContextCompat.getDrawable(context, R.drawable.bg_input)!!)

        openScreen = {
            openDateDialog()
        }
    }

    var date: MyDate? = null
        set(value){
            if(value!=field){
                field = value
                onDateChanged?.invoke(value)
            }
            text = date?.convertToReadable()?:""
        }

    var onDateChanged: CallBackK<MyDate?>? = null

    private fun openDateDialog(){
        val date = date?: MyDate.build(Date())

        if(isSpinnerMode){
            DatePickerDialog(context,  R.style.MySpinnerDatePickerStyle,OnDateSelected(), date.year, date.month - 1, date.day).show()
        }else{
            DatePickerDialog(context,  OnDateSelected(), date.year, date.month - 1, date.day).show()
        }

    }

    inner class OnDateSelected() : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            val date = MyDate(year, month + 1, dayOfMonth)
            this@DateSelector.date = date
        }
    }

    fun bind(viewLifecycleOwner: LifecycleOwner,mutableLiveData: MutableLiveData<MyDate?>){
        mutableLiveData.observe(viewLifecycleOwner){
            date = it
        }

        onDateChanged = {
            if(mutableLiveData.value!=it){
                mutableLiveData.value = it
            }
        }
    }
}