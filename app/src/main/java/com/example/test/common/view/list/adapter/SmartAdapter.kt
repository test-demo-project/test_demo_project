package com.example.test.common.view.list.adapter


import androidx.recyclerview.widget.DiffUtil

abstract class SmartAdapter<T : ISmartModel> : SimpleAdapter<T>() {

    override fun updateItems(list: List<T>) {

        val diffCallback = DiffUtilCallback(items, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.items.clear()
        this.items.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

}
