package com.example.test.common.utils


import com.example.test.common.utils.TimeConverter.Companion.userDateFormatterShort
import com.example.test.common.utils.TimeConverter.Companion.userDayWithMonth
import com.example.test.common.utils.TimeConverter.Companion.userTimeFormatter
import com.example.test.common.utils.time.DateAndTime
import com.example.test.common.utils.time.MyDate
import com.example.test.common.utils.time.MyTime
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

class TimeConverter() {
    companion object {
        val userTimeFormatter = SimpleDateFormat("HH:mm", Locale("ru"))
        val userDateFormatterShort = SimpleDateFormat("dd.MM.yyyy", Locale("ru"))
        val userFullDateWidhDayOfWeek = SimpleDateFormat("dd MMMM, EEEE", Locale("ru"))

        val userDayWithMonth = SimpleDateFormat("dd MMMM", Locale("ru"))

        val userDateFormatterFull = SimpleDateFormat("dd MMMM yyyy", Locale("ru"))
        //val serverDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        val serverTimeFormat = SimpleDateFormat("HH:mm:ss", Locale.US)

        val dayOfWeekFormat = SimpleDateFormat("EEE", Locale("ru"))


        val SERVER_DATE_WITH_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

        //val serverDateWithTimeFormat = SimpleDateFormat("", Locale.US)

        /*  val serverTimeConverter = ServerTimeConverter()
          val serverDateConvereter = ServerDateConverter()*/

        fun formatTime(myTime: MyTime): String {
            return formatTime(myTime.convertToDate())
        }

        fun formatTime(date: Date): String {
            return userTimeFormatter.format(date)
        }

        private fun <T> List<T>.shiftLeft(): List<T> {
            val newList = ArrayList<T>(size)
            for (i in (0..size)) {
                var newIndex = (i + 1) % size
                if (newIndex < 0) {
                    newIndex = size - 1
                }
                newList.add(get(newIndex))
            }
            return newList
        }

        private fun getNames(locale: Locale, short: Boolean): List<String> {
            val dfs = DateFormatSymbols(locale)

            val weekdays: Array<String> = if (short) {
                dfs.shortWeekdays
            } else {
                dfs.weekdays
            }

            val removedZero = weekdays.filterIndexed() { index, s -> index != 0 }

            return removedZero.shiftLeft().map { it.capitalize() }
        }


        fun parseDateWithTime(dateWithTime: String): DateAndTime? {
            val format = SimpleDateFormat(SERVER_DATE_WITH_TIME_FORMAT, Locale.US)

            val date = format.parse(dateWithTime)

            if (date == null) {
                return null
            }
            return DateAndTime(MyDate.build(date), MyTime.build(date))
        }

    }


}

private fun buildDayOfWeek(date: MyDate): String {
    val d = date.convertToDate()
    return TimeConverter.dayOfWeekFormat.format(d)
}

/*private fun formatDate(context: Context, date: MyDate): SpannableString {
    val dateDate = date.convertToDate()
    val dayOfWeek = buildDayOfWeek(date) + ", "
    val spanable = SpannableStringBuilder()
    spanable.append(dayOfWeek.setSpan(ContextCompat.getColor(context, R.color.grey)))

    val dayAndMonth = TimeConverter.dayAndFullMonthFormat.format(dateDate)
    val dayAndMonthSpannable =
        dayAndMonth.setSpan(ContextCompat.getColor(context, R.color.black_light))

    spanable.append(dayAndMonthSpannable)

    return SpannableString(spanable)
}*/


fun MyDate.convertToReadable(): String {
    return userDateFormatterShort.format(this.convertToDate())
}

fun MyDate.convertToDayWithFullMonth(): String {
    return userDayWithMonth.format(this.convertToDate())
}

fun MyTime.convertToReadable(): String {
    return userTimeFormatter.format(this.convertToDate())
}