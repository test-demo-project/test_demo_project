package com.example.test.common.extension

import android.app.AlertDialog
import android.content.Context
import androidx.fragment.app.Fragment
import moxy.MvpView

import moxy.viewstate.MvpViewState
import moxy.viewstate.strategy.alias.OneExecution
import org.jetbrains.anko.alert
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast


@OneExecution
fun MvpView.showAlert(message: String): AlertDialog {
    val view = (this as MvpViewState<*>).views.find { it is Context || it is Fragment }

    return when (view) {
        is Context -> view.alert(message)
        is Fragment -> view.requireContext().alert(message)
        else -> null!!
    }.also {
        it.positiveButton(android.R.string.ok, {})
    }.show()
}

@OneExecution
fun MvpView.showMessage(message: String) {
    val view = (this as MvpViewState<*>).views.find { it is Context || it is Fragment }

    when (view) {
        is Context -> view.toast(message)
        is Fragment -> view.toast(message)
    }
}

@OneExecution
fun MvpView.showLongMessage(message: String) {
    val view = (this as MvpViewState<*>).views.find { it is Context || it is Fragment }

    when (view) {
        is Context -> view.longToast(message)
        is Fragment -> view.longToast(message)
    }
}

@OneExecution
fun MvpView.showMessage(message: Int) {
    val view = (this as MvpViewState<*>).views.find { it is Context || it is Fragment }

    when (view) {
        is Context -> view.toast(message)
        is Fragment -> view.toast(message)
    }
}