package com.example.test.common.view.list.item_decoration

import android.content.Context
import android.widget.LinearLayout
import androidx.core.content.ContextCompat


class SpaceItemDecoration(
    context: Context,
    drawableRes: Int,
    orientation: Int,
    showLast: Boolean = false
) :
    DividerItemDecoration(context, orientation, showLast) {


    companion object {
        val HORIZONTAL = LinearLayout.HORIZONTAL
        val VERTICAL = LinearLayout.VERTICAL

    }

    init {
        setDrawable(ContextCompat.getDrawable(context, drawableRes)!!)
    }
}
