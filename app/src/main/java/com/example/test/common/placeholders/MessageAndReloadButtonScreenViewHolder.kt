package com.example.test.common.placeholders

import com.example.test.common.extension.CallBackKUnit


interface MessageAndReloadButtonScreenViewHolder : ScreenViewHolder {
    fun setMessage(message: String)
    fun setPositiveButtonVisible(visible:Boolean)
    fun setNegativeButtonVisible(visible:Boolean)

    fun setPositiveButtonCallback(callback: CallBackKUnit)
    fun setNegativeButtonCallback(callback: CallBackKUnit)

    fun setPositiveButtonText(text: String)
    fun setNegativeButtonText(text:String)




}