package com.example.test.common.extension

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.test.R


import com.example.test.common.errors.getContext
import moxy.MvpView
import moxy.viewstate.MvpViewState


/*fun MvpView.showMessageDialog(
    message: String,
    positiveButtonId: Int,
    onPositiveClicked: (() -> Unit)? = null
) {
    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }

    val context: Context? = view?.getContext() ?: this.getContext()


    val dialog = AlertDialog.Builder(context!!)
            .setPositiveButton(positiveButtonId, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    onPositiveClicked?.invoke()
                }
            })
            .create()
    dialog.setMessage(message)
    dialog.show()
}*/

fun MvpView.showMessageDialog(
    message: String, positiveButtonId: Int,
    cancelable: Boolean = true,
    onPositiveClicked: (() -> Unit)? = null,
    negativeButton: Int? = null,
    onNegativeClicked: (() -> Unit)? = null,
    dangerous: Boolean = false
) {

    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }

    val context: Context? = view?.getContext() ?: this.getContext()

    val dialogBuilder = AlertDialog.Builder(context!!)

    dialogBuilder.setPositiveButton(positiveButtonId) { _, _ ->
        onPositiveClicked?.invoke()
    }

    if (negativeButton != null) {
        dialogBuilder.setNegativeButton(negativeButton) { _, _ ->
            onNegativeClicked?.invoke()
        }
    }

    dialogBuilder.setCancelable(cancelable)

    dialogBuilder.setMessage(message)


    val dialog = dialogBuilder.create()


    dialog.show()

    if(dangerous){
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(
            ContextCompat.getColor(
                context,
                R.color.error
            )
        )
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(
            ContextCompat.getColor(
                context,
                R.color.text_color
            )
        )
    }



}
