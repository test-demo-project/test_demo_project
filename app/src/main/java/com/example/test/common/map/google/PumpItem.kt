package com.example.test.common.map.google

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class PumpItem(
    val id:Int,
    position: LatLng
): ClusterItem{

    private val _position: LatLng = position

    override fun getPosition(): LatLng {
       return _position
    }

    override fun getTitle(): String? {
        return null
    }

    override fun getSnippet(): String? {
        return null
    }
}