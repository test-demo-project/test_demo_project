package com.example.test.common.placeholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class AbstractScreenViewHolder(layoutId: Int, viewGroup: ViewGroup) :
    ScreenViewHolder {

    protected val m_view: View

    override fun getView(): View {
        return m_view
    }

    init {
        val context = viewGroup.context
        m_view = LayoutInflater.from(context).inflate(layoutId, viewGroup, false)
    }

    override fun visibilityChanged(visible: Boolean) {

    }
}