package com.example.test.common.utils.location

import android.location.Location

interface LocationCallback {
    fun setLocation(location: Location)
    fun locationAvailable(available:Boolean){}
}