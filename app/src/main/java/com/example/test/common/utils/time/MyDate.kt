package com.example.test.common.utils.time

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class MyDate(val year: Int, val month: Int, val day: Int) : Parcelable {

    fun convertToDate(): Date {
        val calendar = Calendar.getInstance().also {
            it.clear()
            applyDatesToCalendar(it)
        }
        return calendar.time
    }

    fun applyDatesToCalendar(calendar: Calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, day)
        calendar.set(Calendar.MONTH, month - 1)
        calendar.set(Calendar.YEAR, year)
    }

    companion object {
        fun build(date: Date): MyDate {
            val calendar = Calendar.getInstance().also {
                it.time = date
            }

            return MyDate(
                day = calendar.get(Calendar.DAY_OF_MONTH),
                month = calendar.get(Calendar.MONTH) + 1,
                year = calendar.get(Calendar.YEAR)
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MyDate

        if (day != other.day) return false
        if (month != other.month) return false
        if (year != other.year) return false

        return true
    }

    override fun hashCode(): Int {
        var result = day
        result = 31 * result + month
        result = 31 * result + year
        return result
    }

    override fun toString(): String {
        return "MyDate(year=$year, month=$month, day=$day)"
    }


}
