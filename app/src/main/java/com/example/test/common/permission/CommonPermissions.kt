package com.example.test.common.permission

import android.Manifest


class CommonPermissions{

        companion object{
                var IMAGE_PERMISSIONS = listOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                )

                var LOCATION_PERMISSION = listOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                )
        }


}

