package com.example.test.common.map.google

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import com.example.test.common.map.AbstractMapWrapperFragment
import com.google.android.gms.maps.*
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.*

import com.swapmap.mvp.base.abstract_map.IMapFragment
import com.example.test.common.map.model.MapBounds
import com.example.test.common.map.model.MapCoordinate
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.example.test.R
import com.google.android.gms.maps.model.*

import java.util.*

class GoogleMapFragment() : AbstractMapWrapperFragment(), OnMapReadyCallback, IMapFragment {

    private val markers = Hashtable<ClusterItem, Marker>()

    private lateinit var clusterManager: ClusterManager<PumpItem>


    init {

    }

    private val tasksList = LinkedList<Task>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_google_map_fragment, container, false)
    }


    private var googleMap: GoogleMap? = null

    val mapFragment: SupportMapFragment by lazy { childFragmentManager.findFragmentByTag("fragment_map") as SupportMapFragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(savedInstanceState==null){
            mapFragment.getMapAsync(this)
        }
    }

    private fun execute(task: Task) {
        val map = googleMap
        if (map == null) {
            val strategy = task.strategy
            when (strategy) {
                TaskExecutionStrategy.ADD_TO_END_SINGLE -> {
                    val sameTask = tasksList.find { it.javaClass.name == task.javaClass.name }
                    if (sameTask != null) {
                        tasksList.remove(sameTask)
                    }
                    tasksList.add(task)
                }
                TaskExecutionStrategy.ADD_TO_END -> {
                    tasksList.add(task)
                }
            }

        } else {
            executeOnGoogleMap(map, task)
        }
    }

    private fun executeTasks() {
        val map = googleMap
        if (map == null) return
        tasksList.forEach {
            executeOnGoogleMap(map, it)
        }
        tasksList.clear()
    }


    private fun executeOnGoogleMap(map: GoogleMap, task: Task) {
        when (task) {
            is MoveMapTask -> {
                if (task.smooth) {
                    if (task.zoom == null) {
                        map.animateCamera(
                            CameraUpdateFactory.newLatLng(task.coordinate.toLatLng()),
                            200,
                            null
                        )
                    } else {
                        map.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                task.coordinate.toLatLng(),
                                task.zoom
                            ),
                            200,
                            null
                        )
                    }

                } else {
                    if (task.zoom == null) {
                        map.moveCamera(CameraUpdateFactory.newLatLng(task.coordinate.toLatLng()))
                    } else {
                        map.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                task.coordinate.toLatLng(),
                                task.zoom
                            )
                        )
                    }
                }
            }
            is SetMarkers -> {
                setMarkers(map, task.markers)
            }
            is SetZoom -> {
                setZoom(map, task.zoom)
            }
            is SetMapBounds -> {
                setMapBounds(map, task)
            }
            is LocationPermissionGrandes->{
                showMyLocationOnTheMap(map)
            }
        }
    }

    private fun setMapBounds(googleMap: GoogleMap, setMapBounds: SetMapBounds) {
        val b = setMapBounds.mapBounds

        val updateCamera = CameraUpdateFactory.newLatLngBounds(
            LatLngBounds(
                b.bottomLeft.toLatLng(), b.topRight.toLatLng()
            ), 50
        )

        if (setMapBounds.smooth) {
            googleMap.animateCamera(updateCamera)
        } else {
            googleMap.moveCamera(updateCamera)
        }
    }

    private fun setZoom(googleMap: GoogleMap, zoom: Float) {
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(zoom))
    }

    private fun setMarkers(googleMap: GoogleMap, mapMarkers: List<PumpItem>) {

        clusterManager.clearItems()

        clusterManager.addItems(mapMarkers)

        clusterManager.cluster()

        /*  val newHashSet = HashSet(mapMarkers)

          val toAdd = HashSet(mapMarkers.filter {!markers.containsKey(it) })

          val toRemove = markers.filter { !newHashSet.contains(it.key) }

          toRemove.forEach {
              it.value?.remove()
              markers.remove(it.key)
          }

          toAdd.forEach {
              addMarker(googleMap, it)
          }*/

    }

    private fun removeMarker() {}

    override fun onDestroy() {
        cleanMarkers()
        super.onDestroy()
    }


    private fun cleanMarkers() {
        markers.forEach {
            it.value.remove()
        }
        markers.clear()
    }

    private fun addMarker(googleMap: GoogleMap, mapMarker: PumpItem) {


        /*  val eventPinDrawing = EventPinDrawing(requireContext(), mapMarker.isMyEvent, null)
          val position = mapMarker.position.toLatLng()
          val markerOptions = MarkerOptions()
              .position(position)
              .icon(BitmapDescriptorFactory.fromBitmap(eventPinDrawing.createBitmap()))

          val marker = googleMap.addMarker(markerOptions)
          marker.tag = mapMarker
          markers[mapMarker] = marker

          if(mapMarker.image!=null){
              ImageLoader.loadDrawableWithCallback(requireContext(), mapMarker.image) {
                  //Check if marker were not deleted or destroyed
                  if(markers.containsKey(mapMarker) && markers.get(mapMarker)==marker){
                      val eventPinDrawingWithImage = EventPinDrawing(requireContext(), mapMarker.isMyEvent, it)
                      marker.setIcon(BitmapDescriptorFactory.fromBitmap(eventPinDrawingWithImage.createBitmap()))
                  }
              }
          }*/
    }

    private var currentMovingReason: Int? = null

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(googleMap: GoogleMap?) {
        if(this.googleMap!=null){
            return
        }

        this.googleMap = googleMap

        if (googleMap != null) {

            clusterManager = ClusterManager(context, googleMap)

            clusterManager.renderer =
                PumpsClusterRenderer(requireContext(), googleMap, clusterManager)

            googleMap.setOnCameraMoveStartedListener { reason ->
                currentMovingReason = reason
            }

            googleMap.setOnCameraMoveListener(object : GoogleMap.OnCameraMoveListener {
                override fun onCameraMove() {
                    val bounds = googleMap.projection.visibleRegion.latLngBounds
                    this@GoogleMapFragment.currentBounds.value =
                        MapBounds(
                            MapCoordinate(bounds.southwest.latitude, bounds.southwest.longitude),
                            MapCoordinate(bounds.northeast.latitude, bounds.northeast.longitude)
                        )

                    onTargetPositionChanged?.invoke(googleMap.cameraPosition.target.toMapCoordinate())
                    onBoundsChanged?.invoke(this@GoogleMapFragment.currentBounds.value!!)
                }
            })

            googleMap.setOnCameraIdleListener {
                val reason = currentMovingReason
                if (reason != null) {
                    if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                        onTargetPositionChangedByUser?.invoke(googleMap.cameraPosition.target.toMapCoordinate())
                        val bounds = googleMap.projection.visibleRegion.latLngBounds
                        onBoundsChangedByUser?.invoke(
                            MapBounds(
                                MapCoordinate(
                                    bounds.southwest.latitude,
                                    bounds.southwest.longitude
                                ),
                                MapCoordinate(bounds.northeast.latitude, bounds.northeast.longitude)
                            )
                        )
                    }
                }
                currentMovingReason = null
                clusterManager.onCameraIdle()
            }

            //clusterManager.onCameraIdle()

            googleMap.setOnCameraMoveCanceledListener {
                currentMovingReason = null
            }

            googleMap.setOnMarkerClickListener(clusterManager)

            clusterManager.setOnClusterItemClickListener(object :
                ClusterManager.OnClusterItemClickListener<PumpItem> {
                override fun onClusterItemClick(item: PumpItem?): Boolean {
                    if (item != null) {
                        markerClickListener?.invoke(item)
                        return true
                    } else {
                        return false
                    }
                }
            })


        }

        executeTasks()
    }

    private fun showMyLocationOnTheMap(googleMap: GoogleMap){
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        if (googleMap != null && !googleMap.isMyLocationEnabled) {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = false
        }
    }

    override fun onLocationAccessGranted() {
        execute(LocationPermissionGrandes())
    }

    private fun LatLng.toMapCoordinate(): MapCoordinate {
        return MapCoordinate(latitude, longitude)
    }

    override fun moveMap(mapCoordinate: MapCoordinate, smooth: Boolean, zoom: Float?) {
        execute(MoveMapTask(mapCoordinate, smooth, zoom))
    }

    override fun setPumps(markers: List<PumpItem>) {
        execute(SetMarkers(markers))
    }

    override fun zoomBy(value: Float) {
        val camera = CameraUpdateFactory.zoomBy(value)
        googleMap?.animateCamera(camera)
    }

    private enum class TaskExecutionStrategy {
        ADD_TO_END, ADD_TO_END_SINGLE
    }

    override fun setZoom(zoom: Float) {
        execute(SetZoom(zoom))
    }

    override fun setMapBounds(mapBounds: MapBounds, smooth: Boolean) {
        execute(SetMapBounds(mapBounds, smooth))
    }

    abstract private class Task(val strategy: TaskExecutionStrategy = TaskExecutionStrategy.ADD_TO_END_SINGLE) {}

    private class MoveMapTask(
        val coordinate: MapCoordinate,
        val smooth: Boolean,
        val zoom: Float? = null
    ) : Task()

    private class SetMarkers(val markers: List<PumpItem>) : Task()
    private class SetZoom(val zoom: Float) : Task()
    private class SetMapBounds(val mapBounds: MapBounds, val smooth: Boolean) : Task()
    private class LocationPermissionGrandes():Task()
}