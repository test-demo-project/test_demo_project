package com.example.test.common.placeholders

import android.view.ViewGroup

interface ViewHolderBuilder<T : ScreenViewHolder> {
    fun provide(viewGroup: ViewGroup): T
}

class ViewHolderBuilderImpl<T : ScreenViewHolder>(val builder: (viewGroup: ViewGroup) -> T) :
    ViewHolderBuilder<T> {
    override fun provide(viewGroup: ViewGroup): T {
        return builder(viewGroup)
    }
}
