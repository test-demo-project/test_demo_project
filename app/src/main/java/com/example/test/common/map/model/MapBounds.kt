package com.example.test.common.map.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MapBounds(
    val bottomLeft: MapCoordinate,
    val topRight: MapCoordinate
):Parcelable