package com.example.test.common.view.view_pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TitledViewPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var fragments = ArrayList<Item>()

    class Item(
        val fragment: Fragment,
        val title: String
    )

    fun setFragments(list: List<Item>) {
        fragments.clear()
        fragments.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return fragments.get(position).fragment
    }

    override fun getCount(): Int {
        return fragments.count()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragments[position].title
    }
}
