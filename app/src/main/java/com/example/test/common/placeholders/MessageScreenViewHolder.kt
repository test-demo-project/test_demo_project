package com.example.test.common.placeholders

interface MessageScreenViewHolder : ScreenViewHolder {
    fun setMessage(message: String)
}