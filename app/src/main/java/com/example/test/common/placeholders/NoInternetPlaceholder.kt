package com.example.test.common.placeholders

import android.view.ViewGroup


import com.example.test.R
import com.example.test.common.extension.CallBackKUnit
import com.example.test.common.extension.onClick
import com.example.test.common.extension.visibleOrGone

import kotlinx.android.synthetic.main.placeholder_error.view.*

class NoInternetPlaceholder(viewGroup: ViewGroup) :
        AbstractScreenViewHolder(R.layout.placeholder_error, viewGroup), MessageAndReloadButtonScreenViewHolder {

    private var positiveButtonCallback: CallBackKUnit? = null

    override fun setPositiveButtonCallback(callback: CallBackKUnit) {
        positiveButtonCallback = callback
    }

    override fun setMessage(message: String) {
        getView().errorMessageText.text = message
    }

    override fun setPositiveButtonText(text: String) {
        getView().positive_button.text = text
    }

    init {
        getView().positive_button.onClick {
            positiveButtonCallback?.invoke()
        }
    }

    override fun setPositiveButtonVisible(visible: Boolean) {
        getView().positive_button.visibleOrGone(visible)
    }

    override fun setNegativeButtonCallback(callback: CallBackKUnit) {
      getView().negative_button.onClick {
          callback?.invoke()
      }
    }

    override fun setNegativeButtonText(text: String) {
        getView().negative_button.text = text
    }

    override fun setNegativeButtonVisible(visible: Boolean) {
        getView().negative_button.visibleOrGone(visible)
    }
}