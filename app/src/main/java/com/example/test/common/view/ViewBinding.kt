package com.example.test.common.view

import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.test.R
import com.example.test.common.utils.MoneyValueFilter
import com.example.test.common.utils.TextListener
import com.google.android.material.slider.RangeSlider

import java.text.NumberFormat
import kotlin.collections.ArrayList

/*
class BindingTag(
    val textListener: TextWatcher?,
    val observer:  Observer<out Any>
){
    val observerOut: Observer<Any> = observer
}
*/


fun TextView.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<String>) {
    val listener = getTag(R.id.binding_tag)
    if (listener is TextListener) {
        removeTextChangedListener(listener)
    }
    observableField.removeObservers(lifecycleOwner)
    setText(observableField.value ?: "")
    val newListener = TextListener {
        observableField.value = (it)
    }
    observableField.observe(lifecycleOwner, Observer {
        if (it != text.toString()) setText(it ?: "")
    })
    addTextChangedListener(newListener)
    setTag(R.id.binding_tag, newListener)
}


fun EditText.bindInt(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Int?>) {

    val listener = getTag(R.id.binding_tag)
    if (listener is TextListener) {
        removeTextChangedListener(listener)
    }
    observableField.removeObservers(lifecycleOwner)

    val value = observableField.value?.toString()

    setText(value)

    setSelection(value?.length?:0)

    val newListener = TextListener {
        val intValue  = it.toIntOrNull()
        observableField.value = intValue
    }

    observableField.observe(lifecycleOwner, Observer {

        if(it != text.toString().toIntOrNull()){
            if(it!=null){
                setText(it.toString())
            }else{
                setText("")
            }
        }
    })

    addTextChangedListener(newListener)
    setTag(R.id.binding_tag, newListener)
}

fun EditText.bindFloat(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Float?>,limitFractionalPart:Int? = null) {

    val listener = getTag(R.id.binding_tag)
    if (listener is TextListener) {
        removeTextChangedListener(listener)
    }
    observableField.removeObservers(lifecycleOwner)

    val format = if(limitFractionalPart!=null){
        "%.${limitFractionalPart}f"
    }else{
        "%.f"
    }

    getTag(R.id.fraction_filter)?.let { filter->
        if(filter is MoneyValueFilter){
            val a = ArrayList(filters?.toList()?: listOf())
            a.remove(filter)
            filters = a.toTypedArray()
        }
    }

    if(limitFractionalPart!=null){
        filters+=MoneyValueFilter()
            .also { it.setDigits(limitFractionalPart)}
    }

    val currentValue = observableField.value

    val value = currentValue?.formatFloat(limitFractionalPart)?:""

    setText(value)

    setSelection(text.toString().length)

    val newListener = TextListener {
        observableField.value = it.toFloatOrNull()
    }

    observableField.observe(lifecycleOwner, Observer {
        if(it != text.toString().toFloatOrNull()){
            setText(it?.formatFloat(limitFractionalPart)?:"")
        }
    })

    addTextChangedListener(newListener)
    setTag(R.id.binding_tag, newListener)
}

private fun Float.formatFloat(fractionalPart:Int?):String{
    val nf = NumberFormat.getNumberInstance();

    nf.minimumFractionDigits = 0
    if(fractionalPart!=null){
        nf.maximumFractionDigits = fractionalPart
    }
    return nf.format(this).replace(',','.')
}


fun EditText.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<String>) {
    val listener = getTag(R.id.binding_tag)
    if (listener is TextListener) {
        removeTextChangedListener(listener)
    }
    observableField.removeObservers(lifecycleOwner)
    setText(observableField.value ?: "")
    setSelection(observableField.value?.length ?: 0)
    val newListener = TextListener {
        observableField.value = (it)
    }
    observableField.observe(lifecycleOwner, Observer{
        if (it != text.toString()){
            setText(it ?: "")
            //setSelection(it.length)
        }
    })
    addTextChangedListener(newListener)
    setTag(R.id.binding_tag, newListener)
}

fun MutableLiveData<String>.isValueBlank(): Boolean {
    return value.isNullOrBlank()
}

fun SwitchCompat.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Boolean>) {
    isChecked = observableField.value ?: false

    observableField.observe(lifecycleOwner, Observer {
        if (it != isChecked) isChecked = it
    })

    setOnCheckedChangeListener { buttonView, isChecked ->
        observableField.value = isChecked
    }
}

fun CheckBox.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Boolean>) {
    isChecked = observableField.value ?: false

    observableField.observe(lifecycleOwner, Observer  {
        if (it != isChecked) isChecked = it
    })

    setOnCheckedChangeListener { buttonView, isChecked ->
        observableField.value = isChecked
    }
}

fun RadioButton.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Boolean>) {
    isChecked = observableField.value ?: false

    observableField.observe(lifecycleOwner, Observer  {
        if (it != isChecked) isChecked = it
    })

    setOnCheckedChangeListener { buttonView, isChecked ->
        if(observableField.value!=isChecked){
            observableField.value = isChecked
        }
    }
}

fun RangeSlider.bind(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<List<Float>>) {
    if (observableField.value?.isNotEmpty() == true) values = observableField.value!!

    observableField.observe(lifecycleOwner, Observer {
        values.clear()
        if (it?.isNotEmpty() == true) values.addAll(it)
    })

    addOnChangeListener { slider, value, fromUser ->
        observableField.value = slider.values
    }
}

/*fun TabLayout.bindPosition(lifecycleOwner: LifecycleOwner, observableField: MutableLiveData<Int>) {
    if (selectedTabPosition != observableField.value) {
        selectTab(getTabAt(observableField.value ?: 0))
    }
    observableField.observe(lifecycleOwner) {
        if (selectedTabPosition != observableField.value) {
            selectTab(getTabAt(observableField.value ?: 0))
        }
    }
    addOnTabSelectedListener { observableField.value = it.position }
}*/

/** Extensions for LiveData */
fun LiveData<String>.get() = value ?: ""
fun LiveData<Boolean>.get() = value ?: false
fun LiveData<Int>.get() = value ?: 0