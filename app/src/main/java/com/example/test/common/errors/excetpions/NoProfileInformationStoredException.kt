package com.example.test.common.errors.excetpions

class NoProfileInformationStoredException() :
        IllegalStateException("No profile information stored ")