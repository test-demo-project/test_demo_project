package com.example.test.common.view.components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.example.test.R
import com.example.test.common.extension.visibleOrGone
import kotlinx.android.synthetic.main.view_text_info.view.*

class TextInfo: LinearLayout{

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){

        if(context!=null && attrs!=null){
            val arr = context.obtainStyledAttributes(attrs,R.styleable.TextInfo)

            icon = arr.getDrawable(R.styleable.TextInfo_text_info_icon)
            text = arr.getString(R.styleable.TextInfo_text_info_text)

            arr.recycle()
        }

    }

    init{
        View.inflate(context, R.layout.view_text_info,this)
    }

    var icon: Drawable?
        set(value) {
            view_text_info_icon.visibleOrGone(value != null)
            view_text_info_icon.setImageDrawable(value)
        }
        get() = view_text_info_icon.drawable

    var text: String? = null
        set(value){
            field = value
            view_text_info_text.text = value
        }

}