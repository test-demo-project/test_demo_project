package com.example.test.common.view.components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.example.test.R
import com.example.test.common.extension.dp
import kotlinx.android.synthetic.main.view_main_screen_button.view.*

class MainScreenButton: CardView{
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        if(attrs!=null){
            val arr = context.obtainStyledAttributes(attrs,R.styleable.MainScreenButton)

            text = arr.getString(R.styleable.MainScreenButton_main_screen_button_text)
            image = arr.getResourceId(R.styleable.MainScreenButton_main_screen_button_image,-1).let {
                if(it!=-1){
                    ContextCompat.getDrawable(context,it)
                }else{
                    null
                }
            }

            arr.recycle()
        }
    }



    var image:Drawable? = null
        set(value){
            field = value
            button_image.setImageDrawable(value)
        }

    var text:String? = null
        set(value){
            field = value
            button_text.text = value
        }

    init{
        View.inflate(context, R.layout.view_main_screen_button,this)

        cardElevation = 5.dp.toFloat()
        radius = 5.dp.toFloat()
        setBackgroundColor(ContextCompat.getColor(context,R.color.white))





    }
}