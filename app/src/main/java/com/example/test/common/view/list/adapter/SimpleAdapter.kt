package com.example.test.common.view.list.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test.common.extension.CallBackK

abstract class SimpleAdapter<T>() : RecyclerView.Adapter<SimpleViewHolder<T>>() {

    var onItemClicked: CallBackK<T>? = null

    protected val items = ArrayList<T>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder<T> {
        return buildViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: SimpleViewHolder<T>, p1: Int) {
        val item = items[p1]
        p0.onBind(item)
        /*onItemClicked?.let { callback ->
            p0.itemView.onClick {
                callback.invoke(item)
            }
        }*/
    }

    abstract fun buildViewHolder(parent: ViewGroup): SimpleViewHolder<T>

    open fun updateItems(list: List<T>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun updateItem(position: Int, item: T) {
        items[position] = item
        notifyItemChanged(position)
    }

    fun addItem(position: Int) {
        notifyItemInserted(position)
    }

    fun updateItem(item: T) {
        val index = items.indexOf(item)
        if(index!=-1){
            notifyItemChanged(index)
        }
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)
    }

    fun removeItem(item: T) {
        removeItem(items.indexOf(item))
    }
}
