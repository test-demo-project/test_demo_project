package com.example.test.common.map

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.example.test.common.extension.CallBackK
import com.example.test.common.map.google.PumpItem
import com.swapmap.mvp.base.abstract_map.IMapFragment
import com.example.test.common.map.model.MapBounds
import com.example.test.common.map.model.MapCoordinate


abstract class AbstractMapWrapperFragment() : Fragment(), IMapFragment {

    val currentBounds = MutableLiveData<MapBounds?>()


    //var mapClickListener: CallBackK<MapCoordinate>? = null

    //var mapLongClickListener: CallBackK<MapCoordinate>? = null

    var markerClickListener: CallBackK<PumpItem>? = null

    var onTargetPositionChanged:CallBackK<MapCoordinate>? = null

    var onTargetPositionChangedByUser:CallBackK<MapCoordinate>? = null

    var onBoundsChanged: CallBackK<MapBounds>? = null

    var onBoundsChangedByUser: CallBackK<MapBounds>? = null

    /*  abstract fun execute(task: Task)
  */
    override fun onDestroy() {
        super.onDestroy()
    }

}