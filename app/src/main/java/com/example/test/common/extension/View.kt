package com.example.test.common.extension

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.view.View
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.example.test.common.utils.ImageLoader


fun View.onClick(l: (v: View?) -> Unit) {
    setOnClickListener(l)
}

fun View.onLongClick(l: (v: android.view.View?) -> Boolean) {
    setOnLongClickListener(l)
}

fun View.visibleOrGone(visibility: Boolean) {
    this.visibility = if (visibility) View.VISIBLE else View.GONE
}

fun View.visibleOrInvisible(visibility: Boolean) {
    this.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}


fun View.changeBgColor(@ColorInt color: Int) {

    val mContext = context

    if (background is ShapeDrawable) { // cast to 'ShapeDrawable'
        val shapeDrawable = background as ShapeDrawable
        shapeDrawable.paint.color = color
    } else if (background is GradientDrawable) { // cast to 'GradientDrawable'
        val gradientDrawable = background as GradientDrawable
        gradientDrawable.setColor(color)
    } else if (background is ColorDrawable) { // alpha value may need to be set again after this call
        val colorDrawable = background as ColorDrawable
        colorDrawable.color = color
    }

}

fun View.changeBgColorRes(@ColorRes color: Int) {
    val res = ContextCompat.getColor(context, color)
    changeBgColor(res)
}

fun ImageView.load(url: String, placeholder: Int? = null) {
    ImageLoader.load(url, this, placeholder)
}