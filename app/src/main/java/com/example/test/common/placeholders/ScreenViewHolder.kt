package com.example.test.common.placeholders

import android.view.View

interface ScreenViewHolder {
    fun getView(): View
    fun visibilityChanged(visible: Boolean)
}