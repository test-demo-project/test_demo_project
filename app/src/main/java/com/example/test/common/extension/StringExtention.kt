package com.example.test.common.extension

import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.Spanned

fun String.parseAsHtml(): Spanned {

    val sequence = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        SpannableString(Html.fromHtml(this, 0))
    } else {

        Html.fromHtml(this)
    }

    return sequence

}

fun String.elepsizeByMaxLength(max: Int): String {
    if (length > max) {
        return "${substring(0, max - 1)}…"
    } else {
        return this
    }
}

fun String?.isEmptyOrNull(): String? {
    if (this == null) return null

    if (isEmpty()) return null

    return this
}