package com.example.test.common.utils

class SuperMinutesChangerClass(var text: Array<String>) {
    var barriers = intArrayOf(1, 60, 60 * 24, 60 * 24 * 7, 60 * 24 * 365, Integer.MAX_VALUE)
    //var titleRes = arrayOf("min", "hr", "day", "week", "year")

    fun minutesToHumanReadable(minutes: Int): String {

        val hours = minutes / 60
        val m = minutes % 60

        val result = StringBuilder()

        if (hours != 0) {
            result.append(hours)
            result.append(" ")
            result.append(text[1])
            result.append(" ")
        }
        if (m != 0) {
            result.append(m)
            result.append(" ")
            result.append(text[0])
            result.append(" ")
        }
        return result.toString()
    }
}