package com.example.test.common.placeholders

interface ProgressBarScreenViewHolder : ScreenViewHolder {
    fun setMessage(message:String?)
}