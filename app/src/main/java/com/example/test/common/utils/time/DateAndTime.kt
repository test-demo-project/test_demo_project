package com.example.test.common.utils.time

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class DateAndTime(
    val date: MyDate,
    val time: MyTime
) : Parcelable {

    companion object {
        fun build(date: Date): DateAndTime {
            val myDate = MyDate.build(date)
            val myTime = MyTime.build(date)
            return DateAndTime(myDate, myTime)
        }
    }

    fun convertToDate(): Date {
        return Calendar.getInstance().also {
            it.clear()
            date.applyDatesToCalendar(it)
            time.applyTimeOnCalendar(it)
        }.time
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DateAndTime

        if (date != other.date) return false
        if (time != other.time) return false

        return true
    }

    override fun hashCode(): Int {
        var result = date.hashCode()
        result = 31 * result + time.hashCode()
        return result
    }


}
