package com.example.test.common.errors.excetpions

class WrongFieldException(val resourceId: Int) : Exception() {

    var additionalData: List<Any>? = null

    constructor(resourceId: Int, list: List<Any>) : this(resourceId) {
        additionalData = ArrayList(list)
    }

}
