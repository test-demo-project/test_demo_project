package com.example.test.common.extension

import android.webkit.MimeTypeMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

fun File.getMimeType(): String? {
    var type: String? = null
    val extension = MimeTypeMap.getFileExtensionFromUrl(toURI().toURL().toString())
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    return type
}

fun File.buildMultipartBody(param: String): MultipartBody.Part {
    val requestBody = this.buildRequestBody()

    val body = MultipartBody.Part.createFormData(
            param, name,
            requestBody
    )

    return body
}

fun File.buildRequestBody(): RequestBody {
    return asRequestBody("image/*".toMediaTypeOrNull())
}
