package com.example.test.common.view.list.adapter

import androidx.recyclerview.widget.DiffUtil


abstract class CoolSmartAdapter : CoolAdapterRaw() {

    open fun updateItems(newList: List<Any>) {
        val diffCallback = DiffUtilCallback(data, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        data.clear()
        data.addAll(newList)

        diffResult.dispatchUpdatesTo(this)
    }

}