package com.example.test.common

import android.content.Context
import android.content.Intent
import com.example.test.common.extension.newChain
import com.example.test.data.repository.preferencesRepository.PreferencesRepository

import com.example.test.mvp.authorisation_phone.AuthorisationPhoneFragment
import com.example.test.mvp.base.activity.AutoFragmentActivity

import com.example.test.mvp.root.RootActivity


class NavigationHelper(val context: Context,val preferencesRepository: PreferencesRepository) {

    enum class ScreenToNavigate {
        USER_AGREEMENT, AUTH, CITY_SELECTION, APP_SCREEN
    }

    companion object{

    }

    fun getNextScreen(): ScreenToNavigate {

        if (!preferencesRepository.isSignedUserAgreement) {
            return ScreenToNavigate.USER_AGREEMENT
        }

        if (preferencesRepository.token.isNullOrEmpty()) {
            return ScreenToNavigate.AUTH
        }

        if (preferencesRepository.currentCity == null) {
            return ScreenToNavigate.CITY_SELECTION
        }

        return ScreenToNavigate.APP_SCREEN
    }

    fun openNextScreen(){

        val screenToNavigate = getNextScreen()

        when(screenToNavigate){
            ScreenToNavigate.USER_AGREEMENT->{
            /*    val intent = AutoFragmentActivity.createIntent<UserAgreementFragment>(
                    context)

                intent.newChain()
                context.startActivity(intent)*/
            }
            ScreenToNavigate.APP_SCREEN->{
                val intent = Intent(context, RootActivity::class.java)
                intent.newChain()
                context.startActivity(intent)
            }
            ScreenToNavigate.CITY_SELECTION->{
                /*val intent = AutoFragmentActivity.createIntent<CityListFragment>(
                    context, bundleOf(CityListFragment.SAVE_CITY to true))

                intent.newChain()
                context.startActivity(intent)*/
            }
            ScreenToNavigate.AUTH->{
                val intent = AutoFragmentActivity.createIntent<AuthorisationPhoneFragment>(context)
                intent.newChain()
                context.startActivity(intent)
            }
        }
    }

}