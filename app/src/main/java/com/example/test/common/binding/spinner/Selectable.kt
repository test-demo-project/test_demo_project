package com.example.test.common.binding.spinner

import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData

class Selectable<T:Any>(): BaseObservable() {

    val selected = MutableLiveData<T?>()
    val variantsList = MutableLiveData<List<T>?>()

    init{
        selected.observeForever(){
            notifyChange()
        }

        variantsList.observeForever(){
            notifyChange()
        }
    }

    fun getSelectedIndex(): Int?{
        val itemInList = variantsList.value?.find { it == selected.value}
        val index= itemInList?.let {
            variantsList.value?.indexOf(itemInList)
        }
        if(index==-1)
            return null
        return index
    }


    fun setSelectedByPosition(position: Int){
        val valueToSet =  variantsList.value?.getOrNull(position)
        if(valueToSet!=selected.value){
            selected.value = valueToSet
        }
    }

    fun setSelectedAny(item:Any?){
        val index= variantsList.value?.indexOf(item)
        if(index!=null){
            if(selected.value!=item){
                selected.value = item as T?
            }
        }
    }

}
