package com.example.test.common.utils

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.Math.round
import java.math.BigDecimal
import java.util.*

@Parcelize
data class InAppCurrency(
    val currency: String?,
    val currencyName: String?,
    val currencySymbol: String?,
    val currencyCode: String?,
    val currencyExtension: String?,
    val fractionDigits: Int?,
    val centsFactor: Int
) : Parcelable {

    fun getCurrencyExtra(): String? {
        if (currencySymbol != null) {
            if (currencySymbol.length == 1) {
                return currencySymbol
            } else {
                if (!currencyExtension.isNullOrBlank()) {
                    return currencyExtension
                } else {
                    return currency
                }
            }
        }
        return currencySymbol ?: currencyExtension
    }

    /*private fun Float.getFraction(index:Int):Byte{
         index * 10
    }*/

    fun getCents(price: Float): Long{
/*
        val par = (price).par(centsFactor)
        var result = par.first.toLong()
        result*=centsFactor
        result+=par.second
        Log.d("PRICE","${price}->${ result}")
        return result*/

        val f= ("%." + fractionDigits + "f")
        val string = f.format(price, Locale.ROOT).replace(',','.')
        val splited = string.split('.')
        val decimalPart = splited[0].toLong()
        val fractionalPart = splited[1].toLong()
        var result = decimalPart * centsFactor
        result+=fractionalPart

        return result
        /*

         *//*val f = "%." + fractionDigits + "f"
        val stringView = f.format(price)
        val split = f.split(".")*//*


      *//*  val decimal = price.toInt()
        val fractionalPart = *//*



        return (price * centsFactor).toInt()*/


    }

    private fun Float.par(count:Int):Pair<Int,Int>{
        val int_part = this.toInt()
        val fractional: Double = round(this * count.toFloat()) / (count - int_part).toDouble()
        val fractional_part_in_integer = (fractional * count).toInt()

        return Pair(int_part,fractional_part_in_integer)
        /*printf("%d, %d\n ", int_part, fractional_part_in_integer)*/
    }

}

fun InAppCurrency.formatPriceWithCurrency(price: Float): String {

    val format =
        ("%." + fractionDigits + "f" + " " + getCurrencyExtra()).trim { it <= ' ' }

    return format.format(price)
}

fun InAppCurrency.formatPriceWithCurrency(cents: Long): String {
    val price = cents.toDouble() / centsFactor.toDouble()
    val format =
        ("%." + fractionDigits + "f" + " " + getCurrencyExtra()).trim { it <= ' ' }

    return format.format(price)
}


fun InAppCurrency.formatPriceWithCurrency(cents: BigDecimal): String {
    val price = cents.toDouble() / centsFactor.toDouble()
    val format =
        ("%." + fractionDigits + "f" + " " + getCurrencyExtra()).trim { it <= ' ' }

    return format.format(price)
}


fun InAppCurrency.formatPriceWithCurrency(cents: Int): String {
    return formatPriceWithCurrency(cents.toLong())
}


val CURRENCY_RU = InAppCurrency("RUB","Российский рубль","₽","RUB","RUB",2,100)

fun formatBonuses(bonuses: Float):String{
    val floatFormat = "%.2f"
    val intFormat = "%d"

    val coins = (bonuses * 100).toInt()

    if((coins % 100)==0){
        return intFormat.format(bonuses.toInt())
    }else{
        return floatFormat.format(bonuses)
    }
}