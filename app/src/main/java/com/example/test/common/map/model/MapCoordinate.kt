package com.example.test.common.map.model

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MapCoordinate(
    val latitude: Double,
    val longitude: Double
):Parcelable{

    constructor(latLng: LatLng):this(latLng.latitude,latLng.longitude){

    }

    fun toLatLng(): LatLng {
        return LatLng(latitude, longitude)
    }
}