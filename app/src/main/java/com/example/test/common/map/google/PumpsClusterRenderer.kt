package com.example.test.common.map.google


import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import com.example.test.R
import com.example.test.common.extension.dp
import com.example.test.databinding.LayoutMultiPumpBinding

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator


class PumpsClusterRenderer(val context: Context,val map: GoogleMap, clusterManager: ClusterManager<PumpItem>): DefaultClusterRenderer<PumpItem>(context,map,clusterManager) {
    private val pump_item_image: BitmapDescriptor

    private val mClusterIconGenerator = IconGenerator(context)

    private val clusterBinding: LayoutMultiPumpBinding

    private val markerSize = context.resources.getDimension(R.dimen.pump_icon_size).toInt()

    init {
        pump_item_image = BitmapDescriptorFactory
            .fromBitmap(resizeMapIcons("map_pump_item", 20.dp, 26.dp))

    }

    init {
        val clusterView = LayoutInflater.from(context).inflate(R.layout.layout_multi_pump, null)
        clusterBinding = LayoutMultiPumpBinding.bind(clusterView)

        mClusterIconGenerator.setContentView(clusterView)
        mClusterIconGenerator.setBackground(null)
    }


    private fun resizeMapIcons(iconName: String, width: Int, height: Int): Bitmap {
        val resources = context.resources
        val imageBitmap = BitmapFactory.decodeResource(resources, resources.getIdentifier(iconName, "drawable", context!!.getPackageName()))
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false)
    }



    override fun onBeforeClusterItemRendered(item: PumpItem, markerOptions: MarkerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions)


        markerOptions?.icon(pump_item_image)
        markerOptions?.anchor(0.5f, 1f)
    }

    override fun onBeforeClusterRendered(cluster: Cluster<PumpItem>, markerOptions: MarkerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions)

        val size = cluster.size


        clusterBinding.clusterCount.text = size.toString()

        val icon = mClusterIconGenerator.makeIcon()

    //    val scaledSize = Bitmap.createScaledBitmap(icon, 50.dp,65.dp, false)

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
        markerOptions.anchor(0.5f, 1f)
    }

    override fun onClusterItemRendered(clusterItem: PumpItem, marker: Marker) {
        marker.tag = clusterItem
        super.onClusterItemRendered(clusterItem, marker)
    }

    override fun onClusterUpdated(cluster: Cluster<PumpItem>, marker: Marker) {
        super.onClusterUpdated(cluster, marker)

        val size = cluster.size


        clusterBinding.clusterCount.text = size.toString()

        val icon = mClusterIconGenerator.makeIcon()


        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon))
        marker.setAnchor(0.5f, 1f)
    }


    /*override fun onClusterItemRendered(clusterItem: PumpItem, marker: Marker) {
        super.onClusterItemRendered(clusterItem, marker)
    }*/





 



}