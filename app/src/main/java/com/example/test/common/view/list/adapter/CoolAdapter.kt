package com.example.test.common.view.list.adapter


abstract class CoolAdapter() : CoolAdapterRaw() {

    open fun updateItems(data: List<Any>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    open fun addData(list: List<Any>) {
        val prevSize = data.size
        data.addAll(list)
        val newSise = data.size
        notifyItemRangeInserted(prevSize, list.size)
    }

}
