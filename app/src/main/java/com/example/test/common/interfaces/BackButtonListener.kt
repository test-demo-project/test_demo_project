package com.example.test.common.interfaces

interface BackButtonListener {
    fun onBackPressed(): Boolean
}