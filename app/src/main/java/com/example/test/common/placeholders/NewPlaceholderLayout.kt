package com.example.test.common.placeholders


import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat

import com.example.test.R
import com.example.test.common.extension.CallBackKUnit
import com.example.test.common.extension.gone
import com.example.test.common.extension.visibleOrInvisible

open class NewPlaceholderLayout(context: Context, attributeSet: AttributeSet) :
    FrameLayout(context, attributeSet) {

    private enum class Layouts() {
        PROGRESS, MESSAGE_WITH_RELOAD, MESSAGE
    }

    private var messageWithButton: NoInternetPlaceholder
    private var progressBar: ProgressBarScreenViewHolder

    private var root: View? = null
    private val rootId: Int

    private val placeholderViews: HashMap<Layouts, View>

    private val visibilityFlags: HashSet<Layouts> = HashSet()

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        super.addView(child, index, params)

        for (i in 0 until childCount) {
            val view = getChildAt(i)
            if (view.id == rootId) {
                root = view
            }
        }

        updatePlaceholdersVisibility()
    }

    /*  open fun getMessagePlaceholderBuilder(): ViewHolderBuilder<MessageScreenViewHolder> {
          return ViewHolderBuilderImpl<MessageScreenViewHolder>() {
              MessageViewHolder(it)
          }
      }

      open fun getNoInternetPlaceholder(): ViewHolderBuilder<NoInternetPlaceholder> {
          return ViewHolderBuilderImpl<NoInternetPlaceholder>() {
              NoInternetPlaceholder(it)
          }
      }*/

    open fun getNoInternetPlaceholder(): ViewHolderBuilder<NoInternetPlaceholder> {
        return ViewHolderBuilderImpl<NoInternetPlaceholder>() {
            NoInternetPlaceholder(it)
        }
    }

    init {
        val attributes =
            context.obtainStyledAttributes(attributeSet, R.styleable.NewPlaceholderLayout)


        rootId = attributes.getResourceId(R.styleable.NewPlaceholderLayout_rootId, -1)

        val placeholderBackgroundResource =
            attributes.getResourceId(R.styleable.NewPlaceholderLayout_placeholder_background, -1)


        if (rootId == -1) {
            throw IllegalStateException("Root id argument is not set")
        }

        /*noInternet = getNoInternetPlaceholder().provide(this).also {

        }*/



        messageWithButton = getNoInternetPlaceholder().provide(this)

        progressBar = StandartProgressBarScreen(this).also {
            if (placeholderBackgroundResource != -1) {
                val bg = ContextCompat.getDrawable(context!!, placeholderBackgroundResource)!!
                it.setBackground(bg)
            }
        }

        //message = getMessagePlaceholderBuilder().provide(this)

        placeholderViews = hashMapOf<Layouts, View>(
            Layouts.PROGRESS to progressBar.getView(),
            Layouts.MESSAGE_WITH_RELOAD to messageWithButton.getView()
        )

        placeholderViews.forEach {
            this.addView(it.value)
            it.value.gone()
        }

        attributes.recycle()
    }


    fun showMessage(show: Boolean, message: String?) {
        this.messageWithButton.setMessage(message ?: "")
        this.messageWithButton.setPositiveButtonText("")
        this.messageWithButton.setPositiveButtonVisible(false)
        this.messageWithButton.setNegativeButtonVisible(false)

        setVisibilityFlag(Layouts.MESSAGE_WITH_RELOAD, show)
        updatePlaceholdersVisibility()
    }

    fun showMessageWithReloadButton(
        show: Boolean,
        message: String? = null,
        reloadButtonText: String? = null,
        buttonAction: CallBackKUnit? = null
    ) {

        this.messageWithButton.setMessage(message ?: "")
        this.messageWithButton.setPositiveButtonText(reloadButtonText ?: "")
        this.messageWithButton.setPositiveButtonVisible(!reloadButtonText.isNullOrBlank())
        this.messageWithButton.setNegativeButtonVisible(false)
        this.messageWithButton.setPositiveButtonCallback {
            buttonAction?.invoke()
        }
        this.setVisibilityFlag(Layouts.MESSAGE_WITH_RELOAD, show)
        updatePlaceholdersVisibility()
    }

    fun showMessageWithButtons(
        show: Boolean,
        message: String? = null,
        positiveButtonText: String? = null,
        negativeButtonText: String? = null,

        onPositiveButtonClicked: CallBackKUnit? = null,
        onNegativeButtonClicked: CallBackKUnit? = null
    ) {

        if(!show){
            this.setVisibilityFlag(Layouts.MESSAGE_WITH_RELOAD, false)
            return
        }


        this.messageWithButton.setPositiveButtonVisible(false)
        this.messageWithButton.setNegativeButtonVisible(false)

        this.messageWithButton.setMessage(message ?: "")

        if(positiveButtonText!=null){
            this.messageWithButton.setPositiveButtonText(positiveButtonText)
            this.messageWithButton.setPositiveButtonCallback {
                onPositiveButtonClicked?.invoke()
            }
            this.messageWithButton.setPositiveButtonVisible(true)
        }

        if(negativeButtonText!=null){
            this.messageWithButton.setNegativeButtonText(negativeButtonText)
            this.messageWithButton.setNegativeButtonCallback {
                onNegativeButtonClicked?.invoke()
            }
            this.messageWithButton.setNegativeButtonVisible(true)
        }

        this.setVisibilityFlag(Layouts.MESSAGE_WITH_RELOAD, true)
        updatePlaceholdersVisibility()
    }


    fun showProgressBar(show: Boolean,message:String?=  null) {
        progressBar.setMessage(message)
        this.setVisibilityFlag(Layouts.PROGRESS, show)
        updatePlaceholdersVisibility()
    }

    private fun setVisibilityFlag(layout: Layouts, set: Boolean) {
        if (set) {
            visibilityFlags.add(layout)
        } else {
            visibilityFlags.remove(layout)
        }
    }

    private fun getCurrentViewToShow(): Layouts? {
        return visibilityFlags.sortedBy { it.ordinal }.firstOrNull()
    }

    private fun updatePlaceholdersVisibility() {
        setupVisibility(getCurrentViewToShow())
    }

    private fun setupVisibility(layout: Layouts?) {
        placeholderViews.forEach { it ->
            updateVisibility(it.value, it.key == layout)
        }

        root?.let {
            updateVisibility(it, layout == null)
        }
    }

    private fun updateVisibility(view: View, visible: Boolean) {
        val isVisible = view.visibility == View.VISIBLE
        if (isVisible != visible) {
            Log.d("PROGRESS_BAR_TEST", "PROGRESS: $visible")
            view?.visibleOrInvisible(visible)
        }
    }

    fun isShowingLayout(): Boolean {
        return getCurrentViewToShow() != null
    }

}