package com.example.test.common.view.list.item_decoration

import android.graphics.Rect
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView

open class SpacesItemDecoration(
        val horizontalSpace: Int,
        val verticalSpace: Int,
        val betweenSpace: Int,
        val orientation: Int
) : RecyclerView.ItemDecoration() {

    companion object {
        const val HORIZONTAL = LinearLayout.HORIZONTAL
        const val VERTICAL = LinearLayout.VERTICAL
    }

    constructor(
            horizontalSpace: Int,
            verticalSpace: Int,
            orientation: Int = VERTICAL
    ) : this(
            horizontalSpace = horizontalSpace,
            verticalSpace = verticalSpace,
            betweenSpace = when(orientation) {
                HORIZONTAL -> horizontalSpace
                VERTICAL -> verticalSpace
                else -> verticalSpace
            },
            orientation = orientation
    )

    open fun doDecorationForItem(index: Int): Boolean {
        return true
    }

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        val index = parent.getChildLayoutPosition(view)
        val size = parent.adapter!!.itemCount

        val drawSpace = doDecorationForItem(index)

        if (drawSpace) {

            when(orientation) {
                HORIZONTAL -> {
                    outRect.top = verticalSpace
                    outRect.bottom = verticalSpace

                    when (index) {
                        0 -> {
                            outRect.left = horizontalSpace
                        }
                        size - 1 -> {
                            outRect.left = betweenSpace
                            outRect.right = horizontalSpace
                        }
                        else -> {
                            outRect.left = betweenSpace
                        }
                    }
                }
                VERTICAL -> {
                    outRect.left = horizontalSpace
                    outRect.right = horizontalSpace

                    when (index) {
                        0 -> {
                            outRect.top = verticalSpace
                        }
                        size - 1 -> {
                            outRect.top = betweenSpace
                            outRect.bottom = verticalSpace
                        }
                        else -> {
                            outRect.top = betweenSpace
                        }
                    }
                }
            }
        }
    }
}
