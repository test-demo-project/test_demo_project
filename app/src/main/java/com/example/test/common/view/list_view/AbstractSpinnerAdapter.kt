package com.example.test.common.view.list_view

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import com.example.test.common.view.list.adapter.SimpleViewHolder

abstract class AbstractSpinnerAdapter<T : Any>() : BaseAdapter() {
    private val items = ArrayList<T>()

    open fun getDropdownViewHolder(parent: ViewGroup): SimpleViewHolder<T>?{
        return null
    }

    abstract fun getViewHolder(parent: ViewGroup): SimpleViewHolder<T>

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val tag = convertView

        val viewHolder = if (tag is SimpleViewHolder<*>) {
            tag as SimpleViewHolder<T>
        } else {
            getViewHolder(parent!!)
        }

      //  val view = ItemChooseCardBinding.inflate(LayoutInflater.from(parent!!.context),parent,false)

        viewHolder.getItemView().layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)

        viewHolder.onBind(items[position])

        viewHolder.itemView.tag = viewHolder

        return viewHolder.getItemView()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val tag = convertView

        val viewHolder = if (tag is SimpleViewHolder<*>) {
            tag as SimpleViewHolder<T>
        } else {
            getDropdownViewHolder(parent!!)?:getViewHolder(parent)
        }

        viewHolder.onBind(items[position])

        viewHolder.itemView.tag = viewHolder

        return viewHolder.getItemView()
    }

    override fun getItem(position: Int): T {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getCount(): Int {
        return items.size
    }

    fun setItems(items: List<T>?) {
        this.items.clear()
        if(items!=null)
            this.items.addAll(items)
        notifyDataSetChanged()
    }
}