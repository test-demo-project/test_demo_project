package com.example.test.common.extension

import android.content.Intent

fun Intent.newChain(){
    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
}