package com.example.test.common.view.list.item_decoration

import android.content.Context
import com.example.test.common.view.list.item_decoration.SpacesItemDecoration
import com.example.test.R


open class ScreenSpacesItemDecoration(
    context: Context,
    betweenSpace: Int,
    orientation: Int = SpaceItemDecoration.VERTICAL
) : SpacesItemDecoration(
    horizontalSpace = context.resources.getDimension(R.dimen.screen_padding_horizontal).toInt(),
    verticalSpace = context.resources.getDimension(R.dimen.screen_padding_top).toInt(),
    betweenSpace = betweenSpace,
    orientation = orientation
)
