package com.carrera.common.view.spinner

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.test.R
import com.example.test.common.binding.spinner.Selectable
import java.util.*


/*fun AppCompatSpinner.setItems(identities: List<Any>?,default:String? = null) {

    val representations: List<String>

    representations = if (identities == null) {
        Collections.emptyList()
    } else {
        identities.map { it.toString() }
    }

    val adapter = ArrayAdapter(
        context,
        android.R.layout.simple_spinner_dropdown_item,
        representations
    )

    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    this.adapter = adapter
}*/


fun AppCompatSpinner.bindSpinner(lifecycleOwner: LifecycleOwner,selectable: Selectable<*>, default: String?){

    onItemSelectedListener = null

    selectable.variantsList.observe(lifecycleOwner, androidx.lifecycle.Observer {
        setItems(selectable.variantsList.value?: listOf(),default){
            selectable.setSelectedAny(it)
        }
    })


    selectable.selected.observe(lifecycleOwner, androidx.lifecycle.Observer {
        val prevListener = onItemSelectedListener
        onItemSelectedListener = null

        val index = selectable.getSelectedIndex()

        if(index!=null){
            if(default!=null){
                setSelection(index+1)
            }else{
                setSelection(index)
            }

        }else{
            setSelection(0)
        }

        onItemSelectedListener = prevListener
    })

    onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            selectable.setSelectedByPosition(position)
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {
            selectable.selected.value = null
        }
    }
}



fun AppCompatSpinner.setItems(identities: List<Any>,default:String? = null,onItemSelected: (position: Any?) -> Unit) {

    val representations = ArrayList<String>()

    /*if (identities == null) {
        Collections.emptyList()
    } else {

    }*/

    if(default!=null){
        representations.add(default)
    }

    representations.addAll(identities.map { it.toString() }?: listOf())

    setAdapterWithPosition(representations,{position->
        if(position==null){
            onItemSelected.invoke(null)
        }else{
            onItemSelected.invoke(identities.get(position))
        }

    },default!=null)

    this.adapter = adapter
}


fun AppCompatSpinner.setAdapterWithPosition(list: List<String>, onItemSelected: (position: Int?) -> Unit,isFirstItemGray:Boolean) {
    this.apply {
/*
        val adapterList = kotlin.collections.mutableListOf<String>()
        firstValue?.let {
            adapterList.add(it)
        }
        adapterList.addAll(list)*/


        adapter = ArrayAdapter<String>(this.context,   android.R.layout.simple_spinner_dropdown_item, list).apply {
            setDropDownViewResource(  android.R.layout.simple_spinner_dropdown_item)
        }
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                val realPosition = if(isFirstItemGray){
                    position-1
                }else{
                    position
                }

                if(realPosition<list.size && realPosition>=0){
                    onItemSelected.invoke(realPosition)
                }else{
                    onItemSelected.invoke(null)
                }

                if (isFirstItemGray) {
                    parent?.findViewById<TextView>(android.R.id.text1)?.apply {
                        setTextColor(if (position == 0) ContextCompat.getColor(context,R.color.text_input_hint_color_full) else ContextCompat.getColor(context,R.color.text_color))
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }
}