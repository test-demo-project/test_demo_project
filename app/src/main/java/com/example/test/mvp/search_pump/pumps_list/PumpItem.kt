package com.example.test.mvp.search_pump.pumps_list

import com.example.test.common.view.list.adapter.ISmartModel

data class PumpItem(
    val pumpId:Int,
    val address:String,
    val number:String,
    val distanceInMeters: Int?,
    val isFavorite:Boolean
): ISmartModel {

    override fun areItemsTheSame(other: Any): Boolean {
        if(other !is PumpItem)return false
        if(other.pumpId != pumpId)return false
        return true
    }
}