package com.example.test.mvp.root

import com.example.test.mvp.base.fragment.navigation.CiceroneNavigationFragment


class RootFragment() : CiceroneNavigationFragment() {

    init {
        setCiceroneUniqueName("RootFragment")
    }

    override fun initializeRouter() {
        getRouter().newRootChain(TabBarFragment.Screen())
    }

}