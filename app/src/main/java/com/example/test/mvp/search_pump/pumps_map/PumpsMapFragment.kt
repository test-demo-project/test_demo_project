package com.example.test.mvp.search_pump.pumps_map

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.R
import com.example.test.common.extension.CallBackK
import com.example.test.common.extension.onClick
import com.example.test.common.extension.visibleOrGone
import com.example.test.common.map.AbstractMapWrapperFragment
import com.example.test.common.map.google.PumpItem
import com.example.test.common.map.model.MapBounds
import com.example.test.common.map.model.MapCoordinate
import com.example.test.common.permission.CommonPermissions
import com.example.test.common.utils.location.LocationCallback
import com.example.test.databinding.FragmentPumpsMapBinding
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment

import com.google.android.gms.maps.model.LatLng
import moxy.presenter.InjectPresenter

class PumpsMapFragment():BaseScreenFragment<PumpsMapView,PumpsMapPresenter>(),PumpsMapView{

    private var _binding: FragmentPumpsMapBinding? = null
    private val binding get() = _binding!!

    @InjectPresenter
    override lateinit var presenter: PumpsMapPresenter

    private lateinit var map: AbstractMapWrapperFragment

    private val locationCallback: MyLocationCallback = MyLocationCallback()

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPumpsMapBinding.inflate(inflater,container,false)
        return binding.root
    }



    inner class MyLocationCallback():LocationCallback{
        override fun setLocation(location: Location) {
            presenter.setCurrentLocation(LatLng(location.latitude,location.longitude))
        }

        override fun locationAvailable(available: Boolean) {
            presenter.onLocationAvailabilityChanged(available)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        map = childFragmentManager.findFragmentById(R.id.google_map_fragment) as AbstractMapWrapperFragment

        map.onBoundsChangedByUser = {
          //  presenter.refresh()
        }

        map.markerClickListener = {
            openPumpDetails(it)
        }


        binding.centerMapOnMeBt.onClick {
            presenter.centerMapOnMe()
        }
    }



    override fun askPermission(callback: CallBackK<Boolean>) {
        requestLocationPermission {
            callback?.invoke(it)
            onLocationPermissionGranted(it)
        }
    }

    override fun checkPermission(callback: CallBackK<Boolean>) {
        callback.invoke(checkLocationPermission())
    }

   /* override fun checkLocationAvailable(callback: CallBackK<Boolean>) {
        //getLocationProvider().
    }*/

    private fun openPumpDetails(pumpItem: PumpItem){
    //    getRouter()?.navigateTo(WaterPumpFragment.Screen(pumpItem.id))
    }

    private fun onLocationPermissionGranted(result:Boolean){
        presenter.onLocationPermissionPermissionGranted(result)

        if(result)
            map.onLocationAccessGranted()

        startLocationUpdates()
    }

    private fun startLocationUpdates(){
        getLocationProvider().getLocationUpdates(locationCallback)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLocationProvider().getLocationUpdates(locationCallback)
    }

    override fun onDestroy() {
        getLocationProvider().removeLocationUpdates(locationCallback)
        super.onDestroy()
    }

    /*override fun setLocation(location: Location) {

    }*/

    override fun setMapBounds(mapBounds: MapBounds, smooth: Boolean) {
        map.setMapBounds(mapBounds,smooth)
    }

    override fun setPumps(pumps: List<PumpItem>) {
        map.setPumps(pumps)
    }

    override fun moveMap(mapCoordinate: MapCoordinate, smooth: Boolean) {
        map.moveMap(mapCoordinate,smooth,14f)
    }

    fun checkLocationPermission():Boolean {
        return checkPermission(CommonPermissions.LOCATION_PERMISSION)
    }

    fun requestLocationPermission(callback: CallBackK<Boolean>) {
        requestPermissions(CommonPermissions.LOCATION_PERMISSION.toTypedArray(),callback)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun setDetectingLocation(visible: Boolean) {
        binding.detectingLocation.visibleOrGone(visible)
    }
}

