package com.example.test.mvp.search_pump.pumps_list

import com.example.test.mvp.base.fragment.list.ListView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

interface PumpumpsListView:ListView<Any>{


    @StateStrategyType(AddToEndSingleTagStrategy::class,tag = "location_placeholder")
    fun showLocationPermissionRequiredPlaceholder(show:Boolean,city:String? = null)

    @StateStrategyType(AddToEndSingleTagStrategy::class,tag = "location_placeholder")
    fun showLocationDetectingPlaceholder(show:Boolean)

    @StateStrategyType(AddToEndSingleTagStrategy::class,tag = "location_placeholder")
    fun showLocationServiceInNotAvailable(show:Boolean, city:String? = null)



}