package com.example.test.mvp.base.fragment.list


import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution


interface ListView<LIST_ITEM> : BaseScreenView {

    @AddToEndSingle
    fun updateItems(items: List<LIST_ITEM>)

    @AddToEndSingle
    fun onLoadDone()

    @OneExecution
    fun updateList()

    @OneExecution
    fun scrollToTop()



    /*@AddToEndSingle
    fun showMessageInList(show: Boolean, message: String?)
*/
    @AddToEndSingle
    fun showProgressBarInList(show: Boolean)

    @OneExecution
    fun resetPagination()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateWithoutAnimations()
}
