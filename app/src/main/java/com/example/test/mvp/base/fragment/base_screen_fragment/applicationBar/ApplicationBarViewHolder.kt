package com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar

import android.view.View
import com.example.test.common.extension.CallBackKUnit

interface ApplicationBarViewHolder {
    fun setBackButtonVisibility(visible: Boolean)
    fun setText(text: String)
    fun getView(): View
    var onBackButtonClicked: CallBackKUnit
}
