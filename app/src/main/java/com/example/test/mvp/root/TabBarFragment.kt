package com.example.test.mvp.root

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.test.R
import com.example.test.common.extension.visibleOrGone
import com.example.test.common.interfaces.BackButtonListener
import com.example.test.common.utils.KeyboardUtils
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.textview.MaterialTextView

import kotlinx.android.synthetic.main.fragment_tab_bar.*
import moxy.presenter.InjectPresenter


class TabBarFragment() :
    BaseScreenFragment<TabBarView, TabBarPresenter>(),
    TabBarView, BackButtonListener {

    @InjectPresenter
    override lateinit var presenter: TabBarPresenter



    private val broadcastReceiver = TabBarFragmentBroadcastReceiver()

    private var currentTab:Tab = Tab.ADDRESSES

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_tab_bar, container, false)
    }

    private fun setMenuSelected(tab: Tab) {
        val size = bottom_navigation.menu.size()

        val tabValues = Tab.values()

       /* for (i in 0 until size) {

            val currentTab = tabValues[i]

            val item = bottom_navigation.menu.getItem(i)


            item.setIcon(currentTab.icon)



        }*/

        if( bottom_navigation.selectedItemId!=tab.ordinal)
            bottom_navigation.selectedItemId = tab.ordinal


    }

    override fun onStart() {
        super.onStart()
        KeyboardUtils.addKeyboardToggleListener(requireActivity(), keyboardListener)
    }

    override fun onStop() {
        KeyboardUtils.removeKeyboardToggleListener(keyboardListener)
        super.onStop()
    }

    private val keyboardListener = object : KeyboardUtils.SoftKeyboardToggleListener {
        override fun onToggleSoftKeyboard(isVisible: Boolean) {
            bottom_navigation.visibleOrGone(!isVisible)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Tab.values().forEach {
            val m = bottom_navigation.menu.add(Menu.NONE, it.ordinal, Menu.NONE, getString(it.text))

        }

        val tabValues = Tab.values()

        for (i in 0 until tabValues.size) {

            val currentTab = tabValues[i]

            val item = bottom_navigation.menu.getItem(i)

            item.setIcon(currentTab.icon)
        }

        val size = bottom_navigation.menu.size()

        val menuView = bottom_navigation.getChildAt(0) as BottomNavigationMenuView

        for (i in 0 until size) {
            val m = menuView.getChildAt(i)
            val largeTextView =
                menuView.findViewById<MaterialTextView>(com.google.android.material.R.id.largeLabel)
            val smallTextView =
                menuView.findViewById<MaterialTextView>(com.google.android.material.R.id.smallLabel)
            smallTextView.setSingleLine(false);
            largeTextView.setSingleLine(false);

            smallTextView.maxLines = 2
            largeTextView.maxLines = 2
            smallTextView.setGravity(Gravity.CENTER);
            largeTextView.setGravity(Gravity.CENTER);


        }

        bottom_navigation.setOnNavigationItemSelectedListener { item: MenuItem ->

            val id = item.itemId

            val tab = Tab.values().find { it.ordinal == id }

            if (tab != null && currentTab!=tab) {
                presenter.viewState.setTab(tab)
            }

            true
        }

        requireActivity().registerReceiver(
            broadcastReceiver, IntentFilter(
                TAB_BAR_FRAGMENT_BROADCASR_RECEIVER
            )
        )
    }

    override fun onDestroyView() {
        activity?.unregisterReceiver(broadcastReceiver)
        super.onDestroyView()
    }



    override fun setTab(tab: Tab) {
        currentTab = tab
        loadFragment(tab)
        setMenuSelected(tab)
    }

    private var navigateToHome:Boolean = false

    fun getLevel(fragment:Fragment){

    }

    private fun loadFragment(tab: Tab): Boolean {

        val fm = childFragmentManager
        var currentFragment: Fragment? = null
        val fragments = fm.fragments
        if (fragments != null) {
            for (f in fragments) {
                if (f.isVisible) {
                    currentFragment = f
                    break
                }
            }
        }
        val newFragment = fm.findFragmentByTag(tab.name)

        if (currentFragment != null && newFragment != null && currentFragment === newFragment) {
            if (currentFragment === newFragment) {
                /* if (currentFragment is INavigatableToHome && doublePressed) {
                     currentFragment.navigateToHome()
                 }*/

            }
            return false
        }

        //currentTabsChangeListeners.forEach { it.invoke(tab) }

        val transaction = fm.beginTransaction()
        if (newFragment == null) {
            transaction.add(
                R.id.tabs_frame_layout,
                NavigationContainer.newInstance(tab),
                tab.name
            )
        }

        if (currentFragment != null) {
            transaction.hide(currentFragment)
        }

        fragments.forEach {
            if(it!=newFragment){
                transaction.hide(it)
            }
        }

        if (newFragment != null) {
            transaction.show(newFragment)
            if(newFragment is NavigationContainer && navigateToHome){
                newFragment.navigateToHome()
                navigateToHome = false
            }
        }

        transaction.commitNowAllowingStateLoss()


        return true
    }

    private var lastPressTime: Long? = null

    private val CLOSE_TIME = 2000L

    override fun onBackPressed(): Boolean {
        val f = getCurrentTabFragment()
        if (f is BackButtonListener) {
            return f.onBackPressed()
        }
        return false
    }

    fun getCurrentTabFragment(): Fragment? {
        return currentTab?.let {
            childFragmentManager.findFragmentByTag(it.name)
        }
    }

    inner class TabBarFragmentBroadcastReceiver() : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val command = intent?.getStringExtra(BROADCAST_COMMAND)
            when (command) {
                BROADCAST_COMMAND_CHANGE_TAB -> {
                    val tabString = intent.getStringExtra(BROADCAST_ARG_TAB) ?: ""
                    val tab = Tab.valueOf(tabString)
                    navigateToHome = true
                    presenter.viewState.setTab(tab)
                }
                else -> {
                }
            }
        }
    }

    companion object {

        val TAB_BAR_FRAGMENT_BROADCASR_RECEIVER =
            "Test.TabBarFragment"

        val BROADCAST_ARG_TAB = "BROADCAST_ARG_TAB"
        val BROADCAST_COMMAND = "BROADCAST_ARG_COMMAND"
        val BROADCAST_COMMAND_CHANGE_TAB = "BROADCAST_COMMAND_CHANGE_TAB"


        fun sendChangeTab(context: Context, tab: Tab) {
            context.sendBroadcast(Intent(TAB_BAR_FRAGMENT_BROADCASR_RECEIVER).also {
                it.putExtra(BROADCAST_ARG_TAB, tab.toString())
                it.putExtra(BROADCAST_COMMAND, BROADCAST_COMMAND_CHANGE_TAB)
            })
        }

        fun Screen() = FragmentScreen {
            TabBarFragment()
        }
    }
}


