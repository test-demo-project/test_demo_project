package com.example.test.mvp.base.fragment.list

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.example.test.R
import com.example.test.common.errors.showMessage
import com.example.test.common.extension.launchUI
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import java.io.EOFException
import java.util.concurrent.atomic.AtomicBoolean

abstract class PaginatorListPresenter<VIEW : ListView<ITEM>, ITEM : Any>(
    val singlePage: Boolean = false,
    val firstPage:Int = 1,
    val resetListAfterRefresh:Boolean = false
) : BaseScreenPresenter<VIEW>() {



    var search = MutableLiveData<String>()

    init {
        search.observeForever(this::onSearchChanged)
    }

    private var toSearch: String? = null
    private var handlerForSearch = Handler(Looper.getMainLooper())
    protected open val searchDelay = 400

    private fun onSearchChanged(newSearch: String?) {

        val query = if (newSearch?.trim()?.isBlank() ?: true) null else newSearch?.trim()

        if (toSearch != query) {
            toSearch = query

            handlerForSearch.removeCallbacksAndMessages(null)
            handlerForSearch.postDelayed({ ->
                reloadData()
            }, searchDelay.toLong())
        }
    }

    protected var currentPage: Int = -1 + firstPage

    protected var allLoaded: Boolean = false
        private set(value){
            field = value
            this.showProgressBarInList(!value)
        }

    open fun showProgressBarInList(show:Boolean){
        viewState.showProgressBarInList(show)
    }

    protected var loadingNow: AtomicBoolean = AtomicBoolean(false)

    protected var loadingJob: Job? = null

    protected var needUpdateViewAfterLoad: Boolean = true

    protected var data = ArrayList<ITEM>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadNext()

    }

    fun loadNextPage() {
        loadNext()
    }

    open fun notifyLoadingStateChanged(loading: Boolean) {

    }

    private fun loadNext() {

        if (!allLoaded && loadingNow.compareAndSet(false, true)) {
            val nextPage = currentPage + 1

            notifyLoadingStateChanged(true)

            showProgressBarInList(data.isEmpty())
            viewState.showFullContentMessage(false,"")

            loadingJob = launchUI {

                try {
                    val items = withContext(Dispatchers.IO) { load(nextPage, toSearch) }
                    onItemsLoaded(items, nextPage)
                } catch (e : Exception) {
                    if (e.javaClass.name != "kotlinx.coroutines.JobCancellationException") {
                        onError(e, nextPage)
                    }
                }

//                val intercator = getInteractor()
//                intercator.load(nextPage, toSearch).awaitFold({
//                    onItemsLoaded(it, nextPage, intercator)
//                }, {
//                    onError(it, nextPage)
//                })
            }.also {
                it.invokeOnCompletion {
                    loadingNow.set(false)
                    this.showProgressBarInList(false)
                    viewState.onLoadDone()
                    notifyLoadingStateChanged(false)
                }
            }
        }
    }



    abstract suspend fun load(page: Int, search: String?): List<ITEM>

    private fun onError(e: Exception, pageToLoad: Int) {
        if (e is EOFException) {
            onItemsLoaded(emptyList(), pageToLoad)
            return
        }
        val handled = customErrorHandler(e)
        if(handled){
            return
        }
        viewState.showMessage(e){
            viewState.showFullContentMessage(true,it)
        }

        updateItems(data)
    }

    protected open fun customErrorHandler(e: Exception): Boolean {
        return false
    }

    private fun onItemsLoaded(
        items: List<ITEM>,
        pageToLoad: Int
    ) {
        viewState.onLoadDone()
        if (items.isEmpty() && pageToLoad == firstPage) {
            onEmptyListLoaded(true)
            updateItems(data)
            allLoaded = true
            return
        }
        if (items.isEmpty() && pageToLoad != firstPage) {
            allLoaded = true
            // updateItems(data)
            return
        }
        if (singlePage) allLoaded = true

        if (data.isEmpty()) {
            viewState.scrollToTop()
        }
        currentPage = pageToLoad
        addData(items)
        onItemsListChanged(data)
        onEmptyListLoaded(false)
    }


    open fun onEmptyListLoaded(isEmpty: Boolean) {
        if (isEmpty) {
            viewState.showFullContentMessage(true,getEmptyListText())
        }
    }

    open fun getEmptyListText(): String {
        return stringProvider.getString(R.string.list_is_empty_default)
    }

    private fun addData(items: List<ITEM>) {

        data.addAll(items)

        updateItems(data)

    }

    open fun onItemsListChanged(data: List<ITEM>) {

    }

    open fun updateItems(data: List<ITEM>) {
        viewState.updateItems(data)
    }



    override fun refresh(): Boolean {
        super.refresh()
        reloadData()
        return true
    }

    open fun reloadData(){
        reset()
        loadNext()
    }

    private fun reset() {
        data.clear()
        needUpdateViewAfterLoad = true
        loadingJob?.cancel()
        loadingNow.set(false)
        currentPage = -1 + firstPage
        allLoaded = false
        viewState.resetPagination()
        if(resetListAfterRefresh){
            viewState.updateItems(data)
            viewState.updateWithoutAnimations()
        }
    }
}
