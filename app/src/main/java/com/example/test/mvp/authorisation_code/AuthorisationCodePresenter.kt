package com.example.test.mvp.authorisation_code

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import com.example.test.common.extension.runWithDialog
import com.example.test.data.server.model.SmsCodeRequestMethod
import com.example.test.di.DaggerUtils
import com.example.test.interactor.UserInteractor
import com.example.test.interactor.WaterPumpsInteractor
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.lang.Exception
import javax.inject.Inject

@InjectViewState
class AuthorisationCodePresenter(val phone:String):BaseScreenPresenter<AuthorisationCodeView>(){

    @Inject
    lateinit var userInteractor: UserInteractor

    @Inject
    lateinit var waterPumpInteractor: WaterPumpsInteractor

    companion object{
        private const val FIVE_MIN = 1_000L * 30
        private val DEFAULT_METHOD = SmsCodeRequestMethod.CALL

    }

    private var timer: BlockTimer? = null

    val code = MutableLiveData<String>()

    private var codeRequestMethod: SmsCodeRequestMethod = DEFAULT_METHOD

    private fun setCodeRequestMethod(method:SmsCodeRequestMethod){
        this.codeRequestMethod = method
        viewState.setAuthorisationMethod(method)
    }

    init{

        DaggerUtils.appComponent.inject(this)

        updateViewTimerViews()

        val timeToWait = userInteractor.getRepeatSmsTimeLeft(phone)

        if(timeToWait!=null && timeToWait>0){
            startTimer(timeToWait)
        }else{
            startTimer(UserInteractor.SMS_CODE_REPEAT_INTERVAL)
        }

        code.observeForever {
            if(it.length==4){
                viewState.closeKeyboard()
                sendSmsCode()
            }
        }
        viewState.setPhone(phone)
        setCodeRequestMethod(DEFAULT_METHOD)

    }

    private fun sendSmsCode(){
        val code = code.value?:""

        runWithDialog(request = {
            try{
                withContext(Dispatchers.IO){
                    userInteractor.auth(phone,code)
                    waterPumpInteractor.synchronizeFavorites()
                }
                viewState.openNextScreen()
            }catch (e:Exception){
                this@AuthorisationCodePresenter.code.value = ""
                throw e
            }
        })
    }

    private inner class BlockTimer(time: Long) : CountDownTimer(time, 1000L) {

        override fun onTick(p0: Long) {
            viewState.setTimerValue(p0,codeRequestMethod)
        }

        override fun onFinish() {
            timer = null
            updateViewTimerViews()
        }
    }

    private fun updateViewTimerViews() {
        val timer = timer
        viewState.setTimerVisibility(timer!=null)
        viewState.setResendButtonVisibility(timer==null)
    }

    private fun cancelBlockTimer() {
        timer?.cancel()
        timer = null

        updateViewTimerViews()
    }


    private fun startTimer(time: Long?) {
        timer?.cancel()

        if(time!=null && time>0){
            timer = BlockTimer(time)

            viewState.setTimerValue(time,codeRequestMethod)

            timer?.start()

        }

        updateViewTimerViews()
    }

    fun sendCodeAgain() {
        runWithDialog(
            request = {
                //We always repeat sending code using sms method
                withContext(Dispatchers.IO){
                    userInteractor.requestSmsCode(phone,SmsCodeRequestMethod.SMS)
                }
                //We change labels on the screen to
                setCodeRequestMethod(SmsCodeRequestMethod.SMS)
                startTimer(userInteractor.getRepeatSmsTimeLeft(phone))
            }
        )
    }

}