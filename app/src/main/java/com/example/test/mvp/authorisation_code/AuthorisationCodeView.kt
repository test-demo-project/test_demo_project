package com.example.test.mvp.authorisation_code

import com.example.test.data.server.model.SmsCodeRequestMethod
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface AuthorisationCodeView: BaseScreenView{

    @AddToEndSingle
    fun setPhone(phone:String)

    @AddToEndSingle
    fun setResendButtonVisibility(visible:Boolean)

    @AddToEndSingle
    fun setTimerVisibility(visible:Boolean)

    @AddToEndSingle
    fun setTimerValue(millis: Long,method: SmsCodeRequestMethod)

    @OneExecution
    fun closeKeyboard()

    @OneExecution
    fun openNextScreen()

    @AddToEndSingle
    fun setAuthorisationMethod(method:SmsCodeRequestMethod)
}

