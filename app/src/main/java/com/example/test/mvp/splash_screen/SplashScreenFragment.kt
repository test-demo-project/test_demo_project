package com.example.test.mvp.splash_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.BuildConfig
import com.example.test.R
import com.example.test.databinding.FragmentSplashScreenBinding
import com.example.test.di.DaggerUtils

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import moxy.presenter.InjectPresenter

class SplashScreenFragment() : BaseScreenFragment<SplashScreenView, SplashScreenPresenter>(),
    SplashScreenView {

    private var _binding: FragmentSplashScreenBinding? = null
    private val binding get() = _binding!!

    @InjectPresenter
    override lateinit var presenter: SplashScreenPresenter

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSplashScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.appVersion.text = getString(R.string.app_version_template, BuildConfig.VERSION_NAME)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun openNextScreen() {
        DaggerUtils.appComponent.navigationHelper().openNextScreen()
    }

    override fun closeApp() {
        activity?.finish()
    }
}