package com.example.test.mvp.authorisation_code

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.common.utils.PhoneMaskApplyer
import com.example.test.R
import com.example.test.common.extension.*
import com.example.test.common.utils.DateUtil
import com.example.test.common.view.bind
import com.example.test.data.server.model.SmsCodeRequestMethod
import com.example.test.databinding.FragmentAuthorisationCodeBinding
import com.example.test.di.DaggerUtils
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.ApplicationBarViewHolder
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.StandardApplicationBar
import com.github.terrakok.cicerone.androidx.FragmentScreen
import kotlinx.android.parcel.Parcelize
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class AuthorisationCodeFragment: BaseScreenFragment<AuthorisationCodeView,AuthorisationCodePresenter>(),AuthorisationCodeView{

    private var _binding: FragmentAuthorisationCodeBinding? = null
    private val binding get() = _binding!!

    @InjectPresenter
    override lateinit var presenter: AuthorisationCodePresenter

    @ProvidePresenter
    fun providePresenter() = AuthorisationCodePresenter(arguments?.getParcelable<Arg>(ARGUMENT)!!.phone)

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAuthorisationCodeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun closeKeyboard() {
        activity?.hideSoftKeyboard()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setApplicationBarTitle(getString(R.string.authorisation_label))
        binding.sendSmsCodeBtLayout.onClick {
            presenter.sendCodeAgain()
        }

        binding.smsCodeInput.editText.bind(viewLifecycleOwner,presenter.code)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    @Parcelize
    class Arg(
        val phone:String
    ):Parcelable

    override fun setPhone(phone: String) {
        binding.phoneNumberTv.text = PhoneMaskApplyer.formatPhone(phone)
    }

    override fun setResendButtonVisibility(visible: Boolean) {
        binding.sendSmsCodeBtLayout.visibleOrGone(visible)
    }

    override fun setTimerVisibility(visible: Boolean) {
        binding.sendSmsCodeTimerTv.visibleOrGone(visible)
    }

    override fun setTimerValue(millis: Long, method: SmsCodeRequestMethod) {
        when(method){
            SmsCodeRequestMethod.CALL->{
                binding.sendSmsCodeTimerTv.text = getString(R.string.code_by_sms_timer_tv, DateUtil.formatTime(millis))
            }
            SmsCodeRequestMethod.SMS->{
                binding.sendSmsCodeTimerTv.text = getString(R.string.repeat_sms_timer_tv, DateUtil.formatTime(millis))
            }
        }
    }

    companion object{
        fun newInstance(arg: Arg): AuthorisationCodeFragment{
            return AuthorisationCodeFragment().also {
                it.putParcelabele(ARGUMENT,arg)
            }
        }

        fun Screen(arg:Arg) = FragmentScreen{
            newInstance(arg)
        }
    }

    override fun getApplicationBarViewHolder(
        layoutInflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarViewHolder? {
        return StandardApplicationBar(container)
    }

    override fun openNextScreen() {
        DaggerUtils.appComponent.navigationHelper().openNextScreen()
    }

    override fun setAuthorisationMethod(method: SmsCodeRequestMethod) {
        when(method){
            SmsCodeRequestMethod.CALL->{
                binding.repeatSendButtonLabel.text = getString(R.string.send_code_by_sms)
            }
            SmsCodeRequestMethod.SMS->{
                binding.repeatSendButtonLabel.text = getString(R.string.send_code_by_sms)
            }
        }

        binding.callInstructionLabel.visibleOrInvisible(method==SmsCodeRequestMethod.CALL)
        binding.smsInstructionLabel.visibleOrInvisible(method==SmsCodeRequestMethod.SMS)

    }
}