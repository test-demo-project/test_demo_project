package com.example.test.mvp.root

import com.example.test.R

import com.example.test.mvp.profile.ProfileFragment
import com.example.test.mvp.search_pump.SearchPumpFragment
import com.github.terrakok.cicerone.androidx.AppScreen

enum class Tab(
    val text: Int,
    val icon: Int,
    val iconActive: Int,
    val screen: AppScreen?
) {


    ADDRESSES(
        R.string.tab_menu_addresses,
        R.drawable.ic_menu_marker_selector,
        R.drawable.ic_menu_marker_on,
        SearchPumpFragment.Screen()
    ),
    /*PROMOTIONS(
        R.string.tab_menu_promotions,
        R.drawable.ic_menu_gift_off,
        R.drawable.ic_menu_gift_on,
        null
    ),*/
    SETTINGS(R.string.tab_menu_settings,
        R.drawable.ic_menu_settings_selector,
        R.drawable.ic_menu_settings_on,
        ProfileFragment.Screen())

}
