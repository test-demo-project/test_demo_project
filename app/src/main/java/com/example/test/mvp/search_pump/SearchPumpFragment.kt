package com.example.test.mvp.search_pump

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.test.R
import com.example.test.common.interfaces.RouterProvider
import com.example.test.databinding.FragmentSearchPumpBinding
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.ApplicationBarViewHolder
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.StandardApplicationBar
import com.example.test.mvp.root.ViewPagerAdapter
import com.example.test.mvp.search_pump.pumps_list.PumpsListFragment
import com.example.test.mvp.search_pump.pumps_map.PumpsMapFragment
import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.google.android.material.tabs.TabLayoutMediator
import moxy.presenter.InjectPresenter

class SearchPumpFragment():BaseScreenFragment<SearchPumpView,SearchPumpPresenter>(),SearchPumpView,RouterProvider{



    @InjectPresenter
    override lateinit var presenter: SearchPumpPresenter

    private var _binding: FragmentSearchPumpBinding? = null
    val binding get() = _binding!!

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchPumpBinding.inflate(inflater,container,false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setApplicationBarTitle(getString(R.string.find_water_pump))

        viewPagerAdapter = ViewPagerAdapter(childFragmentManager,lifecycle)

        //binding.tabBarLayout.setupWithViewPager()

        viewPagerAdapter.setTabs(listOf(
            object : ViewPagerAdapter.NavigationContainerBuilder{
                override fun create(): Fragment {
                    return PumpsListFragment()
                }

                override fun getName(): String {
                    return getString(R.string.tab_search_by_address)
                }
            },
            object : ViewPagerAdapter.NavigationContainerBuilder{
                override fun create(): Fragment {
                    return PumpsMapFragment()
                }

                override fun getName(): String {
                    return getString(R.string.tab_search_by_map)
                }
            }
        ))

        binding.tabsViewPager.adapter = viewPagerAdapter

        TabLayoutMediator(binding.tabBarLayout, binding.tabsViewPager) { tab, position ->
            tab.text = viewPagerAdapter.getName(position)
        }.attach()

        binding.tabsViewPager.isUserInputEnabled = false
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun getApplicationBarViewHolder(
        layoutInflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarViewHolder? {
        return StandardApplicationBar(container)
    }

    companion object{
        fun Screen() = FragmentScreen(){
            SearchPumpFragment()
        }
    }
}