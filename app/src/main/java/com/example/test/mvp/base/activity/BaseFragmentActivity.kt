package com.example.test.mvp.base.activity

import android.content.Context
import android.os.Bundle

import androidx.fragment.app.Fragment
import com.example.test.R
import com.example.test.common.MyContextWrapper
import com.example.test.common.extension.CallBackK
import com.example.test.common.interfaces.BackButtonListener
import com.example.test.common.permission.IPermissionChecker
import com.example.test.common.permission.LocalPermissionsChecker
import com.example.test.common.utils.location.ILocataionProvider
import com.example.test.common.utils.location.LocationCallback
import com.example.test.common.utils.location.LocationUtils

import moxy.MvpAppCompatActivity
import java.util.*

abstract class BaseFragmentActivity() : MvpAppCompatActivity(),IPermissionChecker,
    ILocataionProvider {

    private final val PERMISSION_REQUEST_CODE = 1

    private val localPermissionChecker = LocalPermissionsChecker(this,PERMISSION_REQUEST_CODE)

    private val locationProvider = LocationUtils(this)

    override fun getLocationUpdates(locationCallback: LocationCallback) {
        locationProvider.getLocationUpdates(locationCallback)
    }

    override fun removeLocationUpdates(listener: LocationCallback) {
        locationProvider.removeLocationUpdates(listener)
    }

    override fun requestPermissions(permissions: Array<String>, callback: CallBackK<Boolean>) {
        localPermissionChecker.requestPermissions(permissions,callback)
    }

    override fun onPermissionChanged() {
        locationProvider.onPermissionChanged()
    }

    override fun checkPermissions(permissions: Array<String>): Boolean {
        return localPermissionChecker.checkPermissions(permissions)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        localPermissionChecker.onRequestPermissionsResult(requestCode, permissions,grantResults)
        locationProvider.onPermissionChanged()
    }

    abstract fun getFragment(): Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.ftc_container, getFragment())
                .commit()
        }
    }

    fun getCurrentFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.ftc_container)!!
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, Locale("ru")))
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.ftc_container)
        if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()
        ) {
            return
        } else {
            super.onBackPressed()
            return
        }
    }

}
