package com.example.test.mvp.base.fragment.base_screen_fragment
import com.example.test.common.extension.CallBackKUnit
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface BaseScreenView : MvpView {

    @StateStrategyType(AddToEndSingleTagStrategy::class,tag = "message_placeholder")
    fun showFullContentMessageWithButton(show: Boolean, text: String? = null, buttonText:Int? = null, onButtonClick: CallBackKUnit? = null)

    @StateStrategyType(AddToEndSingleTagStrategy::class,tag = "message_placeholder")
    fun showFullContentMessage(show: Boolean, message: String)

    @AddToEndSingle
    fun showProgressBar(show: Boolean)

    @AddToEndSingle
    fun setApplicationBarTitle(title: String)

    @AddToEndSingle
    fun showProgressBarDialog(show: Boolean, message: String?)

    @OneExecution
    fun showToastMessage(message:String)
}
