package com.example.test.mvp.root
/*

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.balinasoft.modernsalon.R
import com.balinasoft.modernsalon.common.interfaces.ApplicationBarProvider
import com.balinasoft.modernsalon.common.interfaces.BackButtonListener
import com.balinasoft.modernsalon.common.interfaces.BottomViewProvider
import com.balinasoft.modernsalon.common.interfaces.RouterProvider
import com.balinasoft.modernsalon.mvp.main.Screens
import com.balinasoft.modernsalon.mvp.main.TabContainerFragment
import com.balinasoft.modernsalon.mvp.main.Tab
import com.balinasoft.modernsalon.mvp.main.tabactivity.listeners.KeyboardUtils
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_bottom_nav.*
import me.codezfox.extension.visibleOrGone
import ru.terrakok.cicerone.android.support.SupportAppScreen
import java.util.*
*/


/*
class BottomNavActivity : AppCompatActivity() {

    private val PAGE_ITEM = "PAGE_ITEM"

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    //private lateinit var viewPagerAdapter: ViewPagerAdapter

    private val TABS_TO_SHOW = Tab.values()

    private val DEFAULT_TAB = Tab.HOME

    private var currentTab: Tab = DEFAULT_TAB

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val tabScreen = getTabScreenByMenuItem(item)
        tabScreen?.let {
            openScreen(it)
        }

        false
    }

    private var currentAppBarView: View? = null

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(PAGE_ITEM, getViewPager().currentItem)
    }

  private val TAB_TO_SCREEN_TABLE: List<Pair<Tab, List<Class<out SupportAppScreen>>>> =
        listOf(
            Pair(Tab.MasterRecord, listOf(Screens.AppointmentsListScreen::class.java)),
            Pair(Tab.HomeScreen, listOf(Screens.CategoryScreen::class.java))
        )


    fun openScreen(supportAppScreen: SupportAppScreen) {
 val tab =
            TAB_TO_SCREEN_TABLE.find {
                it.second.find { it.canonicalName == supportAppScreen.javaClass.canonicalName } != null
            }


        if (tab != null) {
            openScreen(tab.first)
            val current = viewPagerAdapter.getCurrentFragment()
            if (current is TabContainerFragment) {
                val storedTab = current.getCurrentTabScreen()
                if (storedTab == tab.first) {
                    current.toHome()
                    if (tab.first.screen.javaClass != supportAppScreen.javaClass) {
                        current.getRouter()?.navigateTo(supportAppScreen)
                    }
                }
            }
        }
    }

    fun openScreen(tabScreen: Tab) {
        currentTab = tabScreen
        setViewPagerTabSelection(tabScreen)
        setBottomTabSelection(tabScreen)
        setApplicationBar(tabScreen)
    }

    override fun invalidateApplicationBar() {
        val currentTab = currentTab
        Log.d("TAB_DEBUG", "invalidateApplicationBar: " + currentTab.toString())
        setApplicationBar(currentTab)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_nav)

        initialize()

        if (savedInstanceState == null) {
            openScreen(DEFAULT_TAB)
        } else {
            val oldTab = getOldTabSelection(savedInstanceState) ?: DEFAULT_TAB
            openScreen(oldTab)
        }

        KeyboardUtils.addKeyboardToggleListener(this, keyboardListener)
    }

    private val keyboardListener = object : KeyboardUtils.SoftKeyboardToggleListener {
        override fun onToggleSoftKeyboard(isVisible: Boolean) {
            bottom_navigation_view.visibleOrGone(!isVisible)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        KeyboardUtils.removeKeyboardToggleListener(keyboardListener)
    }


    private fun getOldTabSelection(savedInstanceState: Bundle?): Tab? {
        val oldTabSelection = savedInstanceState?.getInt(PAGE_ITEM, -1)!!
        if (oldTabSelection == -1) return null

        return getTabByIndex(oldTabSelection)
    }

    override fun invalidateBottomView() {
        setBottomView()
    }

    //--------------------------INITIALIZATION ------------------------------------------------------

    private fun initialize() {
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager).also {
            it.onNewFragment = {
                invalidateApplicationBar()
                invalidateBottomView()
            }
        }
        initBottomNaviagtionView()
        setupViewPagerAndNavBar()
    }

    private fun setupViewPagerAndNavBar() {

        val fragments = Tab.values().map {
            TabContainerFragment.getNewInstance(it)
        }

        viewPagerAdapter.setTabs(fragments)
        slide_viewpager.adapter = viewPagerAdapter
    }

    protected fun getViewPager(): ViewPager {
        return slide_viewpager
    }

    override fun onBackPressed() {
        val fragment = getCurrentFragment()

        if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()
        ) {
            return
        } else {
            (this as RouterProvider).getRouter().exit()
            return
        }
    }

    //----------------------HELPERS-----------------------------------------
    fun setApplicationBar(tabScreen: Tab) {

        val fragment = getCurrentFragmentInstance()

        val appBar = if (fragment is ApplicationBarProvider) {
            fragment.getApplicationBarLayout(coordinatorLayout, layoutInflater)
        } else {
            null
        }

        removeOldApplicationBar()

        if (appBar != null) {
            coordinatorLayout.addView(appBar.view, 0)
            currentAppBarView = appBar.view
        }
    }

    private fun getCurrentFragmentInstance(): Fragment? {

        val fragment = getCurrentFragment()
        val position = viewPagerAdapter.getCurrentFragmentPosition()
        val index = getTabIndex(currentTab)
        val result = if (position == index && fragment != null && fragment.isAdded) {
            fragment
        } else {
            null
        }
        return result
    }

    fun setBottomView() {
        val fragment = getCurrentFragmentInstance()

        val view = if (fragment is BottomViewProvider) {
            fragment.getBottomView(coordinatorLayout, layoutInflater)
        } else {
            null
        }

        bottomView.removeAllViews()

        if (view != null) {
            bottomView.addView(view)
        }
    }

    private fun removeOldApplicationBar() {
        currentAppBarView?.let {
            coordinatorLayout.removeView(it)
        }
        currentAppBarView = null
    }

    fun setViewPagerTabSelection(tabScreen: Tab) {
        val index = getTabIndex(tabScreen)
        getViewPager().setCurrentItem(index, false)
    }

    fun setBottomTabSelection(tabScreen: Tab) {
        val menu = getMenuItemBy(tabScreen)
        menu?.let {
            setTabSelection(menu)
        }
    }

    private fun getTabIndex(tabScreen: Tab): Int {
        return TABS_TO_SHOW.indexOf(tabScreen)
    }

    private fun getTabByIndex(index: Int): Tab {
        return TABS_TO_SHOW[index]
    }

    private fun getCurrentFragment(): Fragment? {
        val i = viewPagerAdapter.getCurrentFragment()

        return i
    }

    //------------------------HELPERS_BOTTOM_NAVIAGTION_VIEW---------------------------------


    private fun initBottomNaviagtionView() {

        bottom_navigation_view.itemIconTintList = null

        bottom_navigation_view.menu.clear()

        for (tab in TABS_TO_SHOW) {

            val menuItem = bottom_navigation_view.menu.add(Menu.NONE, tab.ordinal, Menu.NONE, "")

            menuItem.setIcon(tab.icon)
        }

        bottom_navigation_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private fun getMenuItemBy(tabScreen: Tab): MenuItem? {
        val menuItem = bottom_navigation_view.menu.findItem(tabScreen.ordinal)
        return menuItem
    }

    private fun getTabScreenByMenuItem(menuItem: MenuItem): Tab? {
        return Tab.values().find { it.ordinal == menuItem.itemId }
    }

    private fun setTabSelection(menuItemToSelect: MenuItem?) {

        getAllMenuItems(bottom_navigation_view).forEach {
            clearIconTabSelection(it)
        }

        menuItemToSelect?.let { m ->

            val clickedTabScreen = TABS_TO_SHOW.find { it.ordinal == m.itemId }!!
            m.setIcon(clickedTabScreen.iconActive)
            m.isChecked = true
        }

        //reset tab selection if opened screen not present on tab bar menu
        if (menuItemToSelect == null) {
            setCheckable(bottom_navigation_view, false)
        } else {
            setCheckable(bottom_navigation_view, true)
        }
    }


    private fun setCheckable(view: BottomNavigationView, checkable: Boolean) {
        val menu = view.menu
        for (i in 0 until menu.size()) {
            menu.getItem(i).setCheckable(checkable)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun getAllMenuItems(bottomNavigationView: BottomNavigationView): List<MenuItem> {
        val menu = bottomNavigationView.menu as BottomNavigationMenu

        val result = ArrayList<MenuItem>()
        for (i in 0 until menu.size()) {
            val menuItem = menu.getItem(i)
            result.add(menuItem)
        }

        return result
    }

    private fun clearIconTabSelection(menuItem: MenuItem) {
        val screen = TABS_TO_SHOW.findLast { it.ordinal == menuItem.itemId }
        menuItem.setIcon(screen!!.icon)
    }
}
*/
