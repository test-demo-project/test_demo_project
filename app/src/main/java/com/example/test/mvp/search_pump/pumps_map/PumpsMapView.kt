package com.example.test.mvp.search_pump.pumps_map

import com.example.test.common.extension.CallBackK
import com.example.test.common.map.google.PumpItem
import com.example.test.common.map.model.MapBounds
import com.example.test.common.map.model.MapCoordinate
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface PumpsMapView: BaseScreenView{

    @AddToEndSingle
    fun setPumps(pumps: List<PumpItem>)

    @OneExecution
    fun moveMap(mapCoordinate: MapCoordinate,smooth:Boolean)

    @OneExecution
    fun setMapBounds(mapBounds: MapBounds,smooth: Boolean)

    @AddToEndSingle
    fun setDetectingLocation(visible:Boolean)

    @OneExecution
    fun askPermission(callback:CallBackK<Boolean>)

    @OneExecution
    fun checkPermission(callback: CallBackK<Boolean>)

 /*   @OneExecution
    fun checkLocationAvailable(callback: CallBackK<Boolean>)*/

    /*@OneExecuti

    on
    fun checkLocationPermission(callback: CallBackK<Boolean>)

    @OneExecution
    fun requestLocationPermission(callback: CallBackK<Boolean>)*/
}