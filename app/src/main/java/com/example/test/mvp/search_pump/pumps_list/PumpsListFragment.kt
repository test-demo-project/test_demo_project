package com.example.test.mvp.search_pump.pumps_list

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.test.R
import com.example.test.common.permission.CommonPermissions
import com.example.test.common.placeholders.NewPlaceholderLayout
import com.example.test.common.utils.location.LocationCallback
import com.example.test.common.view.bind
import com.example.test.common.view.list.item_decoration.SpaceItemDecoration
import com.example.test.databinding.FragmentPumpsListBinding
import com.example.test.mvp.base.fragment.list.ListFragment
import com.google.android.gms.maps.model.LatLng
import moxy.presenter.InjectPresenter


class PumpsListFragment() : ListFragment<PumpumpsListView, PumpsListPresenter, Any>(),
    PumpumpsListView,LocationCallback {

    @InjectPresenter
    override lateinit var presenter: PumpsListPresenter

    private var _binding: FragmentPumpsListBinding? = null
    private val binding get() = _binding!!
    private val adapter = PumpsListAdapter()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun getAdapter(): RecyclerView.Adapter<*> {
        return adapter
    }

    override fun getRecyclerView(): RecyclerView {
        return binding.pumpsListRv
    }

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout? {
        return binding.listSwipeRefresh
    }

    override fun getPlaceholderView(): NewPlaceholderLayout {
        return binding.placeholderLayout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLocationProvider().getLocationUpdates(this)

        adapter.onPumpClicked = {
            openPumpDetails(it)
        }
    }

    private fun openPumpDetails(pumpItem:PumpItem){
       // getRouter()?.navigateTo(WaterPumpFragment.Screen((pumpItem.pumpId)))
    }

    override fun onDestroy() {
        getLocationProvider().removeLocationUpdates(this)
        super.onDestroy()
    }

    override fun setLocation(location: Location) {
        presenter.onLocationChanged(LatLng(location.latitude,location.longitude))
    }

    override fun locationAvailable(available: Boolean) {
        super.locationAvailable(available)
        presenter.onLocationAvailabilityChanged(available)
    }

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPumpsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.searchInput.editText.bind(viewLifecycleOwner, presenter.search)

        onLocationPermissionGranted(checkPermission(CommonPermissions.LOCATION_PERMISSION))
    }

    override fun updateItems(items: List<Any>) {
        adapter.updateItems(items)
    }

    override fun applyItemDecorator(recyclerView: RecyclerView) {
        super.applyItemDecorator(recyclerView)
        recyclerView.addItemDecoration(
            SpaceItemDecoration(
                requireContext(),
                R.drawable.item_decorator_pumps,
                SpaceItemDecoration.VERTICAL,
                false
            )
        )
    }

    override fun showLocationPermissionRequiredPlaceholder(show: Boolean,city:String?) {
        binding.rootPlaceholderLayout.showProgressBar(false)

        binding.rootPlaceholderLayout.showMessageWithButtons(
            show = show,
            message = getString(R.string.location_persmision_is_required),
            positiveButtonText = getString(R.string.give_gpe_permission),
            onPositiveButtonClicked = {
                requestPermissions(CommonPermissions.LOCATION_PERMISSION.toTypedArray()){
                    onLocationPermissionGranted(it)
                }
            },
            negativeButtonText = getString(R.string.show_pupms_in_my_city,city),
            onNegativeButtonClicked = {
                presenter.onShowPumpsInCity()
            }
        )
    }

    private fun onLocationPermissionGranted(granted:Boolean){
        presenter.onLocationPermissionGranted(granted)
    }

    override fun showLocationDetectingPlaceholder(show: Boolean) {
        binding.rootPlaceholderLayout.showMessageWithButtons(false)
        binding.rootPlaceholderLayout.showProgressBar(show,getString(R.string.location_detecting))
    }

    override fun showLocationServiceInNotAvailable(show: Boolean,city:String?) {
        binding.rootPlaceholderLayout.showMessageWithButtons(
            show = show,
            message = getString(R.string.location_service_is_turened_off),
            positiveButtonText = getString(R.string.turn_on_location),
            onPositiveButtonClicked = {
                navigateToLocationSettings()
            },
            negativeButtonText = getString(R.string.show_pupms_in_my_city,city),
            onNegativeButtonClicked = {
                presenter.onShowPumpsInCity()
            }
        )
    }

    private fun navigateToLocationSettings(){
        val callGPSSettingIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(callGPSSettingIntent)
    }
}