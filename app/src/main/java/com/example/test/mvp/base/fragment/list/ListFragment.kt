package com.example.test.mvp.base.fragment.list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import com.balinasoft.themoviedb.common.view.list.EndlessScrollListener
import com.example.test.common.view.list.adapter.CoolAdapter
import com.example.test.common.view.list.adapter.LoadingSmartAdapter

import org.jetbrains.anko.support.v4.onRefresh

abstract class ListFragment<VIEW : ListView<ITEM>, PRESENTER : BaseScreenPresenter<VIEW>, ITEM : Any>() :
    BaseScreenFragment<VIEW, PRESENTER>(), ListView<ITEM> {

    protected abstract fun getAdapter(): RecyclerView.Adapter<*>

    private var endlessScrollListener: MyEndlessScrollListener? = null

    inner class MyEndlessScrollListener(layoutManager: LinearLayoutManager) :
        EndlessScrollListener(layoutManager) {
        override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
            val presenter = presenter
            if (presenter is PaginatorListPresenter<*, *>) {
                presenter.loadNextPage()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        getSwipeRefreshLayout()?.onRefresh {
            getSwipeRefreshLayout()?.isRefreshing = presenter.refresh()
        }
    }

    private fun initRecyclerView() {

        getRecyclerView().also {
            val layoutManager = getLayoutManager()

            if (layoutManager is LinearLayoutManager) {
                endlessScrollListener = MyEndlessScrollListener(layoutManager)
                it.addOnScrollListener(endlessScrollListener!!)
            }

            it.adapter = getAdapter()
            it.layoutManager = layoutManager
            applyItemDecorator(it)
        }
    }

    protected fun invalidateAdapter() {
        getRecyclerView().adapter = getAdapter()
    }

    open fun getLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context)
    }

    open fun applyItemDecorator(recyclerView: RecyclerView) {}

    abstract fun getRecyclerView(): RecyclerView

    abstract fun getSwipeRefreshLayout(): SwipeRefreshLayout?

    override fun scrollToTop() {
        getRecyclerView().scrollToPosition(0)
    }

    override fun onLoadDone() {
        getSwipeRefreshLayout()?.isRefreshing = false
    }

    override fun updateList() {
        val a = getAdapter()
        if (a is CoolAdapter) {
            a.update()
        } else {
            a.notifyDataSetChanged()
        }
    }

    override fun showProgressBarInList(show: Boolean) {
        val adapter = getAdapter()
        if(adapter is LoadingSmartAdapter){
            adapter.showProgressBarInAdapter(show)
        }
    }

    override fun resetPagination() {
        endlessScrollListener?.resetState()
    }

    override fun updateWithoutAnimations() {
        getAdapter().notifyDataSetChanged()
    }
}



