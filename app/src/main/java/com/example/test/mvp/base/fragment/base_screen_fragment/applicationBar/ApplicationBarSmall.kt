package com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.R

import com.example.test.common.extension.CallBackKUnit
import com.example.test.common.extension.onClick
import com.example.test.common.extension.visibleOrGone
import kotlinx.android.synthetic.main.view_application_bar_small.view.*


open class ApplicationBarSmall(layoutInflater: LayoutInflater, container: ViewGroup) :
    ApplicationBarViewHolder {

    private val view:View

    init{
        view = LayoutInflater.from(container.context).inflate(R.layout.view_application_bar_small,null)
        view.back_button.onClick {
            onBackButtonClicked?.invoke()
        }
    }

    override var onBackButtonClicked: CallBackKUnit = {}


    init {

    }

    override fun setBackButtonVisibility(visible: Boolean) {
        view.back_button.visibleOrGone(visible)
    }

    override fun setText(text: String) {

    }

    override fun getView(): View {
        return view
    }
}
