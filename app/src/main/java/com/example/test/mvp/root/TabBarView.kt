package com.example.test.mvp.root

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.alias.AddToEndSingle

interface TabBarView: BaseScreenView {

    @AddToEndSingle
    fun setTab(tab: Tab)
}
