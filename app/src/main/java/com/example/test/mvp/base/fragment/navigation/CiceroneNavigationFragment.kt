package com.example.test.mvp.base.fragment.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.test.R
import com.example.test.cicerone.LocalCiceroneHolder
import com.example.test.common.interfaces.BackButtonListener
import com.example.test.common.interfaces.RouterProvider
import com.example.test.di.DaggerUtils
import com.example.test.mvp.base.fragment.base_fragment.BaseFragment
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Navigator
import com.github.terrakok.cicerone.Router
import com.github.terrakok.cicerone.androidx.AppNavigator


abstract class CiceroneNavigationFragment() : Fragment(), RouterProvider, BackButtonListener {

    override fun getRouter(): Router {
        return router_m
    }

    private var navigator: MySupportAppNavigator? = null

    val ciceroneHolder: LocalCiceroneHolder = DaggerUtils.appComponent.provideCiceroneHolder()

    lateinit var cicerone: Cicerone<Router>

    lateinit var router_m: Router

    private var inited: Boolean = false


    inner class MySupportAppNavigator(
        activity: FragmentActivity,
        containerId: Int,
        fragmentManager: FragmentManager
    ) : AppNavigator(
        activity = activity,
        containerId = containerId,
        fragmentManager = fragmentManager
    ) {


        override fun setupFragmentTransaction(
            fragmentTransaction: FragmentTransaction,
            currentFragment: Fragment?,
            nextFragment: Fragment?
        ) {
            super.setupFragmentTransaction(fragmentTransaction, currentFragment, nextFragment)

            if (currentFragment is BaseFragment) {
                ///val level = currentFragment.getLevel()

                val arguments = nextFragment?.arguments ?: Bundle()

                arguments.putInt(BaseFragment.CURRENT_STACK_SIZE, localStackCopy.size)

                nextFragment?.arguments = arguments
            }

            if(currentFragment!=null)
                fragmentTransaction.setCustomAnimations(
                    R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left,

                    R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right
                )

        }

        fun getStackSize(): Int {
            return localStackCopy.size
        }
    }



    fun getCurrentStackSize(): Int? {
        return navigator?.getStackSize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        cicerone = ciceroneHolder!!.getCicerone(getCiceroneQuniqueName()!!)

        router_m = cicerone.router

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab_container, container, false)
    }

    open fun getContainerId(): Int {
        return R.id.ftc_container
    }

    abstract fun initializeRouter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (getChildFragmentManager().findFragmentById(R.id.ftc_container) == null) {
            initializeRouter()
        }
    }

    override fun onResume() {
        super.onResume()
        cicerone.getNavigatorHolder().setNavigator(getNavigator())
    }

    override fun onPause() {
        cicerone.getNavigatorHolder().removeNavigator()
        super.onPause()
    }

    fun getNavigator(): Navigator {
        var nav = navigator
        if (nav == null) {
            nav = MySupportAppNavigator(requireActivity(), getContainerId(), childFragmentManager)
            navigator = nav
        }
        return nav
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(getContainerId())

        if (fragment is BackButtonListener) {
            val hangled = fragment.onBackPressed()
            if (!hangled) {
                getRouter()?.exit()
            }
            return true
        } else {
            getRouter().exit()
            return true
        }
    }

    fun getCurrentFragment(): Fragment? {
        return childFragmentManager.findFragmentById(getContainerId())
    }

    fun setCiceroneUniqueName(name: String) {
        val arguments = getArgumenstOrCreate()
        arguments.putString(CICERONE_UNIQUE_NAME, name)
    }

    fun getCiceroneQuniqueName(): String? {
        return arguments?.getString(CICERONE_UNIQUE_NAME)
    }

    fun getArgumenstOrCreate(): Bundle {
        var arguments = this.arguments
        if (arguments == null) {
            arguments = Bundle()
        }
        this.arguments = arguments
        return arguments
    }

    companion object {
        val CICERONE_UNIQUE_NAME = "CICERONE_UNIQUE_NAME"
        val disableFragmentAnimation: Boolean = false
    }
}
