package com.example.test.mvp.search_pump.pumps_list

import android.view.ViewGroup
import com.example.test.R
import com.example.test.common.view.list.adapter.SimpleViewHolder
import com.example.test.databinding.ItemFavoriteMumpsDividerBinding

class CategoryDividerViewHolder(parent: ViewGroup) : SimpleViewHolder<CategoryDivider>(
    parent,
    R.layout.item_favorite_mumps_divider
) {

    private val binding: ItemFavoriteMumpsDividerBinding

    init {
        binding = ItemFavoriteMumpsDividerBinding.bind(itemView)
    }

    override fun onBind(item: CategoryDivider) {
        binding.dividerIcon.setImageResource(item.icon)
        binding.dividerText.text = item.text
    }
}
