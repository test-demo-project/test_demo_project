package com.example.test.mvp.base.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.test.mvp.base.fragment.navigation.CiceroneNavigationFragment
import com.github.terrakok.cicerone.androidx.FragmentScreen

class AutoFragmentActivity : BaseFragmentActivity() {

    companion object {
        const val FRAGMENT = "FRAGMENT"
        const val ARGUMENT = "ARGUMENT"

        inline fun <reified T : Fragment> createIntent(
            context: Context,
            bundle: Bundle? = null
        ): Intent {
            return Intent(context, AutoFragmentActivity::class.java).apply {
                if (bundle != null)
                    putExtras(bundle)
                putExtra(FRAGMENT, T::class.java.name)
            }
        }

        inline fun <reified T : Fragment> start(context: Context, bundle: Bundle? = null) {
            context.startActivity(createIntent<T>(context, bundle))
        }
    }

    class NavigationFragment : CiceroneNavigationFragment() {

        companion object {
            private const val ARGS = "ARGS"
            fun newInstance(fragmentClassName: String, bundle: Bundle?): NavigationFragment {
                return NavigationFragment().also {
                    it.setCiceroneUniqueName(fragmentClassName + "Cicerone")
                    it.arguments?.putString(FRAGMENT, fragmentClassName)
                    it.arguments?.putBundle(ARGS, bundle?.apply { remove(FRAGMENT) })
                }
            }
        }

        override fun initializeRouter() {
            val fragment = parseFragment()
            val screen = FragmentScreen{
                fragment
            }
            getRouter().newRootScreen(screen)
        }

        private fun getFragmentClassName(): String {
            return requireArguments().getString(FRAGMENT)!!
        }

        private fun parseFragment(): Fragment {
            val className = getFragmentClassName()
            val fragment = Class.forName(className).newInstance() as Fragment
            fragment.arguments = arguments?.getBundle(ARGS)
            return fragment
        }
    }

    override fun getFragment(): Fragment {
        return NavigationFragment.newInstance(getFragmentClassName(), intent.extras)
    }

    private fun getFragmentClassName(): String {
        return intent.getStringExtra(FRAGMENT)!!
    }
}