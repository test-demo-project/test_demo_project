package com.example.test.mvp.root

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

import com.example.test.common.extension.CallBackK


class ViewPagerAdapter(fragmentManager:FragmentManager, lifecycle: Lifecycle) :
     FragmentStateAdapter(fragmentManager,lifecycle) {

    private val fragments = ArrayList<NavigationContainerBuilder>()

    interface NavigationContainerBuilder{
        fun create():Fragment
        fun getName():String
    }

    fun setTabs(tabs: List<NavigationContainerBuilder>) {
        fragments.clear()
        fragments.addAll(tabs)
        notifyDataSetChanged()
    }

    fun getName(position: Int):String{
        return fragments[position].getName()
    }

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position].create()
    }

    /*override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }*/

    private var mCurrentFragment: Fragment? = null
    private var mCurrentPosition: Int? = null

    fun getCurrentFragmentPosition(): Int? {
        return mCurrentPosition
    }

    fun getCurrentFragment(): Fragment? {
        return mCurrentFragment
    }

    var onNewFragment: CallBackK<Fragment>? = null


   /* override fun setPrimaryItem(container: ViewGroup, position: Int, obj: Any) {
        if (getCurrentFragment() !== obj) {
            mCurrentFragment = obj as Fragment
            mCurrentPosition = position

            onNewFragment?.invoke(obj)
        }
        super.setPrimaryItem(container, position, obj)
    }*/
}
