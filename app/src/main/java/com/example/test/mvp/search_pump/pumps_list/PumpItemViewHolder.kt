package com.example.test.mvp.search_pump.pumps_list

import android.view.ViewGroup
import com.example.test.R
import com.example.test.common.extension.onClick
import com.example.test.common.view.list.adapter.SimpleViewHolder
import com.example.test.databinding.ItemPumpBinding

class PumpItemViewHolder(parent:ViewGroup): SimpleViewHolder<PumpItem>(parent,R.layout.item_pump){

    private val binding: ItemPumpBinding

    init{
        binding = ItemPumpBinding.bind(itemView)
    }

    override fun onBind(item: PumpItem) {
        binding.pumpItemRoot.contentDescription = getString(R.string.pump_item_content_description,item.number,item.address,item.distanceInMeters?.let { contentDescription(item.distanceInMeters) })

        binding.addressTv.text = "${item.number}. ${item.address}"
       // binding.addressTv.contentDescription =item.distanceInMeters?.let {  contentDescription(it) }

        binding.distanceTv.text = item.distanceInMeters?.let { formatDistance(it) }?:"-"

        binding.pumpItemRoot.onClick {
            onClick?.invoke(item)
        }
    }

    private fun formatDistance(meters:Int):String{
        if(meters>1000){
            return getString(R.string.distance_km_template,meters/1000f)
        }else{
            return getString(R.string.distance_m_template,meters)
        }
    }

    private fun contentDescription(meters:Int):String{
        if(meters>1000){
            return getString(R.string.distance_km_template_content_description,meters/1000f)
        }else{
            return getString(R.string.distance_m_content_description,meters)
        }
    }
}