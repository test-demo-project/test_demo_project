package com.example.test.mvp.splash_screen

import com.example.test.R
import com.example.test.common.errors.showMessage
import com.example.test.common.extension.launchUI
import com.example.test.common.extension.safeRequest
import com.example.test.common.extension.showMessageDialog
import com.example.test.common.utils.mintTimeExecute
import com.example.test.di.DaggerUtils
import com.example.test.interactor.UserInteractor
import com.example.test.interactor.WaterPumpsInteractor
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class SplashScreenPresenter : BaseScreenPresenter<SplashScreenView>() {

    @Inject
    lateinit var waterPumpsInteractor: WaterPumpsInteractor

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerUtils.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadData()

        launchUI {

        }
    }



    private fun loadData() {
        safeRequest({
            mintTimeExecute(3000) {
                withContext(Dispatchers.IO) {
                    waterPumpsInteractor.synchronize()
                    if(userInteractor.isUserLogined()){
                        userInteractor.getUser()
                    }
                }
            }

            viewState.openNextScreen()

        }, {
            viewState.showMessage(it) {
                viewState.showMessageDialog(
                    message = it,
                    positiveButtonId = R.string.try_again_bt_text,
                    cancelable = false,
                    onPositiveClicked = {
                        loadData()
                    },
                    negativeButton = R.string.close_app,
                    onNegativeClicked = {
                        viewState.closeApp()
                    }
                )
            }

        }, {

        })
    }



}