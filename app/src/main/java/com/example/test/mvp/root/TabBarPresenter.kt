package com.example.test.mvp.root

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import moxy.InjectViewState


@InjectViewState
class TabBarPresenter: BaseScreenPresenter<TabBarView>(){

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.setTab(Tab.ADDRESSES)
    }



}
