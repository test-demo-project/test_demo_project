package com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar

import android.view.*
import com.example.test.R
import com.example.test.common.extension.CallBackK
import com.example.test.common.extension.CallBackKUnit
import kotlinx.android.synthetic.main.view_standart_application_toolbar.view.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource

class StandardApplicationBar(container: ViewGroup): ApplicationBarViewHolder{

    private val view = LayoutInflater.from(container.context).inflate(R.layout.view_standart_application_toolbar,container,false)

    override fun setBackButtonVisibility(visible: Boolean) {
        if(visible){
            view.toolbar.navigationIconResource = R.drawable.ic_back
        }else{
            view.toolbar.navigationIcon = null
        }
        view.toolbar.navigationContentDescription = view.context.getString(R.string.back_button_content_description)
    }

    override fun setText(text: String) {
        view.toolbar_title.text=  text
    }

    override fun getView(): View {
        return view
    }
    override var onBackButtonClicked: CallBackKUnit = {}

    fun setMenu(menu:Int){
        view.toolbar.inflateMenu(menu)
    }

    var onMenuItemClicked: CallBackK<Int>? = null

    init{
        view.toolbar.setNavigationOnClickListener {
            onBackButtonClicked.invoke()
        }
        view.toolbar.setOnMenuItemClickListener(androidx.appcompat.widget.Toolbar.OnMenuItemClickListener {

            onMenuItemClicked?.invoke(it.itemId)

            true
        })
    }

    fun getMenu(): Menu {
        return view.toolbar.menu
    }

    fun getToolbar():androidx.appcompat.widget.Toolbar{
        return view.toolbar
    }
}