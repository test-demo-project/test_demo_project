package com.example.test.mvp.splash_screen

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.alias.OneExecution

interface SplashScreenView: BaseScreenView{

    /*@OneExecution
    fun startApplication()

    @OneExecution
    fun startAuthorisation()

    @OneExecution
    fun startUserAgreement(isAuthorised:Boolean)
*/
    @OneExecution
    fun openNextScreen()

    @OneExecution
    fun closeApp()

}