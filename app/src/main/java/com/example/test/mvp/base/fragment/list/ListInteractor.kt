package com.example.test.mvp.base.fragment.list


interface ListInteractor<ITEM_VIEW : Any> {

    suspend fun load(page: Int, search: String? = null): List<ITEM_VIEW>


}
