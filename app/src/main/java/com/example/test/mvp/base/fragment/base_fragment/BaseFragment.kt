package com.example.test.mvp.base.fragment.base_fragment
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.test.R
import com.example.test.common.extension.CallBackK
import com.example.test.common.interfaces.BackButtonListener
import com.example.test.common.interfaces.RouterProvider
import com.example.test.common.permission.CommonPermissions
import com.example.test.common.permission.IPermissionChecker
import com.example.test.common.utils.location.ILocataionProvider
import com.github.terrakok.cicerone.Router
import moxy.MvpAppCompatFragment
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File

abstract class BaseFragment: MvpAppCompatFragment(), BackButtonListener,RouterProvider {

    fun checkPermission(permissions:List<String>):Boolean{
        val permissionChecker = activity
        if(permissionChecker is IPermissionChecker){

            return permissionChecker.checkPermissions(permissions.toTypedArray())

        }else{
            throw Exception("Activity does not implement IPermissionChecker")
        }
    }

    fun requestPermissions(permissions:Array<String>, callback: CallBackK<Boolean>){
        val permissionChecker = activity
        if(permissionChecker is IPermissionChecker){

            permissionChecker.requestPermissions(permissions,callback)

        }else{
            throw Exception("Activity does not implement IPermissionChecker")
        }
    }

    private var easyImage: EasyImage? = null

    fun selectPhoto(isMultiSelect: Boolean = false, requestCode: Int = 0) {
        requetImagePermissions {
            if (it) {
                Log.d("MY_TAG", "i am gere")
                easyImage = EasyImage.Builder(requireContext())
                    .setCopyImagesToPublicGalleryFolder(false)
                    .allowMultiple(isMultiSelect)

                    .build()

                easyImage?.openChooser(this)

            }
        }
    }

    protected fun requetImagePermissions(callBackK: CallBackK<Boolean>) {
        requestPermissions(CommonPermissions.IMAGE_PERMISSIONS.toTypedArray()){
            callBackK.invoke(it)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage?.handleActivityResult(requestCode,resultCode,data,requireActivity(),object : EasyImage.Callbacks{
            override fun onCanceled(source: MediaSource) {

            }

            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                Toast.makeText(
                    context,
                    getString(R.string.error_in_pic_image),
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                onImagePickedSuccessfully(imageFiles.map { it.file })
            }
        })

    }

    open fun onImagePickedSuccessfully(files: List<File>) {}

    fun getLocationProvider(): ILocataionProvider {
        return activity as ILocataionProvider
    }

    override fun getRouter(): Router? {

        parentFragment?.let {
            if (it is RouterProvider)
                return it.getRouter()
        }
        activity?.let {
            if (it is RouterProvider) {
                return it.getRouter()
            }
        }

        return null
    }

    override fun onBackPressed(): Boolean {
        if (getRouter() != null) {
            getRouter()?.exit()
            return true
        } else {
            return false
        }
    }


    fun getLevel(): Int {
        val level = arguments?.getInt(CURRENT_STACK_SIZE, 0) ?: 0
        val myLevel = level
        return myLevel
    }

    companion object {
        val CURRENT_STACK_SIZE = "PREW_LEWEL"
        val ARGUMENT = "ARGUMENT"
    }
}