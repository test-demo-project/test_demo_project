package com.example.test.mvp.authorisation_phone

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface AuthorisationPhoneView:BaseScreenView{

    @AddToEndSingle
    fun setAuthButtonActive(active:Boolean)


    @OneExecution
    fun hideKeyboard()

}