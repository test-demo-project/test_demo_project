package com.example.test.mvp.authorisation_phone

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.test.common.utils.PhoneMaskApplyer
import com.example.test.BuildConfig
import com.example.test.R
import com.example.test.common.extension.hideSoftKeyboard
import com.example.test.common.extension.onClick
import com.example.test.common.view.bind
import com.example.test.databinding.FragmentAuthorisationBinding
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import moxy.presenter.InjectPresenter


class AuthorisationPhoneFragment: BaseScreenFragment<AuthorisationPhoneView,AuthorisationPhonePresenter>(),AuthorisationPhoneView{

    private var _binding: FragmentAuthorisationBinding? = null
    private val binding get() = _binding!!

    @InjectPresenter
    override lateinit var presenter: AuthorisationPhonePresenter

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAuthorisationBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        PhoneMaskApplyer().applyPhoneStyle(binding.phoneInput.editText)

        binding.phoneInput.editText.bind(viewLifecycleOwner,presenter.phoneInput)

        binding.appVersion.text = getString(R.string.app_version_template, BuildConfig.VERSION_NAME)

        binding.button.onClick {
            presenter.onAuthButtonClicked()
        }
    }

    override fun setAuthButtonActive(active: Boolean) {
        binding.button.isEnabled = active
    }

    override fun hideKeyboard() {
        activity?.hideSoftKeyboard()
    }

    /* override fun onDestroyView() {
                if(keyboardListenersAttached){
                    binding.root.remove(keyboardLayoutListener)
                }
                _binding = null

                super.onDestroyView()
            }*/
    private val keyboardListenersAttached = false


    private val keyboardLayoutListener = OnGlobalLayoutListener {
        val heightDiff: Int = binding.root.getRootView().getHeight() - binding.root.getHeight()
        val contentViewTop: Int =
            requireActivity().getWindow().findViewById<View>(Window.ID_ANDROID_CONTENT)
                .getTop()
        val broadcastManager = LocalBroadcastManager.getInstance(requireContext())
        if (heightDiff <= contentViewTop) {
            onHideKeyboard()
            val intent = Intent("KeyboardWillHide")
            broadcastManager.sendBroadcast(intent)
        } else {
            val keyboardHeight = heightDiff - contentViewTop
            onShowKeyboard(keyboardHeight)
            val intent = Intent("KeyboardWillShow")
            intent.putExtra("KeyboardHeight", keyboardHeight)
            broadcastManager.sendBroadcast(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

    }

    private fun onHideKeyboard(){

    }

    fun onShowKeyboard(height:Int){

    }
}