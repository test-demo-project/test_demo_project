package com.example.test.mvp.authorisation_phone

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.test.common.utils.PhoneMaskApplyer
import com.example.test.common.extension.runWithDialog
import com.example.test.di.DaggerUtils
import com.example.test.interactor.UserInteractor
import com.example.test.mvp.authorisation_code.AuthorisationCodeFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class AuthorisationPhonePresenter: BaseScreenPresenter<AuthorisationPhoneView>(){

    val phoneInput = MutableLiveData<String>()

    @Inject
    lateinit var userInteractor:UserInteractor

    init {
        DaggerUtils.appComponent.inject(this)
    }

    private val phoneIsValid = Transformations.map(phoneInput){
        PhoneMaskApplyer.checkPhone(it)
    }

    init{
        phoneIsValid.observeForever {
            viewState.setAuthButtonActive(it?:false)
        }
        phoneInput.value = ""
    }

    fun onAuthButtonClicked(){

        val phone = phoneInput.value

        val phoneCheck = PhoneMaskApplyer.checkPhone(phone?:"")

        if(phoneCheck==false)return

        val phoneFormatted = PhoneMaskApplyer.formatPhoneForServer(phone?:"")

        viewState.hideKeyboard()

        val timeLeft = userInteractor.getRepeatSmsTimeLeft(phoneFormatted)
        if(timeLeft==null || timeLeft<=0){
            runWithDialog(request = {
                withContext(Dispatchers.IO){
                    userInteractor.requestSmsCodeIfNeeded(phoneFormatted)
                }
                router?.navigateTo(AuthorisationCodeFragment.Screen(AuthorisationCodeFragment.Arg(phone = phoneFormatted)))
            })
        }else{
            router?.navigateTo(AuthorisationCodeFragment.Screen(AuthorisationCodeFragment.Arg(phone = phoneFormatted)))
        }

    }

}