package com.example.test.mvp.search_pump.pumps_list

import com.example.test.common.extension.CallBackK
import com.example.test.common.view.list.adapter.LoadingSmartAdapter

class PumpsListAdapter(): LoadingSmartAdapter(){

    var onPumpClicked: CallBackK<PumpItem>? = null

    init{
        registerItem(PumpItem::class.java){
            PumpItemViewHolder(it).also {
                it.onClick  = {
                    onPumpClicked?.invoke(it)
                }
            }
        }

        registerItem(CategoryDivider::class.java){
            CategoryDividerViewHolder(it)
        }
    }


}