package com.example.test.mvp.profile.cards_adapter

import android.os.Parcelable
import com.example.test.common.utils.time.MyDate
import com.example.test.data.server.model.CardDtoIn
import com.example.test.data.server.model.CreditCardService
import kotlinx.android.parcel.Parcelize

@Parcelize
class CardItem(
    val id:String,
    val number: String,
    val validDate: MyDate?,
    val systemImageRes:Int?
):Parcelable{



    companion object{
        fun build(cardDtoIn: CardDtoIn):CardItem{
            return CardItem(
                id = cardDtoIn.bindingId,
                number = cardDtoIn.mask,
                validDate = null,
                CreditCardService.values().find { it.firstChar==cardDtoIn.mask.getOrNull(0) }?.serviceImage
            )
        }
    }

    fun getLastDigits():String?{
        if(number.length==16){
            return number.substring(12,16)
        }
        return null
    }

}