package com.example.test.mvp.profile.cards_adapter

import android.view.ViewGroup
import com.example.test.common.extension.CallBackK
import com.example.test.common.view.list.adapter.SimpleAdapter
import com.example.test.common.view.list.adapter.SimpleViewHolder


class CardsAdapter(): SimpleAdapter<CardItem>(){

    var onRemove:CallBackK<CardItem>? = null
    var onEdit:CallBackK<CardItem>? = null

    override fun buildViewHolder(parent: ViewGroup): SimpleViewHolder<CardItem> {
        return CardViewHolder(parent).also {
            it.onRemove = {
                onRemove?.invoke(it)
            }

            it.onEdit = {
                onEdit?.invoke(it)
            }
        }
    }
}