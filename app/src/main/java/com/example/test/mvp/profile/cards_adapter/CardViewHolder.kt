package com.example.test.mvp.profile.cards_adapter

import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.test.R
import com.example.test.common.extension.CallBackK
import com.example.test.common.extension.context
import com.example.test.common.extension.onClick
import com.example.test.common.view.list.adapter.SimpleViewHolder
import kotlinx.android.synthetic.main.item_card_big.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CardViewHolder(parent:ViewGroup):SimpleViewHolder<CardItem>(parent, R.layout.item_card_big){

    var onRemove:CallBackK<CardItem>? = null
    var onEdit:CallBackK<CardItem>? = null

    private val cardNumberList = listOf(itemView.card_number_1,itemView.card_number_2,itemView.card_number_3,itemView.card_number_4)

    private fun applyCardNumber(number:String){

        if(number.length == 16){
            val chanks = splitPerPages(number,4)
            cardNumberList.forEachIndexed { index,textView->
                textView.text = chanks[index]
            }
        }else{
            cardNumberList.forEachIndexed { index,textView->
                textView.text = ""
            }
        }
    }

    private fun splitPerPages(str:String,size:Int):List<String>{
        val split: ArrayList<String> = ArrayList()
        for (i in 0..str.length / size) {
            split.add(str.substring(i * size, Math.min((i + 1) * size, str.length)))
        }
        return split
    }

    init{

    }

    private val format = SimpleDateFormat("MM/yy", Locale("ru"))


    override fun onBind(item: CardItem) {
        applyCardNumber(item.number)

        itemView.card_term_date.text = item.validDate?.let {  format.format(item.validDate.convertToDate()) }

        itemView.system_image.setImageDrawable(item.systemImageRes?.let {
            ContextCompat.getDrawable(context,it)
        })

        itemView.edit_bt.onClick {
            onEdit?.invoke(item)
        }

        itemView.remove_bt.onClick {
            onRemove?.invoke(item)
        }

        itemView.remove_bt.contentDescription = item.getLastDigits()?.let { context.getString(R.string.credit_card_content_description_delete,it) }
        itemView.contentDescription = item.getLastDigits()?.let { context.getString(R.string.credit_card_content_description,it) }
    }

}