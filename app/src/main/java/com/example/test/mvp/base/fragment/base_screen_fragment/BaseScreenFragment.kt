package com.example.test.mvp.base.fragment.base_screen_fragment


import android.os.Bundle
import android.view.*
import android.view.animation.Animation
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.test.R
import com.example.test.common.FragmentUtil
import com.example.test.common.extension.CallBackKUnit
import com.example.test.common.extension.toast
import com.example.test.common.extension.visibleOrInvisible
import com.example.test.common.placeholders.NewPlaceholderLayout
import com.example.test.common.view.dialog.ProgressBarDialog
import com.example.test.mvp.base.fragment.base_fragment.BaseFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.ApplicationBarViewHolder
import com.example.test.mvp.base.fragment.navigation.CiceroneNavigationFragment
import kotlinx.android.synthetic.main.fragment_base_screen.*
import kotlinx.android.synthetic.main.fragment_base_screen.view.*


abstract class BaseScreenFragment< VIEW : BaseScreenView, PRESENTER : BaseScreenPresenter<VIEW>>() :
    BaseFragment(), BaseScreenView {

    open lateinit var presenter: PRESENTER

    private var progressBarDialog: ProgressBarDialog? = null

    private var applicationBarText: String? = null

    var applicationBarViewHolder: ApplicationBarViewHolder? = null
        private set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_base_screen, container, false)

        val viewToAttach = provideYourView(inflater, view.payloadContainer, savedInstanceState)
        view.payloadContainer.addView(viewToAttach)

        val applicationBar = buildApplicationBar(inflater, view.applicationBarContainer)

        if (applicationBar != null) {
            view.applicationBarContainer.addView(applicationBar.view)
            //view.coordinator.addView(applicationBar.view,0)
            view.applicationBarShadow.visibleOrInvisible(applicationBar.showShadow)
        }

     //   view.applicationBarShadow.visibleOrInvisible(true)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.router = getRouter()

      //  placeholder_swipe_refresh.isEnabled = false

        /*getPlaceholderView().onRefreshButtonClicked = {
            presenter.refresh()
        }*/
    }

    private fun buildApplicationBar(
        inflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarView? {
        /* if (whereShowApplicationBar != PlaceToShowApplicationBar.IN_FRAGMENT) {
             return null
         }*/

        applicationBarViewHolder = getApplicationBarViewHolder(inflater, container)
        applicationBarViewHolder?.let {

            it.setBackButtonVisibility(needToShowBackButton())
            it.onBackButtonClicked = {
                onBackPressed()
            }

            /*if (it is IProfileApplicationBar) {
                it.onEditProfileClicked = {
                    editProfile()
                }
                it.onSettingsClicked = {
                    settings()
                }
            }*/

            it.setText(applicationBarText ?: "")
            return ApplicationBarView(it.getView(), true)
        }

        return null
    }

 /*   open fun editProfile() {
        getRouter()?.navigateTo(EditProfileFragment.Screen())
    }

    open fun settings() {
        getRouter()?.navigateTo(SettingsFragment.Screen())
    }
*/
    open fun getApplicationBarViewHolder(
        layoutInflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarViewHolder? {
        return null
    }

    abstract fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View

    fun getPlaceholderSwipeRefresh(): SwipeRefreshLayout? {
        return null
    }

    open fun getPlaceholderView(): NewPlaceholderLayout {
        return new_placeholder_layout
    }

  /*  override fun showMessagePlaceholderWithButton(show: Boolean, text: String) {
        getPlaceholderView().showMessageWithReloadButton(
            show,
            text,
            getString(R.string.try_again_bt_text)
        )
    }*/

    /* override fun showMessagePlaceholderWithButton(
        show: Boolean,
        text: String,
        buttonText: Int,
        onButtonClick: CallBackKUnit
    ) {
        getPlaceholderView().showMessageWithReloadButton(
            show,
            text,
            getString(buttonText)
        ) {
            onButtonClick.invoke()
        }
    }*/
    override fun showFullContentMessageWithButton(
        show: Boolean,
        text: String?,
        buttonText: Int?,
        onButtonClick: CallBackKUnit?
    ) {
        getPlaceholderView().showMessageWithReloadButton(
            show = show,
            message = text,
            reloadButtonText = getString(buttonText?:R.string.try_again_bt_text),
            buttonAction = {
                onButtonClick?.invoke()
            })

    }

    /* override fun showMessagePlaceholderWithButton(
        show: Boolean,
        text: String,
        buttonText: Int,
        onButtonClick: CallBackKUnit
    ) {

    }*/

    override fun showProgressBar(show: Boolean) {
        getPlaceholderView().showProgressBar(show)
    }

    override fun setApplicationBarTitle(title: String) {
        applicationBarText = title
        applicationBarViewHolder?.setText(title)
    }

    override fun showFullContentMessage(show: Boolean, message: String) {
        getPlaceholderView().showMessage(show, message)
    }

    override fun showProgressBarDialog(show: Boolean, message: String?) {
        progressBarDialog?.dismiss()
        if (show) {
            val newDialog = ProgressBarDialog(requireContext(), message)
            newDialog.show()
            progressBarDialog = newDialog
        }
    }

    class ApplicationBarView(
        val view: View,
        val showShadow: Boolean
    ) {
        val showMargin: Boolean = true
    }


    open fun needToShowBackButton(): Boolean {
        return !isRootScreen()
    }

    fun isRootScreen(): Boolean {
        val isRootFragment = isRootFragment()
        val isRootActivity = isRootActivity()

        if (isRootFragment == null) {
            return isRootActivity
        }
        return if (isRootFragment == false) {
            false
        } else {
            isRootActivity
        }
    }

    protected fun isRootFragment(): Boolean? {
        /*val level = getLevel()
        return level == 0*/
        val parent = parentFragment
        if(parent is CiceroneNavigationFragment){
            return parent.childFragmentManager.backStackEntryCount==0
        }
        return true
    }

    protected fun isRootActivity(): Boolean {
        val a = activity
        if (a == null) return true
        return a.isTaskRoot
    }

    override fun showToastMessage(message: String) {
        toast(message)
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (FragmentUtil.disableFragmentAnimations) {
            val a: Animation = object : Animation() {}
            a.duration = 0
            return a
        }
        return super.onCreateAnimation(transit, enter, nextAnim)
    }
}