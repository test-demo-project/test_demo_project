package com.example.test.mvp.search_pump

import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import moxy.InjectViewState

@InjectViewState
class SearchPumpPresenter:BaseScreenPresenter<SearchPumpView>() {
}