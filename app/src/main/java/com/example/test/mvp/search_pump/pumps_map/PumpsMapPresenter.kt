package com.example.test.mvp.search_pump.pumps_map

import androidx.lifecycle.Observer
import com.example.test.R
import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.common.errors.showMessage
import com.example.test.common.extension.CallBackK
import com.example.test.common.extension.safeRequest
import com.example.test.common.extension.showMessage
import com.example.test.common.map.google.PumpItem
import com.example.test.common.map.model.MapCoordinate
import com.example.test.data.repository.preferencesRepository.PreferencesRepository
import com.example.test.data.server.model.UserDtoIn
import com.example.test.di.DaggerUtils
import com.example.test.interactor.UserInteractor
import com.example.test.interactor.WaterPumpsInteractor
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.ktx.utils.sphericalDistance
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import moxy.InjectViewState
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

@InjectViewState
class PumpsMapPresenter() : BaseScreenPresenter<PumpsMapView>() {

    @Inject
    lateinit var waterPumpInteractor: WaterPumpsInteractor

    @Inject
    lateinit var preferencesRepository: PreferencesRepository

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerUtils.appComponent.inject(this)
    }

    private var locationAvailable: Boolean = false

    private var isPermissionGranted: Boolean = false

    private var currentLocation: LatLng? = null
    private var onLocationChanged: CallBackK<LatLng>? = null

    private var permissionAsked: Boolean = false

    private var cityId = preferencesRepository.currentCity?.id

    private val userListener = Observer<UserDtoIn?>{
        if(it?.region?.id!=cityId){
            refresh()
        }
    }

    init {
        userInteractor.user.observeForever(userListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        userInteractor.user.removeObserver(userListener)
    }

    private var onCameraMovedAfterLocationDetected: Boolean = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refresh()

    }

    fun setCurrentLocation(latLng: LatLng) {
        val oldLocation = currentLocation

        currentLocation = latLng
        onLocationChanged?.invoke(latLng)

        if (!onCameraMovedAfterLocationDetected) {
            viewState.moveMap(MapCoordinate(latLng), true)
            onCameraMovedAfterLocationDetected = true
        }

        //If user changed the location more than 1 km we update the map
        if (oldLocation == null || oldLocation.sphericalDistance(latLng) > 1000) {
            refresh()
        }
    }



    suspend fun getPermission(): Boolean {
        val g = CompletableDeferred<Boolean>()

        viewState.checkPermission {
            if (it) {
                g.complete(it)
            } else {
                if (!permissionAsked) {
                    viewState.askPermission {
                        g.complete(it)
                        permissionAsked = true
                    }
                } else {
                    g.complete(false)
                }
            }
        }

        return g.await()
    }

    private var locationAwailableCallback: CallBackK<Boolean>? = null

    suspend fun getLocationAvailable(): Boolean {
        return locationAvailable
    }

    private var loading = AtomicBoolean()

    fun loadWaterPumps() {
        if(!loading.compareAndSet(false,true))return

        val p = safeRequest({

            viewState.setDetectingLocation(true)

            val location = getCurrentLocationIfPossible()

            viewState.setDetectingLocation(false)

            val pumps = getWaterPumps(location)

            viewState.setPumps(pumps)

            if(location==null){
                viewState.showMessage(
                    stringProvider.getString(
                        R.string.wodomats_were_shown_in_city,
                        preferencesRepository.currentCity?.name?:""
                    )
                )
            }

        }, {
           viewState.showMessage(it)
        }, {
            viewState.setDetectingLocation(false)
            loading.set(false)
        })
    }

    private suspend fun getCurrentLocationIfPossible(): LatLng? {
        try {
            val c= currentLocation

            if(c!=null){
                return c
            }

            val permission = getPermission()
            if (!permission) return null
            val available = getLocationAvailable()
            if (!available) return null

            return getCurrentLocation()

        } catch (e: java.lang.Exception) {
            return null
        }
    }

    private suspend fun getCurrentLocation(): LatLng? {

        val current = currentLocation

        if (current != null) {
            return current
        }

        val d = CompletableDeferred<LatLng?>()

        onLocationChanged = {
            d.complete(it)
        }

        d.invokeOnCompletion {
            onLocationChanged = null
        }

        return withTimeout(10000) {
            d.await()
        }
    }

    fun onLocationAvailabilityChanged(available: Boolean) {
        val oldvalue = available
        locationAvailable = available

        if(oldvalue==false && available){
            refresh()
        }
    }

    fun onLocationPermissionPermissionGranted(granted:Boolean){
        val oldValue = isPermissionGranted
        isPermissionGranted = granted

        if(granted==true && oldValue==false){
            refresh()
        }
    }

    fun centerMapOnMe() {
        currentLocation?.let {
            viewState.moveMap(MapCoordinate(it), true)
        }
    }

    override fun refresh(): Boolean {
        load()

        return false
    }

    private fun load() {
       loadWaterPumps()
    }

    private suspend fun getWaterPumps(latLng: LatLng?):List<PumpItem>{

        val city = preferencesRepository.currentCity ?: throw ServiceException(R.string.city_not_selected)

        return withContext(Dispatchers.IO){
            val pumps = if (latLng == null) {
                waterPumpInteractor.getWaterPumpsInRegion(city.id)
            } else {
                waterPumpInteractor.getWaterPumpLocationsInRadius(latLng, 100000)
            }

            pumps.map {
                PumpItem(it.id, LatLng(it.mapLat, it.mapLon))
            }
        }

    }


}