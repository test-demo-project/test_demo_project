package com.example.test.mvp.base.fragment.base_screen_fragment

import com.example.test.data.repository.string.StringProvider
import com.example.test.di.DaggerUtils
import com.github.terrakok.cicerone.Router
import moxy.MvpPresenter

open class BaseScreenPresenter<VIEW : BaseScreenView>() : MvpPresenter<VIEW>() {

    lateinit var stringProvider: StringProvider

    init {
        stringProvider = DaggerUtils.appComponent.provideStringProvider()
    }

    var router: Router? = null

    open fun refresh(): Boolean {
        return false
    }


}