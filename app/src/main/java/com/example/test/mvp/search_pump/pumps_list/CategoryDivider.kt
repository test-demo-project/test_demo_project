package com.example.test.mvp.search_pump.pumps_list

import com.example.test.common.view.list.adapter.ISmartModel

data class CategoryDivider(val id: Int, val icon: Int, val text: String) : ISmartModel {
    override fun areItemsTheSame(other: Any): Boolean {
        if (other !is CategoryDivider) return false
        if (other.id != id) return false
        return true
    }
}