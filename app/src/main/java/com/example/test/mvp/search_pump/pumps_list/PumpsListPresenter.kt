package com.example.test.mvp.search_pump.pumps_list

import androidx.lifecycle.Observer
import com.example.test.R
import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.data.server.model.UserDtoIn
import com.example.test.di.DaggerUtils
import com.example.test.interactor.UserInteractor
import com.example.test.interactor.WaterPumpsInteractor
import com.example.test.mvp.base.fragment.list.PaginatorListPresenter
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PumpsListPresenter : PaginatorListPresenter<PumpumpsListView, Any>(singlePage = true) {

    @Inject
    lateinit var interactor: WaterPumpsInteractor

    @Inject
    lateinit var userInteractor: UserInteractor

    private val waterPumpsChangeObserver = Observer<Unit>{
        refresh()
    }



    private val userListener = Observer<UserDtoIn?>{
        if(it?.region?.id!=city){
            refresh()
        }
    }

    init {

    }

    init {
        DaggerUtils.appComponent.inject(this)

        interactor.favoriteSetChanged.observeForever(waterPumpsChangeObserver)
        userInteractor.user.observeForever(userListener)
    }

    private var city = userInteractor?.getCity()?.id

    override fun onDestroy() {
        super.onDestroy()
        interactor.favoriteSetChanged.removeObserver(waterPumpsChangeObserver)
        userInteractor.user.removeObserver(userListener)
    }

    private val favoriteAddressDivider =
        CategoryDivider(
            1,
            R.drawable.ic_favorite,
            stringProvider.getString(R.string.favorite_addresses)
        )

    private val nearsPumps =
        CategoryDivider(
            2,
            R.drawable.ic_location,
            stringProvider.getString(R.string.other_addresses_distance)
        )

    /*private val pumpsInTheCity =
        CategoryDivider(2, R.drawable.ic_location, R.string.other_addresses_distance)*/

    private var currrentLocation: LatLng? = null
    private var locationAvailable: Boolean = false
    private var locationPermissionGranted: Boolean = false
    private var isAllowToShowPumpsInTheCity: Boolean = false

    private fun processListByDistance(items: List<PumpItem>, fav: List<PumpItem>): List<Any> {

        val result = ArrayList<Any>(items.size + 2)

        val favSorted = fav.sortedBy { it.distanceInMeters }
        //val favSet = fav.buildHashMap { it.pumpId }

        val other = items.sortedBy { it.distanceInMeters }

        if (favSorted.isNotEmpty()) {
            result.add(favoriteAddressDivider)
            result.addAll(favSorted)
        }

        if (other.isNotEmpty()) {
            result.add(nearsPumps)
            result.addAll(other)
        }

        return result
    }

    private fun processListByCity(
        items: List<PumpItem>,
        city: String,
        fav: List<PumpItem>
    ): List<Any> {

        val result = ArrayList<Any>(items.size + 2)

        val favSorted = fav.sortedBy { it.distanceInMeters }
        //val favSet = fav.buildHashMap { it.pumpId }

        val other = items.sortedBy { it.number }

        if (favSorted.isNotEmpty()) {
            result.add(favoriteAddressDivider)
            result.addAll(favSorted)
        }

        if (other.isNotEmpty()) {

            val cityPumps =
                CategoryDivider(
                    2,
                    R.drawable.ic_location,
                    stringProvider.getString(R.string.pumps_in_the_city_template, city)
                )

            result.add(cityPumps)

            result.addAll(other)
        }

        return result
    }


    override suspend fun load(page: Int, search: String?): List<Any> {
        val currrentLocation = currrentLocation

        val city = userInteractor.getCity() ?: throw ServiceException(R.string.city_not_selected)



        return withContext(Dispatchers.IO) {
            val favorite = interactor.getFavoriteWaterPumps(currrentLocation).map {
                PumpItem(
                    pumpId = it.id,
                    address = it.address ?: "",
                    number = it.number.toString(),
                    distanceInMeters = it.distance,
                    isFavorite = false
                )
            }

            val pumps = if (currrentLocation == null) {
                if (isAllowToShowPumpsInTheCity) {
                    interactor.getWaterPumpsInRegion(city.id).map {
                        PumpItem(
                            pumpId = it.id,
                            address = it.address ?: "",
                            number = it.externalNumber.toString(),
                            distanceInMeters = null,
                            isFavorite = false
                        )
                    }
                } else {
                    throw com.example.test.common.errors.excetpions.ServiceException(
                        R.string.location_not_awailable
                    )
                }
            } else {
                interactor.getNearestWaterPumps(currrentLocation).map {
                    PumpItem(
                        pumpId = it.id,
                        address = it.address ?: "",
                        number = it.number.toString(),
                        distanceInMeters = it.distance,
                        isFavorite = false
                    )
                }
            }

            val filtered = pumps.filter {
                if (search.isNullOrBlank()) {
                    return@filter true
                } else {
                    return@filter filter(it, search)
                }
            }

            val favoriteFiltered = favorite.filter {
                if (search.isNullOrBlank()) {
                    return@filter true
                } else {
                    return@filter filter(it, search)
                }
            }

            if (currrentLocation != null) {
                processListByDistance(filtered, favoriteFiltered)
            } else {
                processListByCity(filtered, city.name, favoriteFiltered)
            }
        }


    }


    private fun filter(pumpItem: PumpItem, search: String): Boolean {
        if (pumpItem.number.contains(search, true)) return true
        if (pumpItem.address.contains(search, true)) return true
        if (pumpItem.pumpId.toString().contains(search, true)) return true
        return false
    }

    override fun getEmptyListText(): String {
        if (search.value.isNullOrBlank()) {
            if(currrentLocation!=null){
                return stringProvider.getString(R.string.no_water_pumps_near_by)
            }else{
                return super.getEmptyListText()
            }

        } else {
            return stringProvider.getString(R.string.search_result_is_empty)
        }
    }

    fun onShowPumpsInCity() {
        isAllowToShowPumpsInTheCity = true
        updateLocationPlaceholderState()
        refresh()
    }

    fun onLocationChanged(location: LatLng) {
        val oldLocation = currrentLocation
        currrentLocation = location

        if (oldLocation == null) {
            refresh()
        }

        updateLocationPlaceholderState()
    }

    fun onLocationPermissionGranted(accessGranted: Boolean) {
        locationPermissionGranted = accessGranted
        updateLocationPlaceholderState()
    }

    fun onLocationAvailabilityChanged(available: Boolean) {
        locationAvailable = available
        updateLocationPlaceholderState()
    }

    private fun updateLocationPlaceholderState() {
        viewState.showLocationDetectingPlaceholder(false)
        viewState.showLocationServiceInNotAvailable(false)
        viewState.showLocationPermissionRequiredPlaceholder(false)

        if (currrentLocation == null && !isAllowToShowPumpsInTheCity) {
            if (!locationPermissionGranted) {
                viewState.showLocationPermissionRequiredPlaceholder(
                    true,
                    userInteractor.getCity()?.name
                )
                return
            } else if (!locationAvailable) {
                viewState.showLocationServiceInNotAvailable(true, userInteractor.getCity()?.name)
            } else {
                viewState.showLocationDetectingPlaceholder(true)
            }
        } else {

        }
    }

}