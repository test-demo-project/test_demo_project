package com.example.test.mvp.splash_screen

import androidx.fragment.app.Fragment
import com.example.test.mvp.base.activity.BaseFragmentActivity

class SplashScreenActivity():BaseFragmentActivity(){

    override fun getFragment(): Fragment {
        return SplashScreenFragment()
    }
}