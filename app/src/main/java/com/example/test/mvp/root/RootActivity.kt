package com.example.test.mvp.root

import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.test.common.utils.location.LocationCallback
import com.example.test.mvp.base.activity.BaseFragmentActivity

class RootActivity(): BaseFragmentActivity(),LocationCallback{

    override fun getFragment(): Fragment {
        return RootFragment()
    }

    //We connecting to the location service to increase speed of location detecting
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLocationUpdates(this)
    }

    override fun onDestroy() {
        removeLocationUpdates(this)
        super.onDestroy()

    }




    override fun setLocation(location: Location) {

    }

}