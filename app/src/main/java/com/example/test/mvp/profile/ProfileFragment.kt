package com.example.test.mvp.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContract
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test.common.utils.PhoneMaskApplyer
import com.example.test.common.view.list.item_decoration.SpacesItemDecoration
import com.example.test.R
import com.example.test.common.extension.*
import com.example.test.common.view.bind
import com.example.test.data.model.CityModel
import com.example.test.data.server.model.UserDtoIn
import com.example.test.databinding.FragmentProfileBinding
import com.example.test.di.DaggerUtils
import com.example.test.mvp.base.activity.AutoFragmentActivity
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenFragment
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.ApplicationBarViewHolder
import com.example.test.mvp.base.fragment.base_screen_fragment.applicationBar.StandardApplicationBar

import com.example.test.mvp.profile.cards_adapter.CardItem
import com.example.test.mvp.profile.cards_adapter.CardsAdapter
import com.github.terrakok.cicerone.androidx.FragmentScreen
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class ProfileFragment() : BaseScreenFragment<ProfileView, ProfilePresenter>(), ProfileView {

    @InjectPresenter
    override lateinit var presenter: ProfilePresenter

    @ProvidePresenter
    fun providePresenter(): ProfilePresenter = ProfilePresenter(
        arguments?.getBoolean(
            FILL_REUQIRED_FIELDS
        ) ?: false
    )

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val cardsAdapter = CardsAdapter()

   /* private val bindCardActivity = registerForActivityResult(CardBindingFragment.Contract()){
        presenter.onAddCardScreenClosed()
    }*/

    override fun provideYourView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setApplicationBarTitle(getString(R.string.edit_profile))

        PhoneMaskApplyer().applyPhoneStyle(binding.phoneInput.editText)

        binding.phoneInput.editText.bind(viewLifecycleOwner, presenter.phone)

        binding.genderSelector.bind(viewLifecycleOwner, presenter.gender)

        binding.birhDateInput.bind(viewLifecycleOwner, presenter.birthDate)

        binding.birhDateInput.isSpinnerMode = true

        binding.emaiInput.editText.bind(viewLifecycleOwner, presenter.email)

        binding.nameInput.editText.bind(viewLifecycleOwner, presenter.name)

        presenter.city.observe(viewLifecycleOwner, Observer {
            binding.citySelector.text = it?.name
        })

        binding.citySelector.openScreen = {
            getRouter()?.setResultListener(CITY_RESULT_KEY) {
                if (it is CityModel) {
                    presenter.city.value = it
                }
            }
          //  getRouter()?.navigateTo(CityListFragment.Screen(CITY_RESULT_KEY, false))
        }

        cardsAdapter.onEdit = {

        }

        binding.addCardBt.onClick {
            onAddCardButtonClicked()
        }

        cardsAdapter.onRemove = {
            presenter.unbindCard(it)
        }

        binding.cancelBt.onClick {
            presenter.cancel()
        }

        binding.acceptBt.onClick {
            presenter.save()
        }

        binding.notificationEmail.getSwitch().bind(viewLifecycleOwner,presenter.notificationByEmail)
        binding.notificationSms.getSwitch().bind(viewLifecycleOwner,presenter.notificationBySms)

        binding.logOutButton.onClick {
            presenter.logOut()
        }

        applyCarsRv()
    }

    private fun onAddCardButtonClicked(){
     //   bindCardActivity.launch(null)
    }

    private fun applyCarsRv() {
        binding.cardsRv.also {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = cardsAdapter
            it.isNestedScrollingEnabled = false
            it.addItemDecoration(SpacesItemDecoration(0, 16.dp))
        }
    }

    override fun setCards(cards: List<CardItem>) {
        cardsAdapter.updateItems(cards)
        binding.cardsRv.visibleOrGone(!cards.isEmpty())
    }

    override fun setGenderActive(active: Boolean) {
        binding.genderSelector.isEnabled = active
    }

    override fun setBirthDateActive(active: Boolean) {
        binding.birhDateInput.isEnabled = active
    }

    override fun setPhoneActive(active: Boolean) {
        binding.phoneInput.isEnabled = active
    }

    override fun getApplicationBarViewHolder(
        layoutInflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarViewHolder {
        return StandardApplicationBar(container)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun showAllFields(show: Boolean) {
        binding.paymentMethodLabel.visibleOrGone(show)
        binding.paymentMethodLayout.visibleOrGone(show)

        binding.notificationsLayout.visibleOrGone(show)
        binding.notificationsLabel.visibleOrGone(show)

        binding.logOutButton.visibleOrGone(show)
    }

    companion object {
        val CITY_RESULT_KEY = "CITY_RESULT_KEY"
        val FILL_REUQIRED_FIELDS = "FILL_REUQIRED_FIELDS"

        val USER_RESULT = "USER_RESULT"


        fun Screen(fillRequiredFields: Boolean = false) = FragmentScreen {
            ProfileFragment().also {
                it.putBool(FILL_REUQIRED_FIELDS, fillRequiredFields)
            }
        }
    }

    init{

    }

    class Contract(): ActivityResultContract<Boolean,UserDtoIn?>(){
        override fun createIntent(context: Context, input: Boolean?): Intent {

            val intent = AutoFragmentActivity.createIntent<ProfileFragment>(context,bundle = bundleOf(
                FILL_REUQIRED_FIELDS to (input?:false))
            )

            return intent
        }

        override fun parseResult(resultCode: Int, intent: Intent?): UserDtoIn? {
            return intent?.getParcelableExtra<UserDtoIn>(USER_RESULT)
        }
    }

    override fun setProfileAndExit(user: UserDtoIn) {
        activity?.setResult(Activity.RESULT_OK,Intent().also {
            it.putExtra(USER_RESULT,user)
        })
        activity?.finish()
    }

    override fun onLogout() {
        DaggerUtils.appComponent.navigationHelper().openNextScreen()
    }
}