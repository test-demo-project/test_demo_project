package com.example.test.mvp.profile

import com.example.test.data.server.model.UserDtoIn
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenView
import com.example.test.mvp.profile.cards_adapter.CardItem
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface ProfileView: BaseScreenView{

    @AddToEndSingle
    fun setGenderActive(active:Boolean)

    @AddToEndSingle
    fun setBirthDateActive(active:Boolean)

    @AddToEndSingle
    fun setPhoneActive(active:Boolean)

    @AddToEndSingle
    fun setCards(cards:List<CardItem>)

    @AddToEndSingle
    fun showAllFields(show:Boolean)

    @OneExecution
    fun setProfileAndExit(user: UserDtoIn)

    @OneExecution
    fun onLogout()
}