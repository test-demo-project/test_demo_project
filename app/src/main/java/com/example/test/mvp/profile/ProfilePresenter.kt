package com.example.test.mvp.profile

import androidx.lifecycle.MutableLiveData
import com.example.test.R
import com.example.test.common.binding.spinner.Selectable
import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.common.errors.showMessage
import com.example.test.common.extension.*
import com.example.test.common.utils.time.MyDate
import com.example.test.data.model.CityModel
import com.example.test.data.model.Gender
import com.example.test.data.server.model.GenderEnum
import com.example.test.data.server.model.RegionDtoOut
import com.example.test.data.server.model.UpdateUserDtoOut
import com.example.test.data.server.model.UserDtoIn
import com.example.test.di.DaggerUtils
import com.example.test.interactor.BindCardInteractor
import com.example.test.interactor.UserInteractor
import com.example.test.mvp.base.fragment.base_screen_fragment.BaseScreenPresenter
import com.example.test.mvp.profile.cards_adapter.CardItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.lang.Exception
import javax.inject.Inject

@InjectViewState
class ProfilePresenter(val fillRequiredFields: Boolean) : BaseScreenPresenter<ProfileView>() {

    @Inject
    lateinit var userInteractor: UserInteractor

    @Inject
    lateinit var bindCardInteractor: BindCardInteractor

    init {
        DaggerUtils.appComponent.inject(this)
    }

    private var userData: UserDtoIn? = null

    private var userDataNotifications: UserDtoIn? = null

    val name = MutableLiveData<String>()
    val city = MutableLiveData<CityModel?>()
    val phone = MutableLiveData<String>("+79001112233")
    val email = MutableLiveData<String>()
    val birthDate = MutableLiveData<MyDate?>()
    val gender = Selectable<GenderItem>().also {
        it.variantsList.value = Gender.values().map { GenderItem(it) }
    }

    val notificationBySms = MutableLiveData<Boolean>()
    val notificationByEmail = MutableLiveData<Boolean>()

    init {
        viewState.setPhoneActive(false)

        city.value = userInteractor.getCity()

        loadUser()

        viewState.showAllFields(!fillRequiredFields)

        notificationBySms.observeForever { notificationBySms ->
            val user = userDataNotifications
            if (user != null && user.smsMailing != notificationBySms) {

                runWithDialog(request = {
                    userDataNotifications = withContext(Dispatchers.IO) {
                        userInteractor.setSms(notificationBySms)
                    }
                }, onFail = {
                    this.notificationBySms.value = userDataNotifications?.smsMailing
                    false
                })
            }
        }

        notificationByEmail.observeForever { notificationByEmail ->
            val user = userDataNotifications
            if (user != null && user.emailMailing != notificationByEmail) {

                runWithDialog(request = {
                    userDataNotifications = withContext(Dispatchers.IO) {
                        userInteractor.setEmail(notificationByEmail)
                    }
                }, onFail = {
                    this.notificationByEmail.value = userDataNotifications?.emailMailing
                    false
                })
            }
        }
    }

    inner class GenderItem(val gender: Gender) {
        override fun toString(): String {
            return when (gender) {
                Gender.FEMALE -> {
                    stringProvider.getString(R.string.gender_female)
                }
                Gender.MALE -> {
                    stringProvider.getString(R.string.gender_male)
                }
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as GenderItem

            if (gender != other.gender) return false

            return true
        }

        override fun hashCode(): Int {
            return gender.hashCode()
        }

    }

    fun save() {
        updateUser()
    }

    private fun loadUser() {
        runWithPlaceholder(reloadable = true, request = {
            refreshUser()
        }, finally = {})
    }

    private suspend fun refreshUser() {
        withContext(Dispatchers.Main) {
            val user = withContext(Dispatchers.IO) {
                userInteractor.getUser()
            }
            applyUser(user)
            applyUserNotifications(user)
        }
    }

    private fun applyUserNotifications(userDtoIn: UserDtoIn) {
        userDataNotifications = userDtoIn
        notificationByEmail.value = userDtoIn.emailMailing
        notificationBySms.value = userDtoIn.smsMailing
    }

    private fun applyUser(userDtoIn: UserDtoIn) {
        userData = userDtoIn

        name.value = userDtoIn.fullname ?: ""
        city.value = userDtoIn?.region?.let {
            CityModel(id = it.id, it.name)
        }
        phone.value = userDtoIn.phone ?: ""
        email.value = userDtoIn.email ?: ""
        birthDate.value = userDtoIn.birthday

        gender.selected.value = userDtoIn.gender?.let {
            val gender = when (it) {
                GenderEnum.FEMALE -> Gender.FEMALE
                GenderEnum.MALE -> Gender.MALE
            }
            GenderItem(gender)
        }

        viewState.setGenderActive(userDtoIn.gender == null)
        viewState.setPhoneActive(false)
        viewState.setBirthDateActive(userDtoIn.birthday == null)

        val cards = userDtoIn.cards?.map {
            CardItem.build(it)
        }

        viewState.setCards(cards ?: listOf())
    }

    private fun buildUpdateModel(): UpdateUserDtoOut {
        val city = city.value ?: throw ServiceException(R.string.city_not_selected)

        val data = UpdateUserDtoOut(
            fullname = name.value,
            region = RegionDtoOut(tid = city.id),
            birthday = birthDate.value,
            gender = gender.selected.value?.gender.let {
                when (it) {
                    Gender.MALE -> GenderEnum.MALE
                    Gender.FEMALE -> GenderEnum.FEMALE
                    else -> {
                        null
                    }
                }
            },
            smsMailing = null,
            emailMailing = null,
            email = email.value
        )
        return data
    }

    private fun updateUser() {

        try {
            val data = buildUpdateModel()

            if (fillRequiredFields) {

                if (data.email.isNullOrBlank()) {
                    throw ServiceException(R.string.email_is_empty)
                }
            }

            runWithDialog(
                message = stringProvider.getString(R.string.saving_user_data),
                reloadable = false,
                request = {

                    val updatedUser = withContext(Dispatchers.IO) {
                        userInteractor.updateProfile(data)
                    }

                    if (fillRequiredFields) {
                        viewState.setProfileAndExit(updatedUser)
                    } else {
                        applyUser(updatedUser)
                        viewState.showMessage(R.string.profile_saved)
                    }
                })

        } catch (e: Exception) {
            viewState.showMessage(e)
        }
    }

    fun cancel() {
        val user = userData
        if (user != null) {
            applyUser(user)
        }
    }

    fun logOut() {
        runWithDialog(request = {
            withContext(Dispatchers.IO) {
                userInteractor.logoutLocal()
            }

            viewState.onLogout()
        })
    }

    fun unbindCard(cardItem: CardItem) {

        viewState.showMessageDialog(
            message = stringProvider.getString(
                R.string.are_you_shore_to_remove_credit_card_template,
                cardItem.number
            ),
            positiveButtonId = R.string.delete_card,
            onPositiveClicked = {
                runWithDialog(reloadable = false, request = {
                    bindCardInteractor.unbindCard(cardItem.id)
                    refreshUser()
                })
            },
            negativeButton = R.string.cancel,
            dangerous = true
        )
    }

    fun onAddCardScreenClosed() {
        safeRequest({ refreshUser() }, {}, {})
    }
}
