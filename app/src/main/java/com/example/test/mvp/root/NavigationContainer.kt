package com.example.test.mvp.root

import com.example.test.mvp.base.fragment.navigation.CiceroneNavigationFragment

class NavigationContainer() : CiceroneNavigationFragment() {

    override fun initializeRouter() {
        val args = getArgumenstOrCreate()
        val tab = args.getString(TAB)
        if (tab != null) {
            val tabValue = Tab.valueOf(tab)
            val screen = tabValue.screen
            if(screen!=null)
                getRouter().newRootChain(tabValue.screen)
            else{
                throw Exception()
            }
        }
    }

    private fun setTab(tab: Tab) {
        getArgumenstOrCreate().also {
            it.putString(TAB, tab.name)
        }
        setCiceroneUniqueName("NavigationContainer:" + tab.name)
    }

    fun navigateToHome(){
        val args = getArgumenstOrCreate()
        val tab = args.getString(TAB)

        getRouter().backTo(null)


    }

    companion object {
        val TAB = "TAB"
        fun newInstance(tab: Tab): NavigationContainer {
            return NavigationContainer()
                .also {
                    it.setTab(tab)
                }
        }
    }
}
