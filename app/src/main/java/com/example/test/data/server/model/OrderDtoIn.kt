package com.example.test.data.server.model

class OrderDtoIn(
    val id:Long,
    val amount:Float,
    val payStatus:String,
    val waterStatus:String,
    val created:Long,
    val volume: Int,
    val userBonus:Boolean,
    val bonusCount:Float?,
    val idVodomat:Int,
    val waterStatusCode:Int
){

    val waterStatusValue:WaterStatusValue get() = WaterStatusValue.get(waterStatusCode)

}