package com.example.test.data.repository.string

import androidx.annotation.StringRes

interface StringProvider {
    fun getString(@StringRes id: Int): String
    fun getString(@StringRes resId: Int, vararg formatArgs: Any?): String
    fun getQuantityString(
        resId: Int,
        quantiry: Int,
        vararg formatArgs: Any?
    ): String
}