package com.example.test.data.server

import com.example.test.BuildConfig
import com.example.test.common.utils.time.MyDate
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitUtils {

    private const val MAX_LOG_LENGTH = 4_000

    val base: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_URL + "/")
        .addConverterFactory(GsonConverterFactory
            .create(buildGson()))
        .client(buildHttpClient())
        .build()

    private fun buildHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.addInterceptor(TokenInterceptor())

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }

        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)

        return builder.build()
    }

    private fun buildGson() = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .registerTypeAdapter(MyDate::class.java,RetrofitDateDeSerializer())
        .registerTypeAdapter(MyDate::class.java,RetrofitDateSerializer())
        .create()

}