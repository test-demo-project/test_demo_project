package com.example.test.data.server.model

class WaterPumpDtoIn(
    val id:Int,

    val region:RegionDtoIn,

    val map_lat: Double,
    val map_lon:Double,
    val buildingNumber:String?,
    val address:String?,
    val street:String?,
    val waterCost:Float,
    val externalNumber: Int?
)
