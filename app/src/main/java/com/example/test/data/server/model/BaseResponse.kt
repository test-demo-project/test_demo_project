package com.example.test.data.server.model

open class BaseResponse(
    val message:String? = null
)