package com.example.test.data.server.model

enum class WaterStatusValue(val serverCode:Int) {

    WAIT_PAYMENT(0),
    OK(1),
    ERROR(2);

    companion object{
        fun get(serverCode:Int): WaterStatusValue{
            return values().find { it.serverCode == serverCode }!!
        }
    }
}