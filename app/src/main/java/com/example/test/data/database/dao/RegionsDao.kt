package com.example.test.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.test.data.database.entity.RegionEntity

@Dao
abstract class RegionsDao():BaseDao<RegionEntity>(){

    @androidx.room.Query("SELECT * FROM region")
    abstract fun getRegions(): List<RegionEntity>

    @Query("DELETE FROM region")
    abstract fun deleteAll()

}