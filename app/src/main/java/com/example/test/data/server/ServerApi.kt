package com.example.test.data.server

import com.example.test.data.server.model.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ServerApi{

    @GET("vodomats/all")
    fun getAllWaterPumps(): Call<AllWaterPumpsResponse>

    @GET("region/list")
    fun getRegions():Call<RegionsResponse>

    @POST("user/register")
    fun requestSmsCode(@Body body: RequestSmsCodeDtoOut):Call<RequestSmsCodeResponse>

    @POST("user/login")
    fun loginBySmsCode(@Body loginBySmsCodeDtoOut: LoginBySmsCodeDtoOut): Call<LoginBySmsCodeResponse>

    @GET("user")
    fun getUserData(): Call<UserResponse>

    @POST("user/update")
    fun updateUser(@Body updateUserDtoOut: UpdateUserDtoOut):Call<UserResponse>

    @POST("user/logout")
    fun logOut(): Call<BaseResponse>

    @POST("feedback/create")
    @Multipart
    fun feedback(
        @Part multipartBody: MultipartBody.Part,
        @Part files:List<MultipartBody.Part>):Call<BaseResponse>

    @GET("user/orders")
    fun history():Call<OrderHistoryResponse>

    @POST("user/cards/bind")
    fun getBindCardUrl():Call<CardBindUrlDroIn>

    @POST("user/cards/unbind")
    fun unbindCard(@Body body:UnbindCardDtoOut):Call<Any>

    @POST("water/buy")
    fun buyWater(@Body params:BuyWaterDtoOut):Call<BuyWaterResponse>

    @GET("order/{id}")
    fun getOrder(@Path("id")id:Long):Call<BuyWaterResponse>

    @GET("vodomats/liked")
    fun getFavoriteWaterPumps(): Call<AllWaterPumpsResponse>

    @HTTP(method = "DELETE", path = "vodomats/liked", hasBody = true)
    fun deleteFavoriteWaterPump(@Body body:FavoriteWaterPumpId):Call<Unit>

    @POST("vodomats/liked")
    fun addToFavoriteWaterPump(@Body body: FavoriteWaterPumpId):Call<Unit>
}