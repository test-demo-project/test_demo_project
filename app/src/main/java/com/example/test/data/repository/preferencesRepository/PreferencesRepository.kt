package com.example.test.data.repository.preferencesRepository

import com.example.test.data.model.CityModel


interface PreferencesRepository {

    var token:String?

    var smsCodeSendTime:Long?
    var smsCodeSendPhone: String?

    var currentCity: CityModel?

    var isSignedUserAgreement:Boolean

    var isFavoritePupsFunctionHindPopupShowed:Boolean

    var lastUsedVolume: Int

}