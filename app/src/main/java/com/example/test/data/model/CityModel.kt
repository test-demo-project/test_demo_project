package com.example.test.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class CityModel(
    val id:Int,
    val name:String
):Parcelable