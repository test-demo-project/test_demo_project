package com.example.test.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.test.data.database.entity.WaterPumpEntity
import com.example.test.data.database.join.WaterPumpJoin
import com.example.test.data.database.join.WaterPumpLocation

@Dao
abstract class WaterPumpDao: BaseDao<WaterPumpEntity>(){

    @Query("SELECT * FROM pump")
    abstract fun getAllWaterPumps(): List<WaterPumpEntity>

    @Query("SELECT * FROM pump WHERE regionId == :region")
    abstract fun getWaterPumpsByRegion(region:Int):List<WaterPumpEntity>

    @Query("SELECT * FROM pump WHERE id==:id")
    abstract fun getWaterPumpById(id:Int): WaterPumpJoin?

    @Query("DELETE FROM pump")
    abstract fun deleteAll()

    @Query("SELECT id, regionId, map_lat, map_lon FROM pump")
    abstract fun getAllWaterPumpsLocations(): List<WaterPumpLocation>

    @Query("SELECT * FROM pump WHERE map_lat <= :north AND map_lon>= :south AND map_lat>=:west AND map_lon<=:east")
    abstract fun getWaterPumpsInSquare(north: Double, south:Double, west:Double,east:Double):List<WaterPumpEntity>


    @Query("SELECT * FROM pump where id IN (:ids)")
    abstract fun getWaterPumpsWithIds(ids:List<Int>):List<WaterPumpEntity>

    @Query("UPDATE pump SET isFavorite=:isFavorite where id == :id")
    abstract fun setFavorite(id:Int, isFavorite:Boolean)

    @Query("SELECT * FROM pump WHERE isFavorite==1 ")
    abstract fun getFavoritePumps(): List<WaterPumpEntity>

    @Query("UPDATE pump SET isFavorite=0")
    abstract fun clearFavorite()

    @Query("UPDATE pump SET isFavorite=1 WHERE id IN (:ids)")
    abstract fun setFavoritePumps(ids:List<Int>)



    /* @Query("SELECT * FROM pump WHERE mapLat<")
     fun getWaterPumpsByMap(southWestLat:Double,southWestLng:Double, northEastLat:Double,northEastLng:Double)*/


}