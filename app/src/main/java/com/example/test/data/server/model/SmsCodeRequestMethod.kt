package com.example.test.data.server.model

import com.google.gson.annotations.SerializedName

enum class SmsCodeRequestMethod(

){
    @SerializedName("sms")
    SMS,

    @SerializedName("flash-call")
    CALL
}