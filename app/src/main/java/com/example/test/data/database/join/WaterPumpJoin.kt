package com.example.test.data.database.join

import androidx.room.Embedded
import androidx.room.Relation
import com.example.test.data.database.entity.RegionEntity
import com.example.test.data.database.entity.WaterPumpEntity

class WaterPumpJoin(
    @Embedded
    val pump:WaterPumpEntity,

    @Relation(
        parentColumn = "regionId",
        entityColumn = "id"
    )
    val region: RegionEntity
)