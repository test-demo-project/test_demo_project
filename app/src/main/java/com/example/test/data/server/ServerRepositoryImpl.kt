package com.example.test.data.server

import com.example.test.BuildConfig
import com.example.test.R
import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.common.extension.bodyOrError
import com.example.test.common.extension.getMimeType
import com.example.test.common.extension.isSuccessfulOrError
import com.example.test.data.server.model.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import java.io.File

class ServerRepositoryImpl:ServerRepository{

    private val api = RetrofitUtils.base.create(ServerApi::class.java)

    override fun getAllWaterPumps(): List<WaterPumpDtoIn> {
       return api.getAllWaterPumps().bodyOrError().vodomats
    }

    override fun getRegions(): List<RegionDtoIn> {
        return api.getRegions().bodyOrError().regions
    }

    override fun requestSmsCode(body: RequestSmsCodeDtoOut): RequestSmsCodeResponse {
        return api.requestSmsCode(body).bodyOrError()
    }

    override fun loginBySmsCode(loginBySmsCodeDtoOut: LoginBySmsCodeDtoOut): LoginBySmsCodeResponse {
        try{
            return api.loginBySmsCode(loginBySmsCodeDtoOut).bodyOrError()
        }catch (e:HttpException){
            if(e.code()==400){
                throw ServiceException(R.string.wrong_code)
            }else{
                throw e
            }
        }
    }

    override fun getUserData(): UserDtoIn {
        val user = api.getUserData().bodyOrError().user
        if(user.phone.isNullOrBlank()){
            throw UnauthorisedException()
        }
        return user
    }

    override fun updateUser(updateUserDtoOut: UpdateUserDtoOut): UserDtoIn {
        return api.updateUser(updateUserDtoOut).bodyOrError().user
    }

    override fun logOut() {
      api.logOut().isSuccessfulOrError()
    }

    override fun createFeedback(message: String, files: List<File>) {
        val filesBodies =  files.mapIndexed() {index, file->
            val requestBody = file.asRequestBody(file.getMimeType()!!.toMediaTypeOrNull())
            MultipartBody.Part.createFormData("feedbackImage${index}", file.name, requestBody)
        }

        val _message = if(BuildConfig.DEBUG){
            "test ${message}"
        }else{
            message
        }

        val textBody = MultipartBody.Part.createFormData("text",_message)

        api.feedback(textBody,filesBodies).bodyOrError()
    }

    override fun getHistory(): List<OrderDtoIn> {
        return api.history().bodyOrError().orders?:listOf()
    }

    override fun getBindCardUrl(): String {
        return api.getBindCardUrl().bodyOrError().formUrl
    }

    override fun unbindCard(bindingId: String) {
        api.unbindCard(UnbindCardDtoOut(bindingId)).isSuccessfulOrError()
    }

    override fun buyWater(buyWaterDtoOut: BuyWaterDtoOut): OrderDtoIn {
        return api.buyWater(buyWaterDtoOut).bodyOrError().order
    }

    override fun getOrderData(orderId: Long): OrderDtoIn {
        return api.getOrder(orderId).bodyOrError().order
    }

    override fun getFavoriteWaterPumps(): List<WaterPumpDtoIn> {
        return api.getFavoriteWaterPumps().bodyOrError().vodomats
    }

    override fun deleteFavoriteWaterPump(id: Int){
         api.deleteFavoriteWaterPump(FavoriteWaterPumpId(id)).isSuccessfulOrError()
    }

    override fun addToFavoriteWaterPump(id: Int){
        api.addToFavoriteWaterPump(FavoriteWaterPumpId(id)).isSuccessfulOrError()
    }
}