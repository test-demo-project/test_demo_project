package com.example.test.data.server.model

class LoginBySmsCodeDtoOut(
    val phone:String,
    val code:Int
)