package com.example.test.data.server.model

import com.example.test.common.utils.time.MyDate

class UpdateUserDtoOut(
    var fullname:String? = null,
    var region: RegionDtoOut? = null,
    var birthday: MyDate? = null,
    var gender: GenderEnum? = null,
    var smsMailing:Boolean? = null,
    var emailMailing:Boolean? = null,
    var email:String? = null
){

    constructor(user:UserDtoIn) : this(fullname=null) {
        applyUser(user)
    }

    fun applyUser(user:UserDtoIn){
        fullname = user.fullname
        region = user.region?.let {
            RegionDtoOut(it.id)
        }
        birthday = user.birthday
        email = user.email
        emailMailing = user.emailMailing

        gender = user.gender
        smsMailing = user.smsMailing
    }
}