package com.example.test.data.repository.preferencesRepository

import android.content.Context
import android.content.SharedPreferences

import com.chibatching.kotpref.KotprefModel
import com.example.test.data.model.CityModel
import com.google.gson.Gson
import java.lang.Exception

class PreferencesRepositoryImp(context: Context) : KotprefModel(contextProvider = { context }),
    PreferencesRepository {


    override var token:String? by nullableStringPref()

    private var _smsCodeSendTime: Long by longPref(-1)

    override var smsCodeSendTime: Long?
        get() {
            val v = _smsCodeSendTime
            if (v == -1L) {
                return null
            } else {
                return v
            }
        }
        set(value) {
            if (value == null) {
                _smsCodeSendTime = -1L
            } else {
                _smsCodeSendTime = value
            }
        }


    override var smsCodeSendPhone: String? by nullableStringPref()

    private var currentCityGson: String? by nullableStringPref()

    override var currentCity: CityModel?
        get() {
            val json = currentCityGson
            if(json!=null){
                try{
                    return Gson().fromJson<CityModel>(json, CityModel::class.java)
                }catch (e:Exception){
                    return null
                }
            }else{
                return null
            }

        }
        set(value) {
            if(value!=null){
                currentCityGson = Gson().toJson(value)
            }else{
                currentCityGson = null
            }
        }

    override var isSignedUserAgreement: Boolean by booleanPref(false)

    override var isFavoritePupsFunctionHindPopupShowed: Boolean by booleanPref(false)

    override var lastUsedVolume: Int by intPref(0)

}