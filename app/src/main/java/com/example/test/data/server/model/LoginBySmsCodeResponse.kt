package com.example.test.data.server.model

class LoginBySmsCodeResponse(
    val access_token:String,
    val user: UserDtoIn
)