package com.example.test.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "region")
class RegionEntity(
    @PrimaryKey
    val id:Int,
    val name:String
)