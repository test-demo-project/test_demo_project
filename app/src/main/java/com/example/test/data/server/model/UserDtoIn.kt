package com.example.test.data.server.model

import android.os.Parcelable
import com.example.test.common.utils.time.MyDate
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserDtoIn(
    val phone:String?,
    val region:RegionDtoIn?,


    val birthday: MyDate?,
    val gender: GenderEnum?,
    val fullname:String?,
    val firsTime:Boolean,
    val pointsCount:Float,
    val cards:List<CardDtoIn>?,
    val smsMailing:Boolean,
    val emailMailing:Boolean,
    val email:String?,

    @SerializedName("tel_support")
    val telSupport:String?
):Parcelable{


}