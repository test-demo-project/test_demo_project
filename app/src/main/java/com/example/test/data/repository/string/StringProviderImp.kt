package com.example.test.data.repository.string

import android.content.Context
import androidx.annotation.StringRes

class StringProviderImp(private val context: Context) : StringProvider {
    override fun getString(@StringRes id: Int): String {
        return context.getString(id)
    }

    override fun getString(
        @StringRes resId: Int,
        vararg formatArgs: Any?
    ): String {
        return context.getString(resId, *formatArgs)
    }

    override fun getQuantityString(
        resId: Int,
        quantiry: Int,
        vararg formatArgs: Any?
    ): String {
        return context.resources.getQuantityString(resId, quantiry, *formatArgs)
    }

}