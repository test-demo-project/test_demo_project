package com.example.test.data.database.entity

import androidx.room.*
import com.google.android.gms.maps.model.LatLng

@Entity(
    tableName = "pump",
    foreignKeys = arrayOf(
        ForeignKey(
            entity = RegionEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("regionId"),
            onDelete = ForeignKey.RESTRICT,
            onUpdate = ForeignKey.CASCADE
        )
    )
)
class WaterPumpEntity(
    @PrimaryKey()
    val id:Int,

    @ColumnInfo(index = true)
    val regionId:Int,

    @ColumnInfo(name = "map_lat", index = true)
    val mapLat: Double,

    @ColumnInfo(name="map_lon",index = true)
    val mapLon:Double,

    val buildingNumber:String?,

    val address:String?,

    val street:String?,

    //This price in cents
    val waterCost:Long,
    val externalNumber: Int,

    val isFavorite:Boolean
){
    val position get() = LatLng(mapLat,mapLon)
}