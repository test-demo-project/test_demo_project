package com.example.test.data.server

import com.example.test.di.DaggerUtils
import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor : Interceptor {

    private val prefs = DaggerUtils.appComponent.providePreferencesRepository()

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()
        val originalHttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
            .url(url)

        val prefsToken = prefs.token

        if (!prefsToken.isNullOrBlank()) requestBuilder.addHeader("Token", prefsToken)
        requestBuilder.addHeader("Content-Type","application/json")

        val request = requestBuilder.build()
        return chain.proceed(request)

    }
}