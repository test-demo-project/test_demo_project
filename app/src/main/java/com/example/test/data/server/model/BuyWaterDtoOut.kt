package com.example.test.data.server.model

class BuyWaterDtoOut(
    val volume:Int,
    val idVodomat:Int,
    val cardBindingId:String?,
    val useBonus:Boolean
)