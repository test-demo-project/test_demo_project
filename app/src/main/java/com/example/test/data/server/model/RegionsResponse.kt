package com.example.test.data.server.model

class RegionsResponse(
    val regions: List<RegionDtoIn>
)