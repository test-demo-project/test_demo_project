package com.example.test.data.database

import androidx.room.RoomDatabase
import com.example.test.data.database.dao.RegionsDao
import com.example.test.data.database.dao.WaterPumpDao
import com.example.test.data.database.entity.RegionEntity
import com.example.test.data.database.entity.WaterPumpEntity

@androidx.room.Database(
    entities = [RegionEntity::class, WaterPumpEntity::class],
    version = 4,
    exportSchema = false
)
abstract class Database : RoomDatabase() {

    abstract fun getRegionsDao(): RegionsDao
    abstract fun getWaterPumpsDao(): WaterPumpDao

    companion object {
        val DATABASE_NAME = "TEST_DATA_BASE_NAME"
    }
}