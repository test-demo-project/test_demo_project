package com.example.test.data.server.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class CardDtoIn(
    val mask:String,
    val bindingId:String
):Parcelable