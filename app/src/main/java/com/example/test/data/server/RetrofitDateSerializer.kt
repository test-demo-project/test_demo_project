package com.example.test.data.server

import com.example.test.common.utils.time.MyDate
import com.google.gson.*
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*


val SERVER_DATE_FOMAT = "dd.MM.yyyy"

class RetrofitDateSerializer : JsonSerializer<MyDate> {
    override fun serialize(
        srcDate: MyDate?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement? {

        if (srcDate == null)
            return null

        val dateFormat = SimpleDateFormat(SERVER_DATE_FOMAT, Locale.US)

        val formatted = dateFormat.format(srcDate.convertToDate())

        return JsonPrimitive(formatted)
    }
}

class RetrofitDateDeSerializer : JsonDeserializer<MyDate?> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): MyDate? {
        val string = json?.asString ?: return null

        val dateFormat = SimpleDateFormat(SERVER_DATE_FOMAT, Locale.US)

        val date = dateFormat.parse(string)

        return MyDate.build(date)

    }
}
