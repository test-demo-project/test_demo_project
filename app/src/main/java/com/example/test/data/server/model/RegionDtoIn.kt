package com.example.test.data.server.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class RegionDtoIn(

    @SerializedName("tid")
    val id:Int,
    val name:String

):Parcelable{}