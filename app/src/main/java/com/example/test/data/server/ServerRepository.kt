package com.example.test.data.server

import com.example.test.data.server.model.*
import java.io.File

interface ServerRepository{

    fun getRegions(): List<RegionDtoIn>
    fun getAllWaterPumps(): List<WaterPumpDtoIn>

    fun requestSmsCode( body: RequestSmsCodeDtoOut): RequestSmsCodeResponse

    fun loginBySmsCode(loginBySmsCodeDtoOut: LoginBySmsCodeDtoOut): LoginBySmsCodeResponse

    fun getUserData(): UserDtoIn

    fun updateUser(updateUserDtoOut: UpdateUserDtoOut):UserDtoIn

    fun logOut()
    
    fun createFeedback(message:String,files:List<File>)

    fun getHistory(): List<OrderDtoIn>

    fun getBindCardUrl():String

    fun unbindCard(bindingId:String)

    fun buyWater(buyWaterDtoOut: BuyWaterDtoOut):OrderDtoIn

    fun getOrderData(orderId:Long): OrderDtoIn

    fun getFavoriteWaterPumps(): List<WaterPumpDtoIn>

    fun deleteFavoriteWaterPump(id:Int)

    fun addToFavoriteWaterPump(id:Int)
}