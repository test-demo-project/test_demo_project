package com.example.test.data.database.join

import androidx.room.ColumnInfo
import com.google.android.gms.maps.model.LatLng

class WaterPumpLocation(

    @ColumnInfo(name = "id")
    val id:Int,

    @ColumnInfo(name = "regionId")
    val regionId:Int,

    @ColumnInfo(name = "map_lat")
    val mapLat: Double,

    @ColumnInfo(name = "map_lon")
    val mapLon:Double
){
    val position get() = LatLng(mapLat,mapLon)
}