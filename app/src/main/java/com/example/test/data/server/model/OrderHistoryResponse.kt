package com.example.test.data.server.model

class OrderHistoryResponse(
    val orders: List<OrderDtoIn>?
)