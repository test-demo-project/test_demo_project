package com.example.test.data.model

import com.google.android.gms.maps.model.LatLng

class WaterPump(
    val id:Int,
    val waterCost:Long,
    val address:String?,
    val distance:Int?,
    val number:Int,
    val latLng: LatLng,
    val isFavorite:Boolean
)