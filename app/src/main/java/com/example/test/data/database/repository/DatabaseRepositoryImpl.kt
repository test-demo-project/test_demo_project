package com.example.test.data.database.repository

import android.content.Context
import androidx.room.Room
import androidx.room.withTransaction
import com.example.test.data.database.Database
import com.example.test.data.database.entity.RegionEntity
import com.example.test.data.database.entity.WaterPumpEntity
import com.example.test.data.database.join.WaterPumpJoin
import com.example.test.data.database.join.WaterPumpLocation
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.google.maps.android.ktx.utils.sphericalDistance


class DatabaseRepositoryImpl(val context: Context) : DatabaseRepository {

    private val database: Database

    init {
        database = Room.databaseBuilder(
            context,
            Database::class.java,
            Database.DATABASE_NAME
        ).fallbackToDestructiveMigration()
            .build()
    }

    override suspend fun saveRegions(regions: List<RegionEntity>) {
        database.withTransaction {
            database.getRegionsDao().deleteAll()
            database.getRegionsDao().insertOrUpdate(regions)
        }
    }

    override suspend fun saveWaterPumps(waterPumps: List<WaterPumpEntity>) {
        database.withTransaction {
            database.getWaterPumpsDao().deleteAll()
            database.getWaterPumpsDao().insert(waterPumps)
        }
    }

    override suspend fun getAllWaterPumps(): List<WaterPumpEntity> {
        return database.getWaterPumpsDao().getAllWaterPumps()
    }

    override suspend fun getWaterPumpsByRegion(region: Int): List<WaterPumpEntity> {
        return database.getWaterPumpsDao().getWaterPumpsByRegion(region)
    }

    override suspend fun getWaterPumpById(id: Int): WaterPumpJoin? {
        return database.getWaterPumpsDao().getWaterPumpById(id)
    }

    override suspend fun cleanWaterPumpsAndRegions() {
        database.withTransaction {
            database.getWaterPumpsDao().deleteAll()
            database.getRegionsDao().deleteAll()
        }
    }

    override suspend fun getRegions(): List<RegionEntity> {
        return database.getRegionsDao().getRegions()
    }

    override suspend fun getAllWaterPumpsLocations(): List<WaterPumpLocation> {
        return database.getWaterPumpsDao().getAllWaterPumpsLocations()
    }

    override suspend fun getWaterPumpsWithIds(ids: List<Int>): List<WaterPumpEntity> {
        return database.getWaterPumpsDao().getWaterPumpsWithIds(ids)
    }

    override suspend fun setFavorite(id: Int, isFavorite: Boolean) {
        database.getWaterPumpsDao().setFavorite(id, isFavorite)
    }

    override suspend fun getFavoritePumps(): List<WaterPumpEntity> {
        return database.getWaterPumpsDao().getFavoritePumps()
    }

    override suspend fun setFavoritePumps(ids: List<Int>) {
        database.runInTransaction {
            database.getWaterPumpsDao().clearFavorite()
            database.getWaterPumpsDao().setFavoritePumps(ids)
        }
    }

    override suspend fun isFavorite(id: Int): Boolean {
        return database.getWaterPumpsDao().getWaterPumpById(id)?.pump?.isFavorite?:false
    }

    override suspend fun getWaterPumpsInRadius(
        location: LatLng,
        radius: Int
    ): List<WaterPumpEntity> {

        val bounds = calcBounds(location,radius)

        return database.getWaterPumpsDao().getWaterPumpsInSquare(bounds.north,bounds.south,bounds.west,bounds.east).filter {
            it.position.sphericalDistance(location)<=radius
        }
    }

  /*  override suspend fun getWaterPumpsInRadiusSmall(
        location: LatLng,
        radius: Int
    ): List<WaterPumpLocation> {
        val bounds = calcBounds(location,radius)

        Log.d("Bounds",bounds.toString())

        return database.getWaterPumpsDao().getWaterPumpsInSquareSmall(bounds.north,bounds.south,bounds.west,bounds.east).filter {
            it.position.sphericalDistance(location)<=radius
        }

    }*/

    private fun calcBounds(location: LatLng, radius: Int):Bounds{


        val targetNorthEast =
            SphericalUtil.computeOffset(location, radius * Math.sqrt(2.0), 45.0)

        val targetSouthWest =
            SphericalUtil.computeOffset(location, radius * Math.sqrt(2.0), 225.0)

        return Bounds(
            north = targetNorthEast.latitude,
            south = targetSouthWest.longitude,
            west = targetSouthWest.latitude,
            east = targetNorthEast.longitude
        )
    }

    data class Bounds(
        val north:Double,
        val south:Double,
        val west:Double,
        val east:Double
    )


}