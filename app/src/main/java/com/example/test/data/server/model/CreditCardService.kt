package com.example.test.data.server.model

import com.example.test.R

enum class CreditCardService(val firstChar:Char, val title:Int, val serviceImage:Int){

    MIR('2', R.string.card_mir, R.drawable.ic_mir_logo),
    VISA('4',R.string.card_visa, R.drawable.ic_visa),
    MASTER_CARD('5',R.string.card_master_card,R.drawable.ic_mc_symbol)

}