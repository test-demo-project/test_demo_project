package com.example.test.data.database.repository

import com.example.test.data.database.entity.RegionEntity
import com.example.test.data.database.entity.WaterPumpEntity
import com.example.test.data.database.join.WaterPumpJoin
import com.example.test.data.database.join.WaterPumpLocation
import com.google.android.gms.maps.model.LatLng

interface DatabaseRepository {

    suspend fun saveRegions(regions: List<RegionEntity>)

    suspend fun getRegions():List<RegionEntity>

    suspend fun saveWaterPumps(waterPumps: List<WaterPumpEntity>)

    suspend fun getAllWaterPumps(): List<WaterPumpEntity>

    //suspend fun getAllWaterPumps(): List<WaterPumpEntity>

    suspend fun getWaterPumpsByRegion(region: Int): List<WaterPumpEntity>

    suspend fun getWaterPumpById(id: Int): WaterPumpJoin?

    suspend fun cleanWaterPumpsAndRegions()

    suspend fun getAllWaterPumpsLocations(): List<WaterPumpLocation>

    suspend fun getWaterPumpsWithIds(ids: List<Int>):List<WaterPumpEntity>

    suspend fun setFavorite(id:Int,isFavorite:Boolean)

    suspend fun getFavoritePumps(): List<WaterPumpEntity>

    suspend fun setFavoritePumps(ids:List<Int>)

    suspend fun isFavorite(id:Int):Boolean

    suspend fun getWaterPumpsInRadius(location:LatLng, radius:Int): List<WaterPumpEntity>

}


