package com.example.test.interactor

import com.example.test.data.server.ServerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BindCardInteractor(
    val serverRepository: ServerRepository
) {

    suspend fun getFormUrl():String = withContext(Dispatchers.IO){
        serverRepository.getBindCardUrl()
    }

    suspend fun unbindCard(id:String){
        withContext(Dispatchers.IO){
            serverRepository.unbindCard(id)
        }
    }

}