package com.example.test.interactor

import com.example.test.data.server.ServerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

class FeedbackInteractor(val serverRepository: ServerRepository){

    init{

    }

    suspend fun sendFeedback(message:String,files:List<File>){
        withContext(Dispatchers.IO){
            serverRepository.createFeedback(message,files)
        }
    }

}