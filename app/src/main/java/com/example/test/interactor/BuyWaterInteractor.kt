package com.example.test.interactor

import com.example.test.data.server.ServerRepository
import com.example.test.data.server.model.BuyWaterDtoOut
import com.example.test.data.server.model.OrderDtoIn
import com.example.test.data.server.model.WaterStatusValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class BuyWaterInteractor(val serverRepository: ServerRepository) {

    suspend fun buyWater(
        pumpId: Int,
        volume: Int,
        useBonuses: Boolean,
        creditCardId: String?,
        timeToWait:Long
    ): OrderDtoIn {
        return withContext(Dispatchers.IO) {
            val start = System.currentTimeMillis()

            val order = serverRepository.buyWater(
                BuyWaterDtoOut(
                    volume = volume,
                    idVodomat = pumpId,
                    cardBindingId = creditCardId,
                    useBonus = useBonuses
                )
            )

            waitOrderExecution(order,timeToWait)
        }
    }

    suspend fun waitOrderExecution(orderDtoIn: OrderDtoIn,timeout:Long): OrderDtoIn {

        val timeStart = System.currentTimeMillis()

        if(checkCompleteStatus(orderDtoIn)) return orderDtoIn

        return withContext(Dispatchers.IO) {

            var currentOrder = orderDtoIn

            while(!(checkCompleteStatus(currentOrder) || checkTimeout(timeStart,timeout))){

                currentOrder = serverRepository.getOrderData(orderDtoIn.id)

                delay(1000)
            }

            currentOrder

        }
    }

    //Return true if time out
    private fun checkTimeout(timeStart:Long, timeout:Long):Boolean{
        val current = System.currentTimeMillis()

        val timePass = current - timeStart

        return timePass>timeout
    }

    private fun checkCompleteStatus(orderDtoIn: OrderDtoIn):Boolean{
        return orderDtoIn.waterStatusValue == WaterStatusValue.OK || orderDtoIn.waterStatusValue==WaterStatusValue.ERROR
    }

}