package com.example.test.interactor

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.test.common.utils.PhoneMaskApplyer
import com.example.test.data.model.CityModel
import com.example.test.data.repository.preferencesRepository.PreferencesRepository
import com.example.test.data.server.ServerRepository
import com.example.test.data.server.model.*

class UserInteractor(
    val preferencesRepository: PreferencesRepository,
    val serverRepository: ServerRepository,

    ) {


    val userBalance = MutableLiveData<Float?>()

    val user = MutableLiveData<UserDtoIn?>(null)

    init {
        Log.d("USER_INTRACTOR", "CREATE")
    }

    companion object {
        val SMS_CODE_REPEAT_INTERVAL = 1000L * 30L
    }

    fun isUserLogined(): Boolean {
        return preferencesRepository.token != null
    }

    suspend fun requestSmsCode(phone: String,method:SmsCodeRequestMethod) {
        serverRepository.requestSmsCode(RequestSmsCodeDtoOut(phone,method))
        saveRequestSmsCodeTime(phone)
    }

    private fun saveRequestSmsCodeTime(phone: String) {
        preferencesRepository.smsCodeSendPhone = PhoneMaskApplyer.formatPhoneForServer(phone)
        preferencesRepository.smsCodeSendTime = System.currentTimeMillis()
    }

    suspend fun requestSmsCodeIfNeeded(phone: String,method:SmsCodeRequestMethod=SmsCodeRequestMethod.CALL) {
        val lastTime = getSmsCodeSentTime(phone)

        if (lastTime != null) {
            if (getRepeatSmsTimeLeft(phone) ?: 0L <= 0) {
                requestSmsCode(phone,method)
            }
        } else {
            requestSmsCode(phone,method)
        }

    }

    fun getRepeatSmsTimeLeft(phone: String): Long? {
        val clearPhone = PhoneMaskApplyer.formatPhoneForServer(phone)

        val lastTime = getSmsCodeSentTime(clearPhone) ?: return null

        val currentTime = System.currentTimeMillis()

        val timePass = currentTime - lastTime

        return SMS_CODE_REPEAT_INTERVAL - timePass
    }

    fun getSmsCodeSentTime(phone: String): Long? {
        val clearPhone = PhoneMaskApplyer.formatPhoneForServer(phone)

        if (preferencesRepository.smsCodeSendPhone == clearPhone) {
            return preferencesRepository.smsCodeSendTime
        } else {
            return null
        }
    }

    suspend fun auth(phone: String, code: String): UserDtoIn {
        val response = serverRepository.loginBySmsCode(LoginBySmsCodeDtoOut(phone, code.toInt()))
        preferencesRepository.token = response.access_token
        val user = response.user
        saveUser(user)
        return user
    }

    private fun saveUser(userDtoIn: UserDtoIn) {
        userBalance.postValue(userDtoIn.pointsCount)
        preferencesRepository.currentCity = userDtoIn.region?.let {
            CityModel(
                id = it.id,
                it.name
            )
        }
        user.postValue(userDtoIn)
    }

    fun setUserSignedUserAgreement() {
        preferencesRepository.isSignedUserAgreement = true
    }

    fun isSignedUserAgreement(): Boolean {
        return preferencesRepository.isSignedUserAgreement
    }

    fun setCity(city: CityModel) {
        preferencesRepository.currentCity = city
    }

    fun getCity(): CityModel? {
        return preferencesRepository.currentCity
    }

    suspend fun getUser(): UserDtoIn {
        val user = serverRepository.getUserData()
        saveUser(user)
        return user
    }

    suspend fun updateCity(city: CityModel) {

        val user = serverRepository.getUserData()

        val userDtoOut = UpdateUserDtoOut(user)

        userDtoOut.region = RegionDtoOut(city.id)

        updateUser(userDtoOut)
    }

    fun logoutLocal() {
        preferencesRepository.token = null
        preferencesRepository.currentCity = null

        user.postValue(null)
    }

    fun logout() {
        try {
            serverRepository.logOut()
        } finally {
            logoutLocal()
        }
    }

    private fun updateUser(userDtoOut: UpdateUserDtoOut): UserDtoIn {
        val updatedUser = serverRepository.updateUser(userDtoOut)
        saveUser(updatedUser)
        return updatedUser
    }


    //This function ignore notifications
    fun updateProfile(profile: UpdateUserDtoOut): UserDtoIn {
        val user = serverRepository.getUserData()

        val updateUserDtoOut = UpdateUserDtoOut(user)

        updateUserDtoOut.fullname = profile.fullname
        updateUserDtoOut.region = profile.region?.let {
            RegionDtoOut(it.tid)
        }

        updateUserDtoOut.birthday = profile.birthday
        updateUserDtoOut.email = profile.email
        updateUserDtoOut.gender = profile.gender

        return updateUser(updateUserDtoOut)
    }

    fun setSms(enabled: Boolean): UserDtoIn {
        val user = serverRepository.getUserData()

        val dtoOut = UpdateUserDtoOut(user)

        dtoOut.smsMailing = enabled

        return updateUser(dtoOut)
    }

    fun setEmail(enabled: Boolean): UserDtoIn {
        val user = serverRepository.getUserData()

        val dtoOut = UpdateUserDtoOut(user)

        dtoOut.emailMailing = enabled

        return updateUser(dtoOut)
    }

    var isFavoritePupsFunctionHindPopupShowed: Boolean
        set(value) {
            preferencesRepository.isFavoritePupsFunctionHindPopupShowed = value
        }
        get() {
            return preferencesRepository.isFavoritePupsFunctionHindPopupShowed
        }

    var lastUsedVolume: Int?
        get() {
            val value = preferencesRepository.lastUsedVolume
            if (value <= 0) {
                return null
            }
            return value
        }
        set(value) {
            preferencesRepository.lastUsedVolume = value?:0
        }
}

