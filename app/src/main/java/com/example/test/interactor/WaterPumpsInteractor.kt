package com.example.test.interactor

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.test.R
import com.example.test.common.errors.excetpions.ServiceException
import com.example.test.data.database.entity.RegionEntity
import com.example.test.data.database.entity.WaterPumpEntity
import com.example.test.data.database.repository.DatabaseRepository
import com.example.test.data.model.WaterPump
import com.example.test.data.repository.preferencesRepository.PreferencesRepository
import com.example.test.data.server.ServerRepository
import com.example.test.data.server.model.WaterPumpDtoIn
import com.example.test.di.DaggerUtils
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.ktx.utils.sphericalDistance
import javax.inject.Inject

class WaterPumpsInteractor(){

    @Inject
    lateinit var serverRepository: ServerRepository

    @Inject
    lateinit var databaseRepository:DatabaseRepository

    @Inject
    lateinit var preferencesRepository: PreferencesRepository

    init{
        DaggerUtils.appComponent.inject(this)
    }

    private val _favoriteSetChanged = MutableLiveData<Unit>()
    val favoriteSetChanged:LiveData<Unit> = _favoriteSetChanged

    suspend fun synchronize(){

        val regions = serverRepository.getRegions()
        val allWaterPumps = serverRepository.getAllWaterPumps()

        databaseRepository.cleanWaterPumpsAndRegions()

        databaseRepository.saveRegions(
            regions.map {
                RegionEntity(
                    id = it.id,
                    name = it.name
                )
            }
        )

        databaseRepository.saveWaterPumps(
            allWaterPumps.map {
                mapWaterPump(it)
            }
        )

        synchronizeFavorites()
    }

    suspend fun synchronizeFavorites(){
        if(!preferencesRepository.token.isNullOrBlank()){
            val fav = serverRepository.getFavoriteWaterPumps()
            val ids = fav.map { it.id }
            databaseRepository.setFavoritePumps(ids)
        }
    }

    suspend fun getWaterPumpsInRegion(regionId:Int):List<WaterPumpEntity>{
        return databaseRepository.getWaterPumpsByRegion(regionId)
    }

    suspend fun getNearestWaterPumps(latLng: LatLng): List<WaterPump>{
        val timerResult = Result()

        return calcExecutionTime(timerResult){
            databaseRepository.getWaterPumpsInRadius(latLng,100000).map {   waterPumpEntityToModel(it,latLng) }.sortedBy { it.distance }
        }
    }

    private fun waterPumpEntityToModel(waterPumpEntity: WaterPumpEntity,location:LatLng?):WaterPump{
        return WaterPump(
            id = waterPumpEntity.id,
            address = waterPumpEntity.address,
            distance = location?.let {   waterPumpEntity.position.sphericalDistance(it).toInt()} ,
            waterCost = waterPumpEntity.waterCost,
            number = waterPumpEntity.externalNumber!!,
            latLng = waterPumpEntity.position,
            isFavorite = waterPumpEntity.isFavorite
        )
    }


    class Result(){
        var time:Long = 0
            set(value){
                field = value
                Log.d("TIME_RESULT",value.toString())
            }
    }

    private suspend fun <T>calcExecutionTime(result:Result, function: suspend()->T):T{
        val time = System.currentTimeMillis()

        val functionResult = function()

        val newTime = System.currentTimeMillis()

        val diff = newTime - time

        result.time = diff

        return functionResult
    }



   /* suspend fun getAllWaterPumpLocations(): List<WaterPumpLocation> {
        return databaseRepository.getAllWaterPumpsLocations()
    }*/

    suspend fun getWaterPumpLocationsInRadius(center:LatLng, radius:Int):List<WaterPumpEntity>{
        return databaseRepository.getWaterPumpsInRadius(center,radius)
    }

    private fun distance(point1: LatLng,point2: LatLng):Double{
        return point1.sphericalDistance(point2)
    }

    private fun mapWaterPump(waterPumpDtoIn: WaterPumpDtoIn): WaterPumpEntity{
        return WaterPumpEntity(
            id = waterPumpDtoIn.id,
            regionId = waterPumpDtoIn.region.id,
            mapLat = waterPumpDtoIn.map_lat,
            mapLon = waterPumpDtoIn.map_lon,
            buildingNumber = waterPumpDtoIn.buildingNumber,
            address = waterPumpDtoIn.address,
            street = waterPumpDtoIn.street,
            waterCost = (waterPumpDtoIn.waterCost*100).toLong(),
            externalNumber = waterPumpDtoIn.externalNumber!!,
            isFavorite = false
        )
    }

    suspend fun getWaterPump(id:Int): WaterPump{
        val waterPump = databaseRepository.getWaterPumpById(id)?:throw ServiceException(R.string.water_pump_not_found)
        return waterPump.pump.toModel()
    }

    suspend fun isFavoriteWaterPump(id:Int):Boolean{
        return false
    }

    fun WaterPumpEntity.toModel(): WaterPump{
        return WaterPump(
            id = id,waterCost = waterCost,address = address,distance = null,number = externalNumber,latLng = position,isFavorite = isFavorite)
    }

    suspend fun getFavoriteWaterPumps(latLng: LatLng?): List<WaterPump>{
        val favorite = databaseRepository.getFavoritePumps().map {
            waterPumpEntityToModel(it,latLng)
        }

        return favorite
    }


    suspend fun addToFavorite(pumpId:Int){
        serverRepository.addToFavoriteWaterPump(pumpId)
        databaseRepository.setFavorite(pumpId,true)
        _favoriteSetChanged.postValue(Unit)
    }

    suspend fun removeFromFavorite(pumpId:Int){
        serverRepository.deleteFavoriteWaterPump(pumpId)
        databaseRepository.setFavorite(pumpId,false)
        _favoriteSetChanged.postValue(Unit)
    }


}