package com.example.test

import android.app.Application
import android.content.Context
import com.example.test.di.DaggerUtils


class App() : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        DaggerUtils.buildComponents(this)
    }
}
